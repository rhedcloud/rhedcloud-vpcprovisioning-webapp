package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class IdentityCenterSyncPojo extends SharedObject implements IsSerializable {
	/*
	<!-- Action is one of 'Start' or 'Stop' -->
	<!-- Classification is one of 'Standard' or 'ECS' -->
	<!-- Status is one of 'Started', 'Stopped', 'Running' -->
	<!-- SyncStatusHTML is the running 'status' of the migration/sync in HTML form -->
	<!ELEMENT IdentityCenterSyncRequisition (Type, Action, Classification)>
	<!ELEMENT IdentityCenterSync (Type?, Status?, Classification?, SyncStatusHTML?)>
	<!ELEMENT IdentityCenterSyncQuerySpecification (Type?, Classification?)>
	*/
	
	String type;
	String status;
	String classification;
	String syncStatusHTML;

	public IdentityCenterSyncPojo() {
		// TODO Auto-generated constructor stub
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getSyncStatusHTML() {
		return syncStatusHTML;
	}

	public void setSyncStatusHTML(String syncStatusHTML) {
		this.syncStatusHTML = syncStatusHTML;
	}

}
