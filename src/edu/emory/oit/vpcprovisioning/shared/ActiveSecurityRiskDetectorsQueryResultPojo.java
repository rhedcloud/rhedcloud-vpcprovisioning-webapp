package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class ActiveSecurityRiskDetectorsQueryResultPojo extends SharedObject implements IsSerializable {
	ActiveSecurityRiskDetecorsQueryFilterPojo filterUsed;
	List<ActiveSecurityRiskDetectorsPojo> results;

	public ActiveSecurityRiskDetectorsQueryResultPojo() {
		// TODO Auto-generated constructor stub
	}

	public ActiveSecurityRiskDetecorsQueryFilterPojo getFilterUsed() {
		return filterUsed;
	}

	public void setFilterUsed(ActiveSecurityRiskDetecorsQueryFilterPojo filterUsed) {
		this.filterUsed = filterUsed;
	}

	public List<ActiveSecurityRiskDetectorsPojo> getResults() {
		return results;
	}

	public void setResults(List<ActiveSecurityRiskDetectorsPojo> results) {
		this.results = results;
	}

}
