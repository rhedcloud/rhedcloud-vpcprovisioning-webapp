package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountAssignmentPojo extends SharedObject implements IsSerializable, Comparable<AccountAssignmentPojo> {
	/*
	<!ELEMENT AccountAssignment (PublicId+, InstanceArn?, PermissionSetArn?, PrincipalId?, PrincipalType?, TargetId, TargetType, CreateUser, CreateDatetime, LastUpdateUser?, LastUpdateDatetime?)>
	*/
	
//	List<String> publicIds = new java.util.ArrayList<String>();
	String publicId;
	String instanceArn;
	String permissionSetName;
	String principalId;
	String principalType;
	String targetId;
	String targetType;

	public AccountAssignmentPojo() {
		
	}

	@Override
	public int compareTo(AccountAssignmentPojo o) {
		
		return 0;
	}

	public String getPublicId() {
		return publicId;
	}

	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}

	public String getInstanceArn() {
		return instanceArn;
	}

	public void setInstanceArn(String instancArn) {
		this.instanceArn = instancArn;
	}

	public String getPermissionSetName() {
		return permissionSetName;
	}

	public void setPermissionSetName(String permissionSet) {
		this.permissionSetName = permissionSet;
	}

	public String getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getPrincipalType() {
		return principalType;
	}

	public void setPrincipalType(String principalType) {
		this.principalType = principalType;
	}

}
