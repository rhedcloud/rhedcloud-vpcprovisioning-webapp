package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class ActiveSecurityRiskDetecorsQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter {
	List<String> detectorNames = new java.util.ArrayList<String>();
	
	public ActiveSecurityRiskDetecorsQueryFilterPojo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	public List<String> getDetectorNames() {
		return detectorNames;
	}

	public void setDetectorNames(List<String> detectorNames) {
		this.detectorNames = detectorNames;
	}

}
