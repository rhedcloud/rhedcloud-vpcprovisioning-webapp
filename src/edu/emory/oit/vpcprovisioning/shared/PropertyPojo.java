package edu.emory.oit.vpcprovisioning.shared;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class PropertyPojo extends SharedObject implements IsSerializable, Comparable<PropertyPojo> {
	String name;
	String value;
	String prettyName;
	String userComment;
	Date effectiveDate;
	Date expirationDate;
	boolean editable;

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public PropertyPojo() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPrettyName() {
		return prettyName;
	}

	public void setPrettyName(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public int compareTo(PropertyPojo o) {
		if (getPrettyName() != null && o.getPrettyName() != null) {
			return getPrettyName().compareTo(o.getPrettyName());
		}
		else if (getValue() != null && o.getValue() != null) {
			return getValue().compareTo(o.getValue());
		}
		return 0;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}
