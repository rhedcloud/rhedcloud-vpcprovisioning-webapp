package edu.emory.oit.vpcprovisioning.shared;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountExtraMetaDataCache extends SharedObject implements IsSerializable {
	// hashMap is keyed off of accountId
	HashMap<String, AccountExtraMetaDataPojo> hashMap = new HashMap<String, AccountExtraMetaDataPojo>();
	java.util.List<AccountExtraMetaDataPojo> aemdList = new java.util.ArrayList<AccountExtraMetaDataPojo>();
	private static AccountExtraMetaDataCache instance = null;
	
	private AccountExtraMetaDataCache() {}
	
	public static HashMap<String, AccountExtraMetaDataPojo> getCache() {
		if (instance == null) {
			instance = new AccountExtraMetaDataCache();
		}
		return instance.hashMap;
	}
	
	public static List<AccountExtraMetaDataPojo> getList() {
		if (instance == null) {
			instance = new AccountExtraMetaDataCache();
		}
		return instance.aemdList;
	}
	
	public static Date getLastUpdateTime() {
		if (instance == null) {
			instance = new AccountExtraMetaDataCache();
		}
		return instance.getUpdateTime();
	}
}
