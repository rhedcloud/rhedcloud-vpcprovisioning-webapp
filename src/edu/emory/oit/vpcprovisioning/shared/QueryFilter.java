package edu.emory.oit.vpcprovisioning.shared;

public interface QueryFilter {
	public boolean isEmpty();
}
