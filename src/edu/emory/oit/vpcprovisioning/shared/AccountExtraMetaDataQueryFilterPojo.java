package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountExtraMetaDataQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter {
	String accountId;
	UserAccountPojo userLoggedIn;
	boolean fuzzyFilter=false;
	boolean suggestBoxFilter = false;
	
	public AccountExtraMetaDataQueryFilterPojo() {
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public UserAccountPojo getUserLoggedIn() {
		return userLoggedIn;
	}

	public void setUserLoggedIn(UserAccountPojo userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

	public boolean isFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(boolean fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSuggestBoxFilter() {
		return suggestBoxFilter;
	}

	public void setSuggestBoxFilter(boolean suggestBoxFilter) {
		this.suggestBoxFilter = suggestBoxFilter;
	}

}
