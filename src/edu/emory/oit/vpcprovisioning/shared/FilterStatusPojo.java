package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class FilterStatusPojo extends SharedObject implements IsSerializable {

	boolean isValid;
	String filteredText;
	boolean applyFilter;
	
	public FilterStatusPojo() {
		// TODO Auto-generated constructor stub
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getFilteredText() {
		return filteredText;
	}

	public void setFilteredText(String filteredText) {
		this.filteredText = filteredText;
	}

	public boolean isApplyFilter() {
		return applyFilter;
	}

	public void setApplyFilter(boolean applyFilter) {
		this.applyFilter = applyFilter;
	}

}
