package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountAssignmentRequisitionPojo extends SharedObject implements IsSerializable {
	/*
	<!ELEMENT AccountAssignmentRequisition (PublicId+, PrincipalType, TargetId, TargetType?)>
	*/

	List<String> publicIds = new java.util.ArrayList<String>();
	String principalType;
	String targetId;
	String targetType;

	public AccountAssignmentRequisitionPojo() {
		
	}

	public List<String> getPublicIds() {
		return publicIds;
	}

	public void setPublicIds(List<String> publicIds) {
		this.publicIds = publicIds;
	}

	public String getPrincipalType() {
		return principalType;
	}

	public void setPrincipalType(String principalType) {
		this.principalType = principalType;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

}
