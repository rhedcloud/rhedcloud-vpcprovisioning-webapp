package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class IdentityCenterSyncSummaryPojo extends SharedObject implements IsSerializable {
	List<IdentityCenterSyncPojo> iicStatusList = new java.util.ArrayList<IdentityCenterSyncPojo>();
	
	public IdentityCenterSyncSummaryPojo() {
		// TODO Auto-generated constructor stub
	}

	public List<IdentityCenterSyncPojo> getIicStatusList() {
		return iicStatusList;
	}

	public void setIicStatusList(List<IdentityCenterSyncPojo> iicStatusList) {
		this.iicStatusList = iicStatusList;
	}

	public void addIicStatus(IdentityCenterSyncPojo status) {
		this.iicStatusList.add(status);
	}
}
