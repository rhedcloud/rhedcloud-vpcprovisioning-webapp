package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

@SuppressWarnings("serial")
public class AccountExtraMetaDataPojo extends SharedObject implements IsSerializable, Comparable<AccountExtraMetaDataPojo> {
	/*
	<!ELEMENT AccountExtraMetaData (AccountExtraMetaDataId?, AccountId, UnitOrSchool, 
	DepartmentName, PrincipalInvestigatorId, 
	
	PrimaryProjectName, PrimaryProjectDataType, PrimaryProjectPurpose+, 
	PrimaryProjectSoftware, PrimaryProjectPlatform+, PrimaryProjectDomainArea,  
	 
	SecondaryProjectName?, SecondaryProjectDataType?, SecondaryProjectPurpose*,  
	SecondaryProjectSoftware?, SecondaryProjectPlatform*, SecondaryProjectDomainArea?, 
	 
	LevelOfAwsExperience, CliExperience, ReferrerInfo, 
	CreateUser, CreateDatetime, LastUpdateUser?, LastUpdateDatetime?)>
	 */
	String accountId;
	String unitOrSchool;
	String departmentName;
	DirectoryMetaDataPojo principalInvestigatorMetaData;
	String levelOfAwsExperience;
	String cliExperience;
	String referrerInfo;
	
	String primaryProjectName;
	String primaryProjectDataType;
	List<String> primaryProjectPurpose = new java.util.ArrayList<String>();
	String primaryProjectSoftware;
	List<String> primaryProjectPlatform = new java.util.ArrayList<String>();
	String primaryProjectDomainArea;
	
	String secondaryProjectName;
	String secondaryProjectDataType;
	List<String> secondaryProjectPurpose = new java.util.ArrayList<String>();
	String secondaryProjectSoftware;
	List<String> secondaryProjectPlatform = new java.util.ArrayList<String>();
	String secondaryProjectDomainArea;
	
	AccountExtraMetaDataPojo baseline;
	
	public static final ProvidesKey<AccountExtraMetaDataPojo> KEY_PROVIDER = new ProvidesKey<AccountExtraMetaDataPojo>() {
		@Override
		public Object getKey(AccountExtraMetaDataPojo item) {
			return item == null ? null : item.getAccountId();
		}
	};
	public AccountExtraMetaDataPojo() {
	}

	@Override
	public int compareTo(AccountExtraMetaDataPojo o) {
		return getAccountId().compareTo(o.getAccountId());
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUnitOrSchool() {
		return unitOrSchool;
	}

	public void setUnitOrSchool(String unitOrSchool) {
		this.unitOrSchool = unitOrSchool;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public DirectoryMetaDataPojo getPrincipalInvestigatorMetaData() {
		return principalInvestigatorMetaData;
	}

	public void setPrincipalInvestigatorMetaData(DirectoryMetaDataPojo principalInvestigatorMetaData) {
		this.principalInvestigatorMetaData = principalInvestigatorMetaData;
	}

	public String getLevelOfAwsExperience() {
		return levelOfAwsExperience;
	}

	public void setLevelOfAwsExperience(String levelOfAwsExperience) {
		this.levelOfAwsExperience = levelOfAwsExperience;
	}

	public AccountExtraMetaDataPojo getBaseline() {
		return baseline;
	}

	public void setBaseline(AccountExtraMetaDataPojo baseline) {
		this.baseline = baseline;
	}

	public String getCliExperience() {
		return cliExperience;
	}

	public void setCliExperience(String cliExperience) {
		this.cliExperience = cliExperience;
	}

	public String getReferrerInfo() {
		return referrerInfo;
	}

	public void setReferrerInfo(String referrerInfo) {
		this.referrerInfo = referrerInfo;
	}

	public String getPrimaryProjectName() {
		return primaryProjectName;
	}

	public void setPrimaryProjectName(String primaryProjectName) {
		this.primaryProjectName = primaryProjectName;
	}

	public String getPrimaryProjectDataType() {
		return primaryProjectDataType;
	}

	public void setPrimaryProjectDataType(String primaryProjectDataType) {
		this.primaryProjectDataType = primaryProjectDataType;
	}

	public String getPrimaryProjectSoftware() {
		return primaryProjectSoftware;
	}

	public void setPrimaryProjectSoftware(String primaryProjectSoftware) {
		this.primaryProjectSoftware = primaryProjectSoftware;
	}

	public String getPrimaryProjectDomainArea() {
		return primaryProjectDomainArea;
	}

	public void setPrimaryProjectDomainArea(String primaryProjectDomainArea) {
		this.primaryProjectDomainArea = primaryProjectDomainArea;
	}

	public String getSecondaryProjectName() {
		return secondaryProjectName;
	}

	public void setSecondaryProjectName(String secondaryProjectName) {
		this.secondaryProjectName = secondaryProjectName;
	}

	public String getSecondaryProjectDataType() {
		return secondaryProjectDataType;
	}

	public void setSecondaryProjectDataType(String secondaryProjectDataType) {
		this.secondaryProjectDataType = secondaryProjectDataType;
	}

	public String getSecondaryProjectSoftware() {
		return secondaryProjectSoftware;
	}

	public void setSecondaryProjectSoftware(String secondaryProjectSoftware) {
		this.secondaryProjectSoftware = secondaryProjectSoftware;
	}

	public String getSecondaryProjectDomainArea() {
		return secondaryProjectDomainArea;
	}

	public void setSecondaryProjectDomainArea(String secondaryProjectDomainArea) {
		this.secondaryProjectDomainArea = secondaryProjectDomainArea;
	}

	public List<String> getPrimaryProjectPurpose() {
		return primaryProjectPurpose;
	}

	public void setPrimaryProjectPurpose(List<String> primaryProjectPurpose) {
		this.primaryProjectPurpose = primaryProjectPurpose;
	}

	public List<String> getPrimaryProjectPlatform() {
		return primaryProjectPlatform;
	}

	public void setPrimaryProjectPlatform(List<String> primaryProjectPlatform) {
		this.primaryProjectPlatform = primaryProjectPlatform;
	}

	public List<String> getSecondaryProjectPurpose() {
		return secondaryProjectPurpose;
	}

	public void setSecondaryProjectPurpose(List<String> secondaryProjectPurpose) {
		this.secondaryProjectPurpose = secondaryProjectPurpose;
	}

	public List<String> getSecondaryProjectPlatform() {
		return secondaryProjectPlatform;
	}

	public void setSecondaryProjectPlatform(List<String> secondaryProjectPlatform) {
		this.secondaryProjectPlatform = secondaryProjectPlatform;
	}
}
