package edu.emory.oit.vpcprovisioning.shared;

import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

@SuppressWarnings("serial")
public class AccountPojo extends SharedObject implements IsSerializable, Comparable<AccountPojo> {
	String accountId;
	String passwordLocation;
	String accountName;
	List<EmailPojo> emailList = new java.util.ArrayList<EmailPojo>();
	DirectoryMetaDataPojo accountOwnerDirectoryMetaData;
	String speedType;
	String complianceClass;
	List<String> sensitiveDataList = new java.util.ArrayList<String>();
	List<PropertyPojo> properties = new java.util.ArrayList<PropertyPojo>();
	AccountPojo baseline;
	String accountType;
	AccountExtraMetaDataPojo extraMetaData=null;
	
	// phase 2 mods
	String alternateName;
	
	public String getAlternateName() {
		return alternateName;
	}

	public void setAlternateName(String alternateName) {
		this.alternateName = alternateName;
	}

	public static final ProvidesKey<AccountPojo> KEY_PROVIDER = new ProvidesKey<AccountPojo>() {
		@Override
		public Object getKey(AccountPojo item) {
			return item == null ? null : item.getAccountId();
		}
	};
	public AccountPojo() {
	}

	@Override
	public int compareTo(AccountPojo o) {
		return getAccountId().compareTo(o.getAccountId());
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public List<EmailPojo> getEmailList() {
		return emailList;
	}

	public void setEmailList(List<EmailPojo> emailList) {
		this.emailList = emailList;
	}

	public String getSpeedType() {
		return speedType;
	}

	public void setSpeedType(String speedType) {
		this.speedType = speedType;
	}

	public String getPasswordLocation() {
		return passwordLocation;
	}

	public void setPasswordLocation(String passwordLocation) {
		this.passwordLocation = passwordLocation;
	}

	public AccountPojo getBaseline() {
		return baseline;
	}

	public void setBaseline(AccountPojo baseline) {
		this.baseline = baseline;
	}

	public boolean containsEmail(EmailPojo email) {
		for (EmailPojo pojo : this.emailList) {
			if (pojo.getEmailAddress().equalsIgnoreCase(email.getEmailAddress()) && 
				pojo.getType().equalsIgnoreCase(email.getType())) {
				
				return true;
			}
		}
		return false;
	}

	public DirectoryMetaDataPojo getAccountOwnerDirectoryMetaData() {
		return accountOwnerDirectoryMetaData;
	}

	public void setAccountOwnerDirectoryMetaData(DirectoryMetaDataPojo accountOwnerDirectoryMetaData) {
		this.accountOwnerDirectoryMetaData = accountOwnerDirectoryMetaData;
	}

	public String getComplianceClass() {
		return complianceClass;
	}

	public void setComplianceClass(String complianceClass) {
		this.complianceClass = complianceClass;
	}

	public List<String> getSensitiveDataList() {
		return sensitiveDataList;
	}

	public void setSensitiveDataList(List<String> sensitiveDataList) {
		this.sensitiveDataList = sensitiveDataList;
	}

	public List<PropertyPojo> getProperties() {
		return properties;
	}

	public void setProperties(List<PropertyPojo> properties) {
		this.properties = properties;
	}
	
	public boolean isSrdExempt() {
		for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase("srdexempt") && 
				prop.getValue().equalsIgnoreCase("true")) {
				
				return true;
			}
		}
		return false;
	}
	
	public void setSrdExempt(boolean isExempt, UserAccountPojo user) {
		for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase("srdexempt")) {
				prop.setValue(Boolean.toString(isExempt));
				return;
			}
		}
		PropertyPojo prop = new PropertyPojo();
		prop.setName("srdExempt");
		prop.setValue(Boolean.toString(isExempt));
		prop.setCreateUser(user.getPublicId());
		prop.setCreateTime(new Date(System.currentTimeMillis()));
	}
	
	public List<PropertyPojo> getWhitelistedSrds() {
		List<PropertyPojo> props = new java.util.ArrayList<PropertyPojo>();

		for (PropertyPojo prop : this.properties) {
			if (prop.getName().toLowerCase().indexOf("srd:") >= 0 &&
				prop.getName().toLowerCase().indexOf("arn:") < 0 &&
				prop.getValue().equalsIgnoreCase("exempt")) {
				
				props.add(prop);
			}
		}
		
		return props;
	}
	
	public void addWhitelistedSrd(String detectorName, UserAccountPojo user) {
		boolean addIt=true;
		propLoop: for (PropertyPojo prop : this.properties) {
			if ((prop.getName().equalsIgnoreCase("srd:" + detectorName) &&
				prop.getValue().equalsIgnoreCase("exempt")) ||
				(prop.getName().equalsIgnoreCase(detectorName) && 
				prop.getValue().equalsIgnoreCase("exempt"))) {
				
				addIt=false;
				break propLoop;
			}
		}
		
		if (addIt) {
			PropertyPojo prop = new PropertyPojo();
			if (detectorName.toLowerCase().indexOf("srd:") < 0) {
				prop.setName("SRD:" + detectorName);
			}
			else {
				prop.setName(detectorName);
			}
			prop.setValue("exempt");
			prop.setCreateUser(user.getPublicId());
			prop.setCreateTime(new Date(System.currentTimeMillis()));
			this.properties.add(prop);
		}
	}
	
	public void removeWhitelistedSrd(String detectorName) {
		propLoop: for (PropertyPojo prop : this.properties) {
			GWT.log("detectorName passed in is: '" + detectorName + "'  Property name is: '" + prop.getName() + "'");
			if (prop.getName().equalsIgnoreCase("srd:" + detectorName) || 
				prop.getName().equalsIgnoreCase(detectorName)) {
				
				GWT.log("Removing whitelisted srd: " + prop.getName());
				this.properties.remove(prop);
				break propLoop;
			}
		}
	}
	
	public void clearWhitelist() {
		for (PropertyPojo prop : this.properties) {
			if (prop.getValue().equalsIgnoreCase("exempt")) {
				
				this.properties.remove(prop);
			}
		}
	}

	public void addWhitelistedArnForDetector(String arnName, String detectorName, UserAccountPojo user) {
		boolean addIt=true;
		propLoop: for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase("srd:" + detectorName + ":" + arnName) &&
				prop.getValue().equalsIgnoreCase("exempt")) {
				
				addIt=false;
				break propLoop;
			}
		}
		
		if (addIt) {
			PropertyPojo prop = new PropertyPojo();
			prop.setName("SRD:" + detectorName + ":" + arnName);
			prop.setValue("exempt");
			prop.setCreateUser(user.getPublicId());
			prop.setCreateTime(new Date(System.currentTimeMillis()));
			this.properties.add(prop);
		}
	}

	public void addWhitelistedArn(String arnName, UserAccountPojo user) {
		boolean addIt=true;
		propLoop: for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase(arnName) &&
				prop.getValue().equalsIgnoreCase("exempt")) {
				
				addIt=false;
				break propLoop;
			}
		}
		
		if (addIt) {
			PropertyPojo prop = new PropertyPojo();
			prop.setName(arnName);
			prop.setValue("exempt");
			this.properties.add(prop);
		}
	}

	public void removeWhitelistedArnForDetector(String arn, String detectorName) {
		propLoop: for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase("srd:" + detectorName + ":" + arn)) {
				this.properties.remove(prop);
				break propLoop;
			}
		}
	}

	public void removeWhitelistedArn(String arn) {
		propLoop: for (PropertyPojo prop : this.properties) {
			if (prop.getName().equalsIgnoreCase(arn)) {
				this.properties.remove(prop);
				break propLoop;
			}
		}
	}

	public List<PropertyPojo> getWhitelistedArnsForDetector(String detectorName) {
		List<PropertyPojo> props = new java.util.ArrayList<PropertyPojo>();

		for (PropertyPojo prop : this.properties) {
			if (prop.getName().toLowerCase().indexOf("arn:") > 0 &&
				prop.getName().indexOf(detectorName) >= 0 &&
				prop.getValue().equalsIgnoreCase("exempt")) {
				
				props.add(prop);
			}
		}
		
		return props;
	}

	public List<PropertyPojo> getWhitelistedArns() {
		List<PropertyPojo> props = new java.util.ArrayList<PropertyPojo>();

		for (PropertyPojo prop : this.properties) {
			if (prop.getName().toLowerCase().indexOf("arn:") >= 0 &&
				prop.getValue().equalsIgnoreCase("exempt")) {
				
				props.add(prop);
			}
		}
		
		return props;
	}
	
	public boolean clearSrdExemptions() {
		
		boolean clearedExemptions=false;
		for (int i=properties.size()-1; i>=0; i--) {
			PropertyPojo prop = properties.get(i);
			if (prop.getName().toLowerCase().indexOf("srd:") == 0 &&
					prop.getName().toLowerCase().indexOf("arn:") < 0 &&
					prop.getValue().equalsIgnoreCase("exempt")) {
					
					GWT.log("Removing whitelisted srd: " + prop.getName());
					this.properties.remove(i);
					clearedExemptions=true;
				}
		}
		return clearedExemptions;
	}
	public boolean clearSrdArnExemptions() {
		boolean clearedExemptions=false;
		for (int i=properties.size()-1; i>=0; i--) {
			PropertyPojo prop = properties.get(i);
			if ((prop.getName().toLowerCase().indexOf("srd:") == 0 &&
					prop.getName().toLowerCase().indexOf("arn:") > 0 &&
					prop.getValue().equalsIgnoreCase("exempt")) ||
				(prop.getName().toLowerCase().indexOf("arn:") == 0 &&
				prop.getValue().equalsIgnoreCase("exempt"))) {
					
					GWT.log("Removing whitelisted srd/arn: " + prop.getName());
					this.properties.remove(i);
					clearedExemptions=true;
				}
		}

		return clearedExemptions;
	}
	
	public boolean removeProperty(String propertyName) {
		boolean didRemove=false;
		for (int i=properties.size()-1; i>=0; i--) {
			PropertyPojo prop = properties.get(i);
			if (prop.getName().equalsIgnoreCase(propertyName)) {
				properties.remove(i);
				didRemove = true;
			}
		}
		return didRemove;
	}
	
	public boolean hasProperty(String propertyName) {
		boolean hasPropery=false;
		if (propertyName == null) {
			return hasPropery;
		}
		for (int i=properties.size()-1; i>=0; i--) {
			PropertyPojo prop = properties.get(i);
			if (prop.getName().equalsIgnoreCase(propertyName.trim())) {
				hasPropery = true;
			}
		}
		return hasPropery;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public AccountExtraMetaDataPojo getExtraMetaData() {
		return extraMetaData;
	}

	public void setExtraMetaData(AccountExtraMetaDataPojo extraMetaData) {
		this.extraMetaData = extraMetaData;
	}
}
