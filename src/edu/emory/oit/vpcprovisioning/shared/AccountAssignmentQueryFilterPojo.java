package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountAssignmentQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter  {
	/*
	<!ELEMENT AccountAssignmentQuerySpecification (PublicId, PrincipalType?, TargetId, TargetType)>
	<!ELEMENT AccountAssignmentQuerySpecification (PublicId?, PermissionSet?, PrincipalType?, TargetId?, TargetType?)>
	*/

	String publicId;
	String principalType;
	String targetId;
	String targetType;
	String permissionSetName;

	public AccountAssignmentQueryFilterPojo() {
		
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}


	public String getPublicId() {
		return publicId;
	}


	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}


	public String getTargetId() {
		return targetId;
	}


	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}


	public String getTargetType() {
		return targetType;
	}


	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}


	public String getPrincipalType() {
		return principalType;
	}


	public void setPrincipalType(String principalType) {
		this.principalType = principalType;
	}


	public String getPermissionSetName() {
		return permissionSetName;
	}


	public void setPermissionSetName(String permissionSet) {
		this.permissionSetName = permissionSet;
	}

}
