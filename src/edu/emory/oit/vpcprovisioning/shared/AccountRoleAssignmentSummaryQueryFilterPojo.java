package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountRoleAssignmentSummaryQueryFilterPojo extends SharedObject implements IsSerializable {

	UserAccountPojo userLoggedIn;
	
	public AccountRoleAssignmentSummaryQueryFilterPojo() {
		// TODO Auto-generated constructor stub
	}

	public UserAccountPojo getUserLoggedIn() {
		return userLoggedIn;
	}

	public void setUserLoggedIn(UserAccountPojo userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

}
