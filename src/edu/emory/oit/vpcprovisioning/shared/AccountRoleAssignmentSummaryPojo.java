package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountRoleAssignmentSummaryPojo extends SharedObject implements IsSerializable {

	List<AccountPojo> accounts = new java.util.ArrayList<AccountPojo>();
	UserAccountPojo user;
	
	public AccountRoleAssignmentSummaryPojo() {
		// TODO Auto-generated constructor stub
	}

	public List<AccountPojo> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountPojo> accounts) {
		this.accounts = accounts;
	}

	public UserAccountPojo getUser() {
		return user;
	}

	public void setUser(UserAccountPojo user) {
		this.user = user;
	}

}
