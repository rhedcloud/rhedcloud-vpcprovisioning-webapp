package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountExtraMetaDataQueryResultPojo extends SharedObject implements IsSerializable {
	AccountExtraMetaDataQueryFilterPojo filterUsed;
	List<AccountExtraMetaDataPojo> results;

	public AccountExtraMetaDataQueryResultPojo() {
	}

	public AccountExtraMetaDataQueryFilterPojo getFilterUsed() {
		return filterUsed;
	}

	public void setFilterUsed(AccountExtraMetaDataQueryFilterPojo filterUsed) {
		this.filterUsed = filterUsed;
	}

	public List<AccountExtraMetaDataPojo> getResults() {
		return results;
	}

	public void setResults(List<AccountExtraMetaDataPojo> results) {
		this.results = results;
	}

}
