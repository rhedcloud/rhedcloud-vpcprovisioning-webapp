package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class UserAccountRolePojo extends SharedObject implements IsSerializable {

	UserAccountPojo user;
	AccountPojo account;
	String roleName;
	
	public UserAccountRolePojo() {
		// TODO Auto-generated constructor stub
	}

	public UserAccountPojo getUser() {
		return user;
	}

	public void setUser(UserAccountPojo user) {
		this.user = user;
	}

	public AccountPojo getAccount() {
		return account;
	}

	public void setAccount(AccountPojo account) {
		this.account = account;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
