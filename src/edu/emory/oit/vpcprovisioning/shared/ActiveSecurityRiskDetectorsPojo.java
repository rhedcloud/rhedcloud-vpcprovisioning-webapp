package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class ActiveSecurityRiskDetectorsPojo extends SharedObject implements IsSerializable, Comparable<ActiveSecurityRiskDetectorsPojo> {

	List<String> detectorNames = new java.util.ArrayList<String>();
	public ActiveSecurityRiskDetectorsPojo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(ActiveSecurityRiskDetectorsPojo o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public List<String> getDetectorNames() {
		return detectorNames;
	}

	public void setDetectorNames(List<String> detectorNames) {
		this.detectorNames = detectorNames;
	}

}
