package edu.emory.oit.vpcprovisioning.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountAssignmentQueryResultPojo extends SharedObject implements IsSerializable {
	AccountAssignmentQueryFilterPojo filterUsed;
	List<AccountAssignmentPojo> results = new java.util.ArrayList<AccountAssignmentPojo>();

	public AccountAssignmentQueryResultPojo() {
		
	}

	public AccountAssignmentQueryFilterPojo getFilterUsed() {
		return filterUsed;
	}

	public void setFilterUsed(AccountAssignmentQueryFilterPojo filterUsed) {
		this.filterUsed = filterUsed;
	}

	public List<AccountAssignmentPojo> getResults() {
		return results;
	}

	public void setResults(List<AccountAssignmentPojo> results) {
		this.results = results;
	}

}
