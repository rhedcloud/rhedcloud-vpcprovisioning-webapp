package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AccountRoleAssignmentSummaryQueryResultPojo extends SharedObject implements IsSerializable {

	AccountRoleAssignmentSummaryQueryFilterPojo filterUsed;
	AccountRoleAssignmentSummaryPojo summary;
	
	public AccountRoleAssignmentSummaryQueryResultPojo() {
		// TODO Auto-generated constructor stub
	}

	public AccountRoleAssignmentSummaryQueryFilterPojo getFilterUsed() {
		return filterUsed;
	}

	public void setFilterUsed(AccountRoleAssignmentSummaryQueryFilterPojo filterUsed) {
		this.filterUsed = filterUsed;
	}

	public AccountRoleAssignmentSummaryPojo getSummary() {
		return summary;
	}

	public void setSummary(AccountRoleAssignmentSummaryPojo summary) {
		this.summary = summary;
	}

}
