package edu.emory.oit.vpcprovisioning.server.filter;

import java.io.IOException;
import java.util.logging.Logger;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletResponse;

public class SecurityHeadersFilter implements Filter {
	private Logger log = Logger.getLogger(getClass().getName());

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
//		info("[SecurityHeadersFilter] doFilter...");
		HttpServletResponse res = (HttpServletResponse) response;
        
        res.setHeader("Strict-Transport-Security", "max-age=63072000");
        res.setHeader("Content-Security-Policy", "frame-ancestors 'none'");
        res.setHeader("X-Frame-Options", "DENY");
        
        // move on...
        chain.doFilter(request, res);
    }

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
}
