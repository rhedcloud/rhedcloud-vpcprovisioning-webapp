package edu.emory.oit.vpcprovisioning.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;

import org.openeai.afa.Schedule;
import org.openeai.afa.ScheduledCommandImpl;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.LoggerConfig;
import org.openeai.dbpool.EnterpriseConnectionPool;
import org.openeai.jms.consumer.MessageConsumer;
import org.openeai.jms.consumer.PointToPointConsumer;
import org.openeai.jms.consumer.PubSubConsumer;
import org.openeai.jms.consumer.commands.ConsumerCommand;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.utils.config.AppConfigFactoryException;
import org.openeai.utils.lock.DbLock;
import org.openeai.utils.lock.LockException;

// to run local (comment out before committing)
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

//to run in AL3 (uncomment before committing)
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class HealthCheck extends HttpServlet {
	private Logger log = Logger.getLogger(getClass().getName());
	private boolean loggingEnabled = false;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long startTime = System.currentTimeMillis();
		boolean foundAppConfig=false;
		String appName = null;
		try {
			Map<String, AppConfig> appConfigs = 
				(Map<String, AppConfig>) VpcProvisioningServiceImpl.
					appConfigFactory.getAppConfigs();
			Iterator<String> it = appConfigs.keySet().iterator();
			while (it.hasNext()) {
				foundAppConfig=true;
				String appId = it.next();
				AppConfig ac = appConfigs.get(appId);
				appName = ac.getAppName();
				traverseAppConfig(ac);
			}
		}
		catch (AppConfigFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!foundAppConfig) {
			String msg = "No AppConfigs found.  Service may not be started.  "
				+ "Check the config doc of the service.  Processing "
				+ "cannot continue.";
			loggingEnabled=true;
			info(msg);
			throw new ServletException(msg);
		}
		// return some sort of 'relevant' response...
		String text = "Health Check Status:  OK";
		if (appName != null) {
			text = appName + ": " + text; 
		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = endTime - startTime;
		info(text + "  Elapsed time:  " + elapsedTime + " milliseconds.");

		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(text);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void traverseAppConfig(AppConfig ac) throws ServletException {
		Iterator<String> acKeys = ac.getObjects().keySet().iterator();
		Properties healthCheckProps = null;
		boolean loggedNoHcProps=false;
		boolean forceFailure=false;
		while (acKeys.hasNext()) {
			try {
				healthCheckProps = ac.getProperties("HealthCheckProperties");
				String sLoggingEnabled = healthCheckProps.getProperty("loggingEnabled", "false");
				loggingEnabled = Boolean.parseBoolean(sLoggingEnabled);
				String sForceFailure = healthCheckProps.getProperty("forceFailure", "false");
				forceFailure = Boolean.parseBoolean(sForceFailure);
				if (forceFailure) {
					String msg = "The 'forceFailure' property is true.  "
						+ "Forcing a health check failure.";
					info(msg);
					throw new ServletException(msg);
				}
			}
			catch (EnterpriseConfigurationObjectException e1) {
				if (!loggedNoHcProps) {
					loggedNoHcProps=true;
				}
			}
			String keyName = acKeys.next();
            Object obj = ac.getObjects().get(keyName);
            if (obj instanceof org.openeai.config.LoggerConfig) {
            	LoggerConfig lc = (LoggerConfig)obj;
            	Iterator<Object> lcKeys = lc.getProperties().keySet().iterator();
            	while (lcKeys.hasNext()) {
            		Object lcKey = lcKeys.next();
            		if (lcKey instanceof String) {
            			String skey = (String)lcKey;
            			if (skey.toLowerCase().endsWith("filename")) {
            				String fileName = lc.getProperties().getProperty(skey);
            				// pass the file name to some sort of 
            				// log analyzer thing that looks for oddities in the 
            				// log that might indicate issues.
            				// e.g., a bunch of "ThreadPool is busy" messages
            				// it could vary based on the service we're running
            				// we could add properties to the service's AppConfig
            				// that would drive what we're looking for...
            				
            				// go through healthCheckProps and find any logger related
            				// instructions
            				if (healthCheckProps != null) {
            					Iterator<Object> hckeys = healthCheckProps.keySet().iterator();
            					while (hckeys.hasNext()) {
            						String logkey = (String)hckeys.next();
            						// First:  look for a key phrase, e.g., "threadpool is busy"
            						// see how many times that phrase shows up in the log
            						// if it shows up more than the maximum allowed 
            						// percentage, there's an issue.
            						if (logkey != null && 
            							logkey.toLowerCase().indexOf("log.healthcheck") == 0) {
            							String[] loghcValues = 
            								healthCheckProps.getProperty(logkey).split(":");
            							
            							// confirm there are 2 items in the string array
            							// confirm item 0 is a string and item 1 is a number
            							if (loghcValues != null && loghcValues.length == 2) {
                							String searchString = loghcValues[0].trim();
                							long maxPct = 0;
                							try {
                    							maxPct = Long.parseLong(loghcValues[1].trim());
                							}
                							catch (NumberFormatException e) {
                								boolean currentLoggingEnabled=loggingEnabled;
                								loggingEnabled=true;
                								info("Configuration error: the "
               										+ "value '" 
                									+ loghcValues[1].trim() 
                									+ "' is NOT numeric.  Cannot "
                									+ "continue.  Check the "
                									+ "configuration.");
                								loggingEnabled=currentLoggingEnabled;
                								continue;
                							}

                            				try {
                								long pctFound = scanFile(fileName, searchString);
                								if (pctFound > maxPct) {
                									// something is probably wrong
                    								boolean currentLoggingEnabled=loggingEnabled;
                    								loggingEnabled=true;
                									String msg = "The string '" 
                   										+ searchString 
                   										+ "' was found in more than " 
                   										+ maxPct + " percent of the "
       													+ "log (it was found in " 
                   										+ pctFound + " percent of "
           												+ "the log).  There may "
           												+ "be issues with the service."; 
                									info(msg);
                    								loggingEnabled=currentLoggingEnabled;
                									throw new ServletException(msg);
                								}
                							}
                							catch (FileNotFoundException e) {
                								boolean currentLoggingEnabled=loggingEnabled;
                								loggingEnabled=true;
                								info("The log file " + fileName + " could not be found.");
                								loggingEnabled=currentLoggingEnabled;
                								e.printStackTrace();
                							}
            							}
            							else {
            								// configuration error
            								boolean currentLoggingEnabled=loggingEnabled;
            								loggingEnabled=true;
            								info("Configuration error:  the "
           										+ "value for " + logkey + " ('" 
            									+ healthCheckProps.getProperty(logkey) 
            									+ "') does not appear to follow "
            									+ "the proper naming convention "
            									+ "for this logic (log analysis)."
            									+ "  Check the config doc.");
            								loggingEnabled=currentLoggingEnabled;
            							}
            						}
            					}
            				}
            			}
            		}
            	}
            }
            if (obj instanceof org.openeai.afa.ScheduledApp) {
                org.openeai.afa.ScheduledApp sa = (org.openeai.afa.ScheduledApp) obj;
                // need to recursively check the appconfigs in all
                // the commands etc.
                Iterator<String> saKeys = sa.getSchedules().keySet().iterator();
                while (saKeys.hasNext()) {
                	String saKey = saKeys.next();
                	Schedule s = (Schedule) sa.getSchedules().get(saKey);
                	Iterator<String> scmdKeys = s.getCommands().keySet().iterator();
                	while (scmdKeys.hasNext()) {
                		String scmdKey = scmdKeys.next();
                		ScheduledCommandImpl sc = (ScheduledCommandImpl) s.getCommand(scmdKey);
                		traverseAppConfig(sc.getAppConfig());
                	}
                }
            }
            if (obj instanceof org.openeai.jms.consumer.MessageConsumer) {
            	// if it's a consumer, we need to get in some sort of 
            	// recursive loop thing to check the app configs of all
            	// the commands and stuff
                MessageConsumer mc = (MessageConsumer) obj;
                checkConsumer(mc);
            }
            if (obj instanceof org.openeai.jms.producer.MessageProducer) {
                MessageProducer mp = (MessageProducer) obj;
                checkProducer(mp);
            }
            if (obj instanceof org.openeai.jms.producer.ProducerPool) {
                ProducerPool p = (ProducerPool) obj;
                java.util.List producers = p.getProducers();
                for (int i = 0; i < producers.size(); i++) {
                    MessageProducer mp = (MessageProducer) producers.get(i);
                    checkProducer(mp);
                }
            }
            if (obj instanceof org.openeai.moa.XmlEnterpriseObject) {
            }
            if (obj instanceof org.openeai.utils.lock.DbLock) {
            	DbLock dbLock = (DbLock)obj;
            	try {
					dbLock.count();
				}
				catch (LockException e) {
					e.printStackTrace();
					throw new ServletException(e);
				}
            }
            if (obj instanceof org.openeai.dbpool.EnterpriseConnectionPool) {
            	EnterpriseConnectionPool dbPool = (EnterpriseConnectionPool)obj;
            	try {
            		dbPool.getConnection();
				}
				catch (SQLException e) {
					e.printStackTrace();
					throw new ServletException(e);
				}
            }
		}
	}

	private void info(String msg) {
		if (loggingEnabled) {
			log.info(msg);
		}
	}
	
	
	public void checkProducer(MessageProducer mp) throws ServletException {
        if (mp instanceof PointToPointProducer) {
        	PointToPointProducer ppp = (PointToPointProducer)mp;
        	try {
				TemporaryQueue tq = ppp.getQueueSession().createTemporaryQueue();
				tq.delete();
				if (ppp.getProducerStatus() != null && 
					ppp.getProducerStatus().equalsIgnoreCase("started") == false) {
						// producer is reporting as healthy but it's not "started"
						// this could indicate an issue of some sort
						info("Producer " + ppp.getProducerName() + " is "
							+ "reporting as 'healthy' but the producer thinks "
							+ "it's NOT started.  This could be an issue.");
				}
				return;
			}
			catch (JMSException e) {
				e.printStackTrace();
				try {
					ppp.restartProducer();
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
				throw new ServletException(e);
			}
        }
        // PubSubProducer
        if (mp instanceof PubSubProducer) {
        	PubSubProducer psp = (PubSubProducer)mp;
        	try {
				TemporaryTopic tq = psp.getTopicSession().createTemporaryTopic();
				tq.delete();
				if (psp.getProducerStatus() != null && 
					psp.getProducerStatus().equalsIgnoreCase("started") == false) {
						// producer is reporting as healthy but it's not "started"
						// this could indicate an issue of some sort
						info("Producer " + psp.getProducerName() + " is "
							+ "reporting as 'healthy' but the producer thinks "
							+ "it's NOT started.  This could be an issue.");
				}
				return;
			}
			catch (JMSException e) {
				e.printStackTrace();
				try {
					psp.restartProducer();
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
				throw new ServletException(e);
			}
        }
	}
	@SuppressWarnings("unchecked")
	public void checkConsumer(MessageConsumer mc) throws ServletException {
        if (mc instanceof PointToPointConsumer) {
        	PointToPointConsumer ppc = (PointToPointConsumer)mc;
        	try {
        		// check listener status
				TemporaryQueue tq = ppc.getListenerQueueSession().createTemporaryQueue();
				tq.delete();
			}
			catch (JMSException e) {
				e.printStackTrace();
				throw new ServletException(e);
			}
        	Iterator<String> requestCommandKeys = ppc.getRequestCommandMap().keySet().iterator();
        	while (requestCommandKeys.hasNext()) {
        		String rcKey = requestCommandKeys.next();
        		ConsumerCommand rc = (ConsumerCommand)ppc.getRequestCommandMap().get(rcKey);
        		if (rc != null) {
            		AppConfig commandAppConfig = rc.getAppConfig();
            		commandAppConfig.setAppName(rcKey);
            		traverseAppConfig(commandAppConfig);
        		}
        	}
        }
        // PubSubConsumer
        if (mc instanceof PubSubConsumer) {
        	PubSubConsumer psc = (PubSubConsumer)mc;
        	try {
				TemporaryTopic tq = psc.getTopicSession().createTemporaryTopic();
				tq.delete();
			}
			catch (JMSException e) {
				e.printStackTrace();
				throw new ServletException(e);
			}
        	Iterator<String> syncCommandKeys = psc.getSyncCommandMap().keySet().iterator();
        	while (syncCommandKeys.hasNext()) {
        		String rcKey = syncCommandKeys.next();
        		ConsumerCommand rc = (ConsumerCommand)psc.getSyncCommandMap().get(rcKey);
        		if (rc != null) {
            		AppConfig commandAppConfig = rc.getAppConfig();
            		commandAppConfig.setAppName(rcKey);
            		traverseAppConfig(commandAppConfig);
        		}
        	}
        }
	}
	public long scanFile(String fileName,String searchStr) throws FileNotFoundException{
		double totalLines=0;
		double matchingLines=0;
        Scanner scan = new Scanner(new File(fileName));
        while(scan.hasNext()){
        	totalLines++;
            String line = scan.nextLine().toLowerCase().toString();
            if(line.contains(searchStr.toLowerCase())){
                matchingLines++;
            }
        }
		long pctFound = Math.round(matchingLines  * 100 / totalLines);
        return pctFound;
    }
}
