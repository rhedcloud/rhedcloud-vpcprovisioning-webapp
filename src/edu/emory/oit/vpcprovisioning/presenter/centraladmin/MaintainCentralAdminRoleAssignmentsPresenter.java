package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountRolePojo;

public class MaintainCentralAdminRoleAssignmentsPresenter extends PresenterBase implements MaintainCentralAdminRoleAssignmentsView.Presenter {

	private static final Logger log = Logger.getLogger(MaintainCentralAdminRoleAssignmentsPresenter.class.getName());
	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	AccountRoleAssignmentSummaryQueryFilterPojo filter;
	AccountRoleAssignmentSummaryPojo summary;
	AccountRoleAssignmentSummaryPojo filteredList;
	UserAccountPojo userBeingMaintained;
	UserAccountPojo userLoggedIn;
	List<UserAccountRolePojo> cachedAssignments = new java.util.ArrayList<UserAccountRolePojo>();
	List<UserAccountRolePojo> cachedUnassignments = new java.util.ArrayList<UserAccountRolePojo>();

	/**
	 * The refresh timer used to periodically refresh the account list.
	 */
	//	  private Timer refreshTimer;

	/**
	 * Periodically "touch" HTTP session so they won't have to re-authenticate
	 */
	//	  private Timer sessionTimer;

	public MaintainCentralAdminRoleAssignmentsPresenter(ClientFactory clientFactory, boolean clearList, AccountRoleAssignmentSummaryQueryFilterPojo filter, UserAccountPojo userLoggedIn, UserAccountPojo centralAdminBeingMaintained) {
		this.clientFactory = clientFactory;
		this.filter = filter;
		this.userLoggedIn = userLoggedIn;
		this.userBeingMaintained = centralAdminBeingMaintained;
		clientFactory.getMaintainCentralAdminRoleAssignmentsView().setPresenter(this);
	}

	/**
	 * Construct a new {@link MaintainCentralAdminRoleAssignmentsPresenter}.
	 * 
	 * @param clientFactory the {@link ClientFactory} of shared resources
	 * @param place configuration for this activity
	 */
	public MaintainCentralAdminRoleAssignmentsPresenter(ClientFactory clientFactory, MaintainCentralAdminRoleAssignmentsPlace place) {
		this(clientFactory, false, place.getFilter(), place.getUserLoggedIn(), place.getCentralAdminBeingMaintained());
	}

	private MaintainCentralAdminRoleAssignmentsView getView() {
		return clientFactory.getMaintainCentralAdminRoleAssignmentsView();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		final long startTime = System.currentTimeMillis();
		cachedAssignments = new java.util.ArrayList<UserAccountRolePojo>();
		cachedUnassignments = new java.util.ArrayList<UserAccountRolePojo>();

		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		this.eventBus = eventBus;
		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving User Logged In...");
		
		getView().enableButtons();
		clientFactory.getShell().setTitle("VPC Provisioning App");
		clientFactory.getShell().setSubTitle("Central Admins");

		getView().setUserLoggedIn(userLoggedIn);
		getView().setUserBeingMaintained(userBeingMaintained);
		if (userBeingMaintained != null && 
			userBeingMaintained.getPublicId() != null && 
			userBeingMaintained.getPublicId().equalsIgnoreCase(userLoggedIn.getPublicId())) {
			
			// maintaining ourselves, allow changes
			GWT.log("[MaintainCentralAdminRoleAssignmentsPresenter].start maintaining ourselves, allow changes=true");
			getView().setAllowChanges(true);
		}
		else {
			if (userLoggedIn.isCentralAdminManager()) {
				// we're viewing someone else but we're a CAM so allow changes
				GWT.log("[MaintainCentralAdminRoleAssignmentsPresenter].start maintaining someone else, CAM=true, allow changes=true");
				getView().setAllowChanges(true);
			}
			else {
				// we're viewing someone else and we're not a CAM so we can't
				// make changes to other people
				GWT.log("[MaintainCentralAdminRoleAssignmentsPresenter].start maintaining someone else, CAM=false, allow changes=false");
				getView().setAllowChanges(false);
			}
		}
		getView().initPage();

		// refresh the roleassignment list now.
		refreshList(userBeingMaintained);
		
		long endTime = System.currentTimeMillis();
		long elapsedTime = endTime - startTime;
		GWT.log("[MaintainCentralAdminRoleAssignmentsPresenter].start elapsed time: " + elapsedTime + " milliseconds.");
	}

	public void refreshList(final UserAccountPojo user) {
		getView().showPleaseWaitDialog("Retrieving Account/Role Assignments from the IDM Service...");
		// use RPC to get all accounts for the current filter being used
		AsyncCallback<AccountRoleAssignmentSummaryQueryResultPojo> callback = new AsyncCallback<AccountRoleAssignmentSummaryQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				log.log(Level.SEVERE, "Exception Retrieving Central Admins", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your list of accounts.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(AccountRoleAssignmentSummaryQueryResultPojo result) {
//				GWT.log("Got " + result.size() + " central admins back from server.");
				if (result != null) {
					summary = result.getSummary();
					getView().setAccountRoleAssignmentSummary(summary);
				}
				else {
					// error
					getView().showMessageToUser("RoleAssignment summary from the server is null.");
					return;
				}
				
				// apply authorization mask
				if (user.isCentralAdmin()) {
					getView().applyCentralAdminMask();
				}
				else {
					getView().showMessageToUser("An error has occurred.  The user logged in does not "
							+ "appear to be associated to any valid roles required to view this data.");
					getView().applyAWSAccountAuditorMask();
					// TODO: need to not show them the list of accounts???
				}
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
			}
		};

		GWT.log("refreshing account/role assignment summary list...");
		if (filter == null) {
			filter = new AccountRoleAssignmentSummaryQueryFilterPojo();
		}
		filter.setUserLoggedIn(user);
		VpcProvisioningService.Util.getInstance().getAccountRoleAssignmentSummaryForFilter(filter, callback);
	}

	@Override
	public void stop() {
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void assignUserToRoleInAccount(final UserAccountPojo user, final String roleName, final AccountPojo account) {
		AsyncCallback<RoleAssignmentPojo> raCallback = new AsyncCallback<RoleAssignmentPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Error during unassignment", caught);
				getView().showStatus("There was an exception on the " +
						"server creating the role assignment.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(final RoleAssignmentPojo roleAssignment) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().showStatus("Assigned " + user.getFullName() + 
					" (" + user.getPublicId() + ") to the " + 
					roleName + " role in account #" + account.getAccountId());
			}
		};
		// now, create the role assignment and add the role assignment to the account
//		getView().showStatus("Creating Role Assignment(s) with the IDM service in the background, you'll be notified when it is complete...");
		VpcProvisioningService.Util.getInstance().createRoleAssignmentForPersonInAccount(user.getPublicId(), account.getAccountId(), roleName, raCallback);
		
	}

	@Override
	public void unassignUserFromRoleInAccount(final UserAccountPojo user, final String roleName, final AccountPojo account) {
		// RoleAssignment.Delete
		// THEN
		// Remove the AccountRolePojo from user
		// THEN
		// update the user on the server side cache
		// This will avoid having to get all the roles for the user on the 
		// server time which will be an order of maginitude faster
		
		AsyncCallback<Void> raCallback = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Error during unassignment", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server removing the role assignment for " + 
						user.getFullName() + " from the " + roleName + 
						" role in account #" + account.getAccountId() + 
						".  Message from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Void result) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				
				getView().showStatus("Removed " + user.getFullName() + 
						" (" + user.getPublicId() + ") from the " + 
						roleName + " role in account #" + account.getAccountId());
			}
		};
//		getView().showStatus("Deleting Role Assignment(s) with the IDM service in the background...");
		VpcProvisioningService.Util.getInstance().removeRoleAssignmentForPersonInAccount(account, user, roleName, raCallback);
	}

	@Override
	public void filterByText(String filterBeingTyped) {
		filteredList = new AccountRoleAssignmentSummaryPojo();
		filteredList.setUser(userBeingMaintained);
		filteredList.setAccounts(new java.util.ArrayList<AccountPojo>());
		for (AccountPojo acct : summary.getAccounts()) {
			if (acct.getAccountId() != null && 
				acct.getAccountId().indexOf(filterBeingTyped) >= 0) {
				
				filteredList.getAccounts().add(acct);
			}
			else if (acct.getAccountName() != null && 
				acct.getAccountName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.getAccounts().add(acct);
			}
			else if (acct.getAlternateName() != null && 
					acct.getAlternateName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
					
					filteredList.getAccounts().add(acct);
			}
			else if (acct.getAccountOwnerDirectoryMetaData() != null &&
					acct.getAccountOwnerDirectoryMetaData().getFirstName() != null &&
					acct.getAccountOwnerDirectoryMetaData().getFirstName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.getAccounts().add(acct);
			}
			else if (acct.getAccountOwnerDirectoryMetaData() != null &&
					acct.getAccountOwnerDirectoryMetaData().getLastName() != null &&
					acct.getAccountOwnerDirectoryMetaData().getLastName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.getAccounts().add(acct);
			}
		}
		getView().setAccountRoleAssignmentSummary(filteredList);
		if (filteredList.getAccounts().size() == 0) {
			getView().showNoResultsMessage();
		}
		else {
			getView().hideNoResultsMessage();
		}
	}

	@Override
	public void cacheAccountRoleAssignment(UserAccountRolePojo uar) {
		boolean cacheit = true;
		uarLoop:  for (UserAccountRolePojo u : cachedUnassignments) {
			if (u.getAccount().getAccountId().equalsIgnoreCase(uar.getAccount().getAccountId()) && 
				u.getRoleName().equalsIgnoreCase(uar.getRoleName())) {
				
				GWT.log(uar.getRoleName() + " was unassigned in this session, just removing it from the unassignment cache");
				cachedUnassignments.remove(u);
				cacheit = false;
				break uarLoop;
			}
		}
		if (cacheit) {
			GWT.log("caching role assignment for role " + uar.getRoleName());
			cachedAssignments.add(uar);
		}
		GWT.log("cached assignment size is now " + cachedAssignments.size());
		GWT.log("cached UN-assignment size is now " + cachedUnassignments.size());
	}

	@Override
	public void cacheAccountRoleUnassignment(UserAccountRolePojo uar) {
		// check the assignment cache and see if this one is in it, 
		// if it is, just remove it from the cache and don't cache the unassignment
		// because it shouldn't be necessary
		boolean cacheit = true;
		uarLoop:  for (UserAccountRolePojo u : cachedAssignments) {
			if (u.getAccount().getAccountId().equalsIgnoreCase(uar.getAccount().getAccountId()) && 
				u.getRoleName().equalsIgnoreCase(uar.getRoleName())) {
				
				GWT.log(uar.getRoleName() + " was assigned in this session, just removing it from the assignment cache");
				cachedAssignments.remove(u);
				cacheit = false;
				break uarLoop;
			}
		}
		if (cacheit) {
			GWT.log("caching role UN-assignment for role " + uar.getRoleName());
			cachedUnassignments.add(uar);
		}
		GWT.log("cached assignment size is now " + cachedAssignments.size());
		GWT.log("cached UN-assignment size is now " + cachedUnassignments.size());
	}

	@Override
	public void saveCachedChanges() {
		// assignments first
		
		getView().showStatus("Potential long running task.  Saving the Role "
			+ "Assignment(s) / Unassignment(s) "
			+ "in the background, you'll be notified "
			+ "when processing is complete...");
		
		GWT.log("there are " + cachedAssignments.size() + " assignments to process");
		for (UserAccountRolePojo u : cachedAssignments) {
			this.assignUserToRoleInAccount(u.getUser(), u.getRoleName(), u.getAccount());
		}
		GWT.log("there are " + cachedUnassignments.size() + " UN-assignments to process");
		for (UserAccountRolePojo u : cachedUnassignments) {
			this.unassignUserFromRoleInAccount(u.getUser(), u.getRoleName(), u.getAccount());
		}
		ActionEvent.fire(getEventBus(), ActionNames.GO_HOME_CENTRAL_ADMIN, userBeingMaintained);
	}
}
