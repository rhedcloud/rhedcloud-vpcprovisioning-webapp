package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.presenter.View;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.ui.client.PresentsWidgets;

public interface ManageSrdBehaviorView extends IsWidget, View {
	/**
	 * The presenter for this view.
	 */
	public interface Presenter extends PresentsWidgets {
		public EventBus getEventBus();
		public ClientFactory getClientFactory();
		public void logMessageOnServer(final String message);
		void refreshList(final UserAccountPojo user);
		void filterByText(String filterBeingTyped);
//		void cacheAccount(AccountPojo account);
//		void saveCachedChanges();
		void saveAccount(AccountPojo account, boolean applyToOthers, boolean refreshPage);
		void parseAndApplyArnExemptionsForSrd(String arnString, String detectorName);
		void initializeSrdArnPropertiesForExistingAccounts();
		void clearSrdArnExemptionsForAllAccounts();
		void clearSrdExemptionsForAllAccounts();
	}

	/**
	 * Sets the new presenter, and calls {@link Presenter#stop()} on the previous
	 * one.
	 */
	void setPresenter(Presenter presenter);
	void setReleaseInfo(String releaseInfoHTML);
	void initPage();
	void setAllowChanges(boolean allow);
	boolean isAllowChanges();
	void setAccounts(List<AccountPojo> accounts);
	void setActiveDetectorNames(List<String> detectorNames);
	void applyAccountWhitelistsToOtherAccounts(AccountPojo sourceAccount);
	void selectAccountsToApplyArnExemption(List<AccountPojo> accounts, String detectorName, String arn);
	void showInitializeAccountSrdArdPropertiesButton();
	void hideInitializeAccountSrdArdPropertiesButton();
	void showOrUpdateRunningStatusPanel(String message);
	void hideRunningStatusPanel();
	void showNoResultsMessage();
	void hideNoResultsMessage();
}
