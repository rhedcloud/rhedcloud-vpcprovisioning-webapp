package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class MaintainCentralAdminRoleAssignmentsPlace extends Place {

	UserAccountPojo userLoggedIn;
	UserAccountPojo centralAdminBeingMaintained;
	AccountRoleAssignmentSummaryQueryFilterPojo filter;

	@Prefix(Constants.MAINTAIN_CENTRAL_ADMIN_ROLE_ASSIGNMENTS)
	public static class Tokenizer implements PlaceTokenizer<MaintainCentralAdminRoleAssignmentsPlace> {

		public MaintainCentralAdminRoleAssignmentsPlace getPlace(String token) {
			return new MaintainCentralAdminRoleAssignmentsPlace();
		}

		public String getToken(MaintainCentralAdminRoleAssignmentsPlace place) {
			return "";
		}
	}

	public MaintainCentralAdminRoleAssignmentsPlace() {
		// TODO Auto-generated constructor stub
	}

	public UserAccountPojo getUserLoggedIn() {
		return userLoggedIn;
	}

	public void setUserLoggedIn(UserAccountPojo userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

	public AccountRoleAssignmentSummaryQueryFilterPojo getFilter() {
		return filter;
	}

	public void setFilter(AccountRoleAssignmentSummaryQueryFilterPojo filter) {
		this.filter = filter;
	}

	public UserAccountPojo getCentralAdminBeingMaintained() {
		return centralAdminBeingMaintained;
	}

	public void setCentralAdminBeingMaintained(UserAccountPojo centralAdminBeingMaintained) {
		this.centralAdminBeingMaintained = centralAdminBeingMaintained;
	}

}
