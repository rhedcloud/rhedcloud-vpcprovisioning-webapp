package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import edu.emory.oit.vpcprovisioning.shared.Constants;

public class ManageSrdBehaviorPlace extends Place {

	@Prefix(Constants.MANAGE_SRD_BEHAVIOR)
	public static class Tokenizer implements PlaceTokenizer<ManageSrdBehaviorPlace> {

		public ManageSrdBehaviorPlace getPlace(String token) {
			return new ManageSrdBehaviorPlace();
		}

		public String getToken(ManageSrdBehaviorPlace place) {
			return "";
		}
	}

	public ManageSrdBehaviorPlace() {
		// TODO Auto-generated constructor stub
	}
}
