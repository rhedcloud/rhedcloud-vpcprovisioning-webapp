package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.client.event.CentralAdminListUpdateEvent;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.IdentityCenterSyncPojo;
import edu.emory.oit.vpcprovisioning.shared.IdentityCenterSyncSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class ListCentralAdminPresenter extends PresenterBase implements ListCentralAdminView.Presenter {

	private static final Logger log = Logger.getLogger(ListCentralAdminPresenter.class.getName());
	/**
	 * A boolean indicating that we should clear the account list when started.
	 */
	private final boolean clearList;

	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	RoleAssignmentQueryFilterPojo filter;
	RoleAssignmentSummaryPojo centralAdmin;
	UserAccountPojo userLoggedIn;

	/**
	 * The refresh timer used to periodically refresh the account list.
	 */
	//	  private Timer refreshTimer;

	/**
	 * Periodically "touch" HTTP session so they won't have to re-authenticate
	 */
	//	  private Timer sessionTimer;

	public ListCentralAdminPresenter(ClientFactory clientFactory, boolean clearList, RoleAssignmentQueryFilterPojo filter) {
		this.clientFactory = clientFactory;
		this.clearList = clearList;
		clientFactory.getListCentralAdminView().setPresenter(this);
	}

	/**
	 * Construct a new {@link ListCentralAdminPresenter}.
	 * 
	 * @param clientFactory the {@link ClientFactory} of shared resources
	 * @param place configuration for this activity
	 */
	public ListCentralAdminPresenter(ClientFactory clientFactory, ListCentralAdminPlace place) {
		this(clientFactory, place.isListStale(), place.getFilter());
	}

	private ListCentralAdminView getView() {
		return clientFactory.getListCentralAdminView();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		this.eventBus = eventBus;
		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving User Logged In...");
		
		AsyncCallback<String> myNetIdCallback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().setMyNetIdURL("Exception getting MyNETId URL from Server: " + caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				getView().setMyNetIdURL(result);
			}
		};
		VpcProvisioningService.Util.getInstance().getMyNetIdURL(myNetIdCallback);

		refreshIicState();
		
		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				log.log(Level.SEVERE, "Exception Retrieving Central Admins", caught);
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().disableButtons();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the Central Admins you're associated to.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(final UserAccountPojo userLoggedIn) {
				getView().enableButtons();
				clientFactory.getShell().setTitle("VPC Provisioning App");
				clientFactory.getShell().setSubTitle("Central Admins");

				// Clear the account list and display it.
				if (clearList) {
					getView().clearList();
				}

				getView().setUserLoggedIn(userLoggedIn);
				getView().initPage();
//				setCentralAdminList(Collections.<RoleAssignmentSummaryPojo> emptyList());

				// Request the account list now.
				refreshList(userLoggedIn);
			}
		};
		GWT.log("getting user logged in from server...");
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(false, userCallback);
	}

	/**
	 * Refresh the CIDR list.
	 */
	public void refreshList(final UserAccountPojo user) {
		getView().showPleaseWaitDialog("Retrieving Central Administrators from the IDM Service...");
		// use RPC to get all accounts for the current filter being used
		AsyncCallback<List<RoleAssignmentSummaryPojo>> callback = new AsyncCallback<List<RoleAssignmentSummaryPojo>>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				log.log(Level.SEVERE, "Exception Retrieving Central Admins", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your list of accounts.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(List<RoleAssignmentSummaryPojo> result) {
				GWT.log("Got " + result.size() + " central admins back from server.");
				setCentralAdminList(result);
				// apply authorization mask
				if (user.isCentralAdmin()) {
					getView().applyCentralAdminMask();
				}
				else {
					getView().showMessageToUser("An error has occurred.  The user logged in does not "
							+ "appear to be associated to any valid roles required to view this data.");
					getView().applyAWSAccountAuditorMask();
					// TODO: need to not show them the list of accounts???
				}
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
			}
		};

		GWT.log("refreshing Central Admin list...");
		if (filter == null) {
			filter = new RoleAssignmentQueryFilterPojo();
		}
//		filter.setUserLoggedIn(user);
		VpcProvisioningService.Util.getInstance().getCentralAdmins(callback);
	}

	/**
	 * Set the list of accounts.
	 */
	private void setCentralAdminList(List<RoleAssignmentSummaryPojo> centralAdmins) {
		getView().setCentralAdmins(centralAdmins);
		if (eventBus != null) {
			eventBus.fireEventFromSource(new CentralAdminListUpdateEvent(centralAdmins), this);
		}
	}

	@Override
	public void stop() {
		
		
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	@Override
	public void selectCentralAdmin(RoleAssignmentSummaryPojo selected) {
		this.centralAdmin = selected;
		// TODO fire view/edit account action maybe
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public RoleAssignmentQueryFilterPojo getFilter() {
		return filter;
	}

	public void setFilter(RoleAssignmentQueryFilterPojo filter) {
		this.filter = filter;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void deleteCentralAdmin(final RoleAssignmentSummaryPojo centralAdmin) {
	}

	@Override
	public void maintainCentralAdminRole(final UserAccountPojo userLoggedIn, RoleAssignmentSummaryPojo ras) {
		this.userLoggedIn = userLoggedIn;
		getView().showPleaseWaitDialog("Getting user information for selected central admin...");
		AsyncCallback<UserAccountPojo> raCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Error during unassignment", caught);
			}

			@Override
			public void onSuccess(UserAccountPojo result) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				ActionEvent.fire(getEventBus(), 
					ActionNames.MAINTAIN_CENTRAL_ADMIN_ROLE_ASSIGNMENTS, 
					userLoggedIn, 
					result);
			}
		};
		VpcProvisioningService.Util.getInstance().getUserAccountForRoleAssignmentSummary(ras, raCallback);
	}

	@Override
	public void refreshIicState() {
		AsyncCallback<IdentityCenterSyncSummaryPojo> cb = new AsyncCallback<IdentityCenterSyncSummaryPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("[refreshIicState] onFailure");
				HTML status = new HTML("IIC Migration/Sync status Unknown/Error");
				status.getElement().getStyle().setColor("red");
				status.getElement().getStyle().setFontSize(1.2, Unit.EM);
				status.setTitle("Error obtaining status from server.  Something isn't right.  " + caught.getMessage());
				getView().setIicStatus(status);
			}

			@Override
			public void onSuccess(IdentityCenterSyncSummaryPojo result) {
				GWT.log("[refreshIicState] onSuccess");
				String migrationState = "<span style='color:red;'>unknown</span>";
				String syncState = "<span style='color:red;'>unknown</span>";
				String ecsSyncState = "<span style='color:red;'>unknown</span>";
				for (IdentityCenterSyncPojo pojo : result.getIicStatusList()) {
					if (pojo.getType().equalsIgnoreCase("migrate")) {
						// migration
						if (pojo.getStatus().equalsIgnoreCase("running")) {
							migrationState = "<span style='color:green;'>running</span>";
						}
						else {
							migrationState = "<span style='color:red;'>not running</span>";
						}
					}
					else if (pojo.getClassification().equalsIgnoreCase("standard")) {
						// standard sync
						if (pojo.getStatus().equalsIgnoreCase("running")) {
							syncState = "<span style='color:green;'>running</span>";
						}
						else {
							syncState = "<span style='color:red;'>not running</span>";
						}
					}
					else {
						// ecs iic sync
						if (pojo.getStatus().equalsIgnoreCase("running")) {
							ecsSyncState = "<span style='color:green;'>running</span>";
						}
						else {
							ecsSyncState = "<span style='color:red;'>not running</span>";
						}
					}
				}
				
				HTML status = new HTML(
					"IIC Migration status:  " + migrationState + "<br>"   
					+ "IIC Sync status:  " + syncState + "<br>"
					+ "ECS ICC Sync status: " + ecsSyncState);
				status.getElement().getStyle().setFontSize(1.2, Unit.EM);
				getView().setIicStatus(status);
			}
		};
		VpcProvisioningService.Util.getInstance().getIicStatusSummary(cb);
	}
}
