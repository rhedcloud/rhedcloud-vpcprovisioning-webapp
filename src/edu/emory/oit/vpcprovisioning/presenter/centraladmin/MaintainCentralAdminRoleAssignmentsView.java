package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.presenter.View;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountRolePojo;
import edu.emory.oit.vpcprovisioning.ui.client.PresentsWidgets;

public interface MaintainCentralAdminRoleAssignmentsView extends IsWidget, View {
	/**
	 * The presenter for this view.
	 */
	public interface Presenter extends PresentsWidgets {
//		void selectCentralAdmin(RoleAssignmentSummaryPojo selected);
		public EventBus getEventBus();
		public ClientFactory getClientFactory();
//		void deleteCentralAdmin(RoleAssignmentSummaryPojo account);
		public void logMessageOnServer(final String message);
		void refreshList(final UserAccountPojo user);

		public void assignUserToRoleInAccount(UserAccountPojo user, String roleName, AccountPojo account);
		public void unassignUserFromRoleInAccount(UserAccountPojo user, String roleName, AccountPojo account);
		void filterByText(String filterBeingTyped);
		void cacheAccountRoleAssignment(UserAccountRolePojo uar);
		void cacheAccountRoleUnassignment(UserAccountRolePojo uar);
		void saveCachedChanges();
	}

	/**
	 * Sets the new presenter, and calls {@link Presenter#stop()} on the previous
	 * one.
	 */
	void setPresenter(Presenter presenter);
	void setReleaseInfo(String releaseInfoHTML);
	void initPage();
	void setAccountRoleAssignmentSummary(AccountRoleAssignmentSummaryPojo summary);
	void setAllowChanges(boolean allow);
	boolean isAllowChanges();
	void setUserBeingMaintained(UserAccountPojo user);
	void showNoResultsMessage();
	void hideNoResultsMessage();
}
