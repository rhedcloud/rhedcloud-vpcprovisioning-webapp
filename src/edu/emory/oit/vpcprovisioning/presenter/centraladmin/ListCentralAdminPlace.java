package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentQueryFilterPojo;

public class ListCentralAdminPlace extends Place {

	/**
	 * The tokenizer for this place. case recordList doesn't have any state, so we don't
	 * have anything to encode.
	 */
	@Prefix(Constants.LIST_CENTRAL_ADMIN)
	public static class Tokenizer implements PlaceTokenizer<ListCentralAdminPlace> {

		public ListCentralAdminPlace getPlace(String token) {
			return new ListCentralAdminPlace(true);
		}

		public String getToken(ListCentralAdminPlace place) {
			return "";
		}
	}

	private final boolean listStale;
	RoleAssignmentQueryFilterPojo filter;

	/**
	 * Construct a new {@link case recordListPlace}.
	 * 
	 * @param case recordListStale true if the case record list is stale and should be cleared
	 */
	public ListCentralAdminPlace(boolean listStale) {
		this.listStale = listStale;
	}

	/**
	 * Check if the case record list is stale and should be cleared.
	 * 
	 * @return true if stale, false if not
	 */
	public boolean isListStale() {
		return listStale;
	}

	public RoleAssignmentQueryFilterPojo getFilter() {
		return filter;
	}

	public void setFilter(RoleAssignmentQueryFilterPojo filter) {
		this.filter = filter;
	}
}
