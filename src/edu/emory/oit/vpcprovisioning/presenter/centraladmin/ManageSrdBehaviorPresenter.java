package edu.emory.oit.vpcprovisioning.presenter.centraladmin;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.ActiveSecurityRiskDetecorsQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.ActiveSecurityRiskDetectorsQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class ManageSrdBehaviorPresenter extends PresenterBase implements ManageSrdBehaviorView.Presenter {

	private static final Logger log = Logger.getLogger(ManageSrdBehaviorPresenter.class.getName());
	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	HashMap<String, AccountPojo> cachedAccounts = new java.util.HashMap<String, AccountPojo>();
	List<AccountPojo> accounts = new java.util.ArrayList<AccountPojo>();
	List<AccountPojo> filteredList = new java.util.ArrayList<AccountPojo>();
	UserAccountPojo userLoggedIn;

	/**
	 * The refresh timer used to periodically refresh the account list.
	 */
	//	  private Timer refreshTimer;

	/**
	 * Periodically "touch" HTTP session so they won't have to re-authenticate
	 */
	//	  private Timer sessionTimer;

	public ManageSrdBehaviorPresenter(ClientFactory clientFactory, boolean clearList) {
		this.clientFactory = clientFactory;
		clientFactory.getManageSrdBehaviorView().setPresenter(this);
	}

	/**
	 * Construct a new {@link ManageSrdBehaviorPresenter}.
	 * 
	 * @param clientFactory the {@link ClientFactory} of shared resources
	 * @param place configuration for this activity
	 */
	public ManageSrdBehaviorPresenter(ClientFactory clientFactory, ManageSrdBehaviorPlace place) {
		this(clientFactory, false);
	}

	private ManageSrdBehaviorView getView() {
		return clientFactory.getManageSrdBehaviorView();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		final long startTime = System.currentTimeMillis();
		cachedAccounts = new java.util.HashMap<String, AccountPojo>();

		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		this.eventBus = eventBus;
		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving User Logged In...");
		
		AsyncCallback<ActiveSecurityRiskDetectorsQueryResultPojo> asrdCB = new AsyncCallback<ActiveSecurityRiskDetectorsQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ActiveSecurityRiskDetectorsQueryResultPojo result) {
				getView().setActiveDetectorNames(result.getResults().get(0).getDetectorNames());
			}
		};
		VpcProvisioningService.Util.getInstance().getActiveSecurityDetectorsForFilter(new ActiveSecurityRiskDetecorsQueryFilterPojo(), asrdCB);
		
		AsyncCallback<Boolean> initCB = new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error determining whether or not we need to initialize SRS/ARD account properties", caught);
				getView().hideInitializeAccountSrdArdPropertiesButton();
			}

			@Override
			public void onSuccess(Boolean result) {
				if (result) {
					getView().showInitializeAccountSrdArdPropertiesButton();
				}
				else {
					getView().hideInitializeAccountSrdArdPropertiesButton();
				}
			}
		};
		VpcProvisioningService.Util.getInstance().isInitializeAccountProperties(initCB);
		
		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				log.log(Level.SEVERE, "Exception Retrieving Central Admins", caught);
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().disableButtons();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the Central Admins you're associated to.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(final UserAccountPojo user) {
				userLoggedIn = user;
				getView().enableButtons();
				clientFactory.getShell().setTitle("VPC Provisioning App");
				clientFactory.getShell().setSubTitle("Central Admins");

				getView().setUserLoggedIn(user);
				getView().initPage();

				if (user.isCentralAdminManager()) {
					// we're viewing someone else but we're a CAM so allow changes
					getView().setAllowChanges(true);
				}
				else if (user.isCentralAdmin()) {
					// we're viewing someone else and we're not a CAM so we can't
					// make changes to other people
					getView().setAllowChanges(false);
				}
				else {
					getView().showMessageToUser("An error has occurred.  The user logged in does not "
							+ "appear to be associated to any valid roles required to view this data.");
					getView().applyAWSAccountAuditorMask();
					// TODO: need to not show them the list of accounts???
				}

				// refresh the roleassignment list now.
				refreshList(user);
				
				// TODO: apply masks
				
				long endTime = System.currentTimeMillis();
				long elapsedTime = endTime - startTime;
				GWT.log("[ManageSrdBehaviorPresenter].start elapsed time: " + elapsedTime + " milliseconds.");
			}
		};
		
		GWT.log("getting user logged in from server...");
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(true, userCallback);
	}

	public void refreshList(final UserAccountPojo user) {
		getView().showPleaseWaitDialog("Retrieving Account/SRD behavior...");
		// use RPC to get all accounts for the current filter being used
		AsyncCallback<AccountQueryResultPojo> callback = new AsyncCallback<AccountQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				log.log(Level.SEVERE, "Exception Retrieving Central Admins", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your list of accounts.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(AccountQueryResultPojo result) {
//				GWT.log("Got " + result.size() + " central admins back from server.");
				if (result != null) {
					accounts = result.getResults();
					getView().setAccounts(result.getResults());
				}
				else {
					// error
					getView().showMessageToUser("account result from server is null.");
					return;
				}
				
                getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
			}
		};

		GWT.log("refreshing account/srd behavior summary list...");
		AccountQueryFilterPojo filter = new AccountQueryFilterPojo();
		filter.setUserLoggedIn(user);
		VpcProvisioningService.Util.getInstance().getAccountsForFilter(filter, callback);
	}

	@Override
	public void stop() {
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void filterByText(String filterBeingTyped) {
		GWT.log("ManageSrdBehaviiorPresenter: filtering by: '" + filterBeingTyped + "'");
		filteredList = new java.util.ArrayList<AccountPojo>();
		for (AccountPojo acct : this.accounts) {
			if (acct.getAccountId() != null && 
				acct.getAccountId().indexOf(filterBeingTyped) >= 0) {
					
				filteredList.add(acct);
			}
			else if (acct.getAccountName() != null && 
				acct.getAccountName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.add(acct);
			}
			else if (acct.getAlternateName() != null && 
				acct.getAlternateName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.add(acct);
			}
			else if (acct.getAccountOwnerDirectoryMetaData() != null &&
				acct.getAccountOwnerDirectoryMetaData().getFirstName() != null &&
				acct.getAccountOwnerDirectoryMetaData().getFirstName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.add(acct);
			}
			else if (acct.getAccountOwnerDirectoryMetaData() != null &&
				acct.getAccountOwnerDirectoryMetaData().getLastName() != null &&
				acct.getAccountOwnerDirectoryMetaData().getLastName().toLowerCase().indexOf(filterBeingTyped.toLowerCase()) >= 0) {
				
				filteredList.add(acct);
			}
		}
		getView().setAccounts(filteredList);
		if (filteredList.size() == 0) {
			getView().showNoResultsMessage();
		}
		else {
			getView().hideNoResultsMessage();
		}
	}

	@Override
	public void saveAccount(final AccountPojo account, final boolean applyToOthers, final boolean refreshPage) {
		getView().showPleaseWaitDialog("Updating account...");
		AsyncCallback<AccountPojo> cb = new AsyncCallback<AccountPojo>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error updating account", caught);;
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitDialog();
				getView().showMessageToUser("There was an exception on the " +
						"server updating the account.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(AccountPojo result) {
				getView().hidePleaseWaitDialog();
				if (refreshPage) {
					start(eventBus);
				}
				if (applyToOthers) {
					getView().applyAccountWhitelistsToOtherAccounts(account);
				}
			}
			
		};
		GWT.log("account " + account.getAccountId() + " has " + account.getWhitelistedArns().size() + " whitelisted ARNs");
		GWT.log("account " + account.getAccountId() + " has " + account.getWhitelistedSrds().size() + " whitelisted SRDs");
		VpcProvisioningService.Util.getInstance().updateAccount(account, cb);
	}

	@Override
	public void parseAndApplyArnExemptionsForSrd(String arnString, final String detectorName) {
		getView().showPleaseWaitDialog("Applying ARN exemptions for SRD: " + detectorName + " Please wait...");
    	
		AsyncCallback<Void> cb = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error applying ARN exemptions on the server", caught);;
				getView().hidePleaseWaitDialog();
				getView().showMessageToUser("There was an exception on the " +
						"server applying ARN exemption(s).  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(Void result) {
				getView().hidePleaseWaitDialog();
				// TODO: tell the view to remove that row...
//				start(eventBus);
			}
		};
		if (arnString != null) {
			final String trimedArnString = arnString.trim();
	    	String[] arnParts = trimedArnString.split(":");
	    	String acctId = arnParts[4];
	    	if (acctId == null || acctId.length() == 0) {
	    		// prompt user to select one or more accounts
	    		// refresh the list of accounts first
	    		AsyncCallback<AccountQueryResultPojo> callback = new AsyncCallback<AccountQueryResultPojo>() {
	    			@Override
	    			public void onFailure(Throwable caught) {
	                    getView().hidePleaseWaitPanel();
	    				getView().hidePleaseWaitDialog();
	    				log.log(Level.SEVERE, "Exception Retrieving Accounts", caught);
	    				getView().showMessageToUser("There was an exception on the " +
	    						"server retrieving your list of accounts.  " +
	    						"<p>Message from server is: " + caught.getMessage() + "</p>");
	    			}

	    			@Override
	    			public void onSuccess(AccountQueryResultPojo result) {
	    	    		getView().selectAccountsToApplyArnExemption(result.getResults(), detectorName, trimedArnString);
	    			}
	    		};

	    		GWT.log("getting accounts for account selection popup...");
	    		AccountQueryFilterPojo filter = new AccountQueryFilterPojo();
	    		filter.setUserLoggedIn(userLoggedIn);
	    		VpcProvisioningService.Util.getInstance().getAccountsForFilter(filter, callback);
	    	}
	    	else {
				VpcProvisioningService.Util.getInstance().parseAndApplyArnExemptionsForSrd(trimedArnString, detectorName, cb);
	    	}
		}
		else {
			getView().hidePleaseWaitDialog();
			getView().showMessageToUser("Please enter a comma separated list of ARNs");
		}
	}

	@Override
	public void initializeSrdArnPropertiesForExistingAccounts() {
		AsyncCallback<Void> initCB = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				GWT.log("Error initializing account SRD/ARN properties.", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server initializing SRD/ARN account properties.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(Void result) {
				// refresh the page
				getView().hidePleaseWaitDialog();
				start(eventBus);
			}
		};
		getView().showPleaseWaitDialog("Initializing SRD/ARN properties for all existing accounts, please wait...");
		VpcProvisioningService.Util.getInstance().initializeSrdArnPropertiesForExistingAccounts(userLoggedIn, initCB);
	}

	@Override
	public void clearSrdArnExemptionsForAllAccounts() {
		getView().showPleaseWaitDialog("Clearing all SRD/ARN exemptions from all accounts, please wait...");
		List<AccountPojo> accountsToSave = new java.util.ArrayList<AccountPojo>();
		for (AccountPojo pojo : accounts) {
			// remove all SRD exemptions from the account and save it
			if (pojo.clearSrdArnExemptions()) {
				accountsToSave.add(pojo);
			}
		}
		
		AsyncCallback<Void> acctsCB = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				GWT.log("Error saving accounts", caught);
			}

			@Override
			public void onSuccess(Void result) {
				getView().hidePleaseWaitDialog();
				start(eventBus);
			}
		};
		VpcProvisioningService.Util.getInstance().updateAccounts(accountsToSave, acctsCB);
	}

	@Override
	public void clearSrdExemptionsForAllAccounts() {
		getView().showPleaseWaitDialog("Clearing all SRD exemptions from all accounts, please wait...");
		List<AccountPojo> accountsToSave = new java.util.ArrayList<AccountPojo>();
		for (AccountPojo pojo : accounts) {
			// remove all SRD exemptions from the account and save it
			if (pojo.clearSrdExemptions()) {
				accountsToSave.add(pojo);
			}
		}
		
		AsyncCallback<Void> acctsCB = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				GWT.log("Error saving accounts", caught);
			}

			@Override
			public void onSuccess(Void result) {
				getView().hidePleaseWaitDialog();
				start(eventBus);
			}
		};
		VpcProvisioningService.Util.getInstance().updateAccounts(accountsToSave, acctsCB);
	}
}
