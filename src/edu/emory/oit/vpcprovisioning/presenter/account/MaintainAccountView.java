package edu.emory.oit.vpcprovisioning.presenter.account;

import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.presenter.View;
import edu.emory.oit.vpcprovisioning.shared.AccountExtraMetaDataPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.CustomRolePojo;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertiesPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertyPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleDeprovisioningRequisitionPojo;
import edu.emory.oit.vpcprovisioning.shared.SpeedChartPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.ui.client.PresentsConfirmation;

public interface MaintainAccountView extends Editor<AccountPojo>, IsWidget, View {
	/**
	 * The presenter for this view.
	 */
	public interface Presenter extends PresentsConfirmation {
		/**
		 * Delete the current account or cancel the creation of a account.
		 */
		void deleteAccount();

		/**
		 * Create a new account or save the current account based on the values in the
		 * inputs.
		 */
		void saveAccount();
		AccountPojo getAccount();
		public boolean isValidAccountId(String value);
		public boolean isValidAccountName(String value);
		public EventBus getEventBus();
		public ClientFactory getClientFactory();
		public void setDirectoryMetaDataTitleOnWidget(String netId, Widget w);
		public void setSpeedChartStatusForKeyOnWidget(String key, Widget w, boolean confirmSpeedType);
		public void setSpeedChartStatusForKey(String key, Label label, boolean confirmSpeedType);
		public boolean didConfirmSpeedType();
		public SpeedChartPojo getSpeedType();
		public void logMessageOnServer(final String message);
		public void setDirectoryPerson(DirectoryPersonPojo pojo);
		public DirectoryPersonPojo getDirectoryPerson();
		public void addDirectoryPersonInRoleToAccount(String roleName);
		public void getRoleAssignmentsForAccount();
		public List<RoleAssignmentSummaryPojo> getRoleAssignmentSummaries();
		public void removeRoleAssignmentFromAccount(String accountId, RoleAssignmentSummaryPojo roleAssignmentSummary);
		public MaintainAccountView getView();

//		public void selectAccountNotification
		void saveNotification(AccountNotificationPojo selected);
		void deleteNotification(AccountNotificationPojo selected);
		void showSrdForAccountNotification(AccountNotificationPojo selected);
		void refreshAccountNotificationList(final UserAccountPojo user);
		void setAccountNotificationFilter(AccountNotificationQueryFilterPojo filter);
		AccountNotificationQueryFilterPojo getAccountNotificationFilter();
		void filterByText(String filterBeingTyped);

		public void setSelectedProperty(PropertyPojo prop);
		public PropertyPojo getSelectedProperty();
		public void updateProperty(PropertyPojo prop);
		
		public List<CustomRolePojo> getExistingCustomRoles();
		public void deprovisionCustomRole();
		public RoleDeprovisioningRequisitionPojo getRoleDeprovisioningRequisition();
		void refreshCustomRoleList(final UserAccountPojo user);
		public boolean isEdititingExtraMetaData();
		public AccountExtraMetaDataPojo getAccountExtraMetaData();
		void saveAccountExtraMetaData();
		void deleteAccountExtraMetaData();
	}

	/**
	 * Get the driver used to edit tasks in the view.
	 */
	//	  RequestFactoryEditorDriver<TaskProxy, ?> getEditorDriver();

	/**
	 * Specify whether the view is editing an existing account or creating a new
	 * account.
	 * 
	 * @param isEditing true if editing, false if creating
	 */
	void setEditing(boolean isEditing);

	/**
	 * Lock or unlock the UI so the user cannot enter data. The UI is locked until
	 * the account is loaded.
	 * 
	 * @param locked true to lock, false to unlock
	 */
	void setLocked(boolean locked);

	/**
	 * The the violation associated with the name.
	 * 
	 * @param message the message to show, or null if no violation
	 */
	void setAccountIdViolation(String message);
	void setAccountNameViolation(String message);

	/**
	 * Set the {@link Presenter} for this view.
	 * 
	 * @param presenter the presenter
	 */
	void setPresenter(Presenter presenter);
	
	void initPage();
	void setReleaseInfo(String releaseInfoHTML);
	void setEmailTypeItems(List<String> emailTypes);
	void setAwsAccountsURL(String awsAccountsURL);
	void setAwsBillingManagementURL(String awsBillingManagementURL);
	void setSpeedTypeStatus(String status);
	void setSpeedTypeColor(String color);
	void addSpeedTypeStyle(String styleName);
	Widget getSpeedTypeWidget();
	void setSpeedTypeConfirmed(boolean confirmed);
	boolean isSpeedTypeConfirmed();
	void addRoleAssignment(int index, String name, String netId, String roleName, String widgetTitle);
	void setRoleAssignmentSummaries(List<RoleAssignmentSummaryPojo> summaries);
	void setComplianceClassItems(List<String> complianceClassTypes);
	void enableAdminMaintenance();
	void disableAdminMaintenance();
	
//	void addAccountNotification(int rowNumber, AccountNotificationPojo accountNotification);
//	void initializeAccountNotificationGrid(int rowSize);
//	void clearAccountNotificationList();
	void showWaitForNotificationsDialog(String message);
	void hidWaitForNotificationsDialog();
	
	void setAccountNotifications(List<AccountNotificationPojo> pojos);
	void removeAccountNotificationFromView(AccountNotificationPojo pojo);
	void showFilteredStatus();
	void hideFilteredStatus();

	public void setCimpInstance(boolean isCimpInstance);
	public void setFinancialAccountFieldLabel(String label);
//	public void initializeCustomRoleTable();
	void showNoResultsMessage();
	void hideNoResultsMessage();
	void clearAdminTable();
	void setExistingCustomRoles(List<CustomRolePojo> customRoles);
	void setUnitOrSchoolItems(List<String> purposes);
	void setPrimaryPurposeItems(List<String> purposes);
	void setSecondaryPurposeItems(List<String> purposes);
	void setPrimaryPlatformItems(List<String> platforms);
	void setSecondaryPlatformItems(List<String> platforms);
	void setAwsExperienceLevelItems(PropertiesPojo levels);
	void setReferrerItems(List<String> items);
	public boolean hasStartedExtraMetaData();
	public boolean hasStartedSecondaryProjectData();
	public List<Widget> getMissingRequiredExtraMetaDataFields();
	public void resetExtraMetaDataFieldStyles();
}
