package edu.emory.oit.vpcprovisioning.presenter.vpcp;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.VpcpListUpdateEvent;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.presenter.vpc.ListVpcPresenter;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcpPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcpQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcpQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcpSummaryPojo;

public class ListVpcpPresenter extends PresenterBase implements ListVpcpView.Presenter {
	private static final Logger log = Logger.getLogger(ListVpcPresenter.class.getName());
	/**
	 * A boolean indicating that we should clear the Vpc list when started.
	 */
	private final boolean clearList;

	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	VpcpQueryFilterPojo filter;
	UserAccountPojo userLoggedIn;


	public ListVpcpPresenter(ClientFactory clientFactory, boolean clearList, VpcpQueryFilterPojo filter) {
		this.clientFactory = clientFactory;
		this.clearList = clearList;
		clientFactory.getListVpcpView().setPresenter(this);
	}

	/**
	 * Construct a new {@link ListVpcPresenter}.
	 * 
	 * @param clientFactory the {@link ClientFactory} of shared resources
	 * @param place configuration for this activity
	 */
	public ListVpcpPresenter(ClientFactory clientFactory, ListVpcpPlace place) {
		this(clientFactory, place.isListStale(), place.getFilter());
	}

	private ListVpcpView getView() {
		return clientFactory.getListVpcpView();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		this.eventBus = eventBus;
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();

		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving User Logged In...");
		
		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitPanel();
                getView().hidePleaseWaitDialog();
                getView().disableButtons();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your user information.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(final UserAccountPojo user) {
				userLoggedIn = user;
				getView().setUserLoggedIn(user);
				getView().initPage();
				getView().enableButtons();
				clientFactory.getShell().setTitle("VPC Provisioning App");
				clientFactory.getShell().setSubTitle("VPCPs");

				// Clear the Vpc list and display it.
				if (clearList) {
					getView().clearList();
				}


				// Request the Vpc list now.
				if (getView().viewAllVpcps()) {
					// show all of them
					refreshListWithAllVpcps(user);
				}
				else {
					// only show the default maximum
					refreshListWithMaximumVpcps(user);
				}
			}
		};
		GWT.log("getting user logged in from server...");
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(false, userCallback);
	}

	/**
	 * Refresh the CIDR list.
	 */
	public void refreshList(final UserAccountPojo user) {
		// use RPC to get all Vpcs for the current filter being used
		AsyncCallback<VpcpQueryResultPojo> callback = new AsyncCallback<VpcpQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitPanel();
                getView().hidePleaseWaitDialog();
				log.log(Level.SEVERE, "Exception Retrieving Vpcs", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your list of Vpcs.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(VpcpQueryResultPojo result) {
				GWT.log("Got " + result.getResults().size() + " Vpcps for " + result.getFilterUsed());
				setVpcpSummaryList(result.getResults());
				// apply authorization mask
				if (user.isCentralAdmin()) {
					getView().applyCentralAdminMask();
				}
				else {
					getView().applyAWSAccountAuditorMask();
				}
                getView().hidePleaseWaitDialog();
                getView().hidePleaseWaitPanel();
			}
		};

		GWT.log("refreshing Vpcp list...");
		VpcProvisioningService.Util.getInstance().getVpcpSummariesForFilter(filter, callback);
	}

	@Override
	public void refreshListWithMaximumVpcps(UserAccountPojo user) {
        getView().hidePleaseWaitDialog();
		getView().showPleaseWaitDialog("Retrieving the default maximum list of VPCP objects from the AWS Account service...");

		filter = new VpcpQueryFilterPojo();
		filter.setAllVpcps(false);
		filter.setDefaultMaxVpcps(true);
		
		refreshList(user);
	}

	@Override
	public void refreshListWithAllVpcps(UserAccountPojo user) {
        getView().hidePleaseWaitDialog();
		getView().showPleaseWaitDialog("Retrieving ALL VPCP objects from the AWS Account service (this could take a while)...");

		filter = new VpcpQueryFilterPojo();
		filter.setAllVpcps(true);
		filter.setDefaultMaxVpcps(false);
		
		refreshList(user);
	}

	@Override
	public void filterByProvisioningId(boolean includeAllVpcps, String provisioningId) {
		if (provisioningId == null || provisioningId.length() == 0) {
			getView().hidePleaseWaitDialog();
			getView().showMessageToUser("Please enter a provisioning id");
			return;
		}

		getView().showFilteredStatus();
        getView().hidePleaseWaitDialog();
		getView().showPleaseWaitDialog("Filtering list by provisioning id " + provisioningId + "...");
		
		filter = new VpcpQueryFilterPojo();
		filter.setAllVpcps(false);
		filter.setDefaultMaxVpcps(false);
		filter.setProvisioningId(provisioningId);
		
		refreshList(userLoggedIn);
	}

	/**
	 * Set the list of Vpcs.
	 */
	private void setVpcpSummaryList(List<VpcpSummaryPojo> vpcpSummaries) {
		getView().setVpcpSummaries(vpcpSummaries);
		if (eventBus != null) {
			eventBus.fireEventFromSource(new VpcpListUpdateEvent(vpcpSummaries), this);
		}
	}

	@Override
	public void stop() {
		
		
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	@Override
	public void selectVpcp(VpcpPojo selected) {
		
		
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public VpcpQueryFilterPojo getFilter() {
		return filter;
	}

	public void setFilter(VpcpQueryFilterPojo filter) {
		this.filter = filter;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void deleteVpcp(final VpcpPojo vpcp) {
//		if (Window.confirm("Delete the AWS Vpcp " + vpcp.getProvisioningId() + "?")) {
//			getView().showPleaseWaitDialog("Deleting VPC Provisioning item...");
//			AsyncCallback<Void> callback = new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					getView().showMessageToUser("There was an exception on the " +
//							"server deleting the Vpc.  Message " +
//							"from server is: " + caught.getMessage());
//					getView().hidePleaseWaitDialog();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					// remove from dataprovider
//					getView().removeVpcpFromView(vpcp);
//					getView().hidePleaseWaitDialog();
//					// status message
//					getView().showStatus(getView().getStatusMessageSource(), "Vpcp was deleted.");
//					
//					// TODO fire list Vpcs event...
//				}
//			};
//			VpcProvisioningService.Util.getInstance().deleteVpcp(vpcp, callback);
//		}
	}

}
