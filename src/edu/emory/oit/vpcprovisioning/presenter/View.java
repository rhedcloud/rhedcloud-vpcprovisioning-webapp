package edu.emory.oit.vpcprovisioning.presenter;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.client.AppShell;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public interface View extends IsWidget {
	void showMessageToUser(String message);
	
	void showPleaseWaitDialog(String pleaseWaitHTML);
	void hidePleaseWaitDialog();
	
	void hidePleaseWaitPanel();
	void showPleaseWaitPanel(String pleaseWaitHTML);

	void setInitialFocus();
	public void showStatus(Widget source, String message);
	public void showStatus(String message);
	public Widget getStatusMessageSource();
	public void applyNetworkAdminMask();
	public void applyCentralAdminMask();
	public void applyAWSAccountAdminMask();
	public void applyAWSAccountAuditorMask();
	public void setUserLoggedIn(UserAccountPojo user);
	public List<Widget> getMissingRequiredFields();
	public void resetFieldStyles();
	public void applyStyleToMissingFields(List<Widget> fields);
	public void setFieldViolations(boolean fieldViolations);
	public boolean hasFieldViolations();
	HasClickHandlers getCancelWidget();
	HasClickHandlers getOkayWidget();
	void vpcpPromptOkay(String valueEntered);
	void vpcpPromptCancel();
	void vpcpConfirmOkay();
	void vpcpConfirmCancel();
	void disableButtons();
	void enableButtons();
	public void setAppShell(AppShell appShell);
	public AppShell getAppShell();
}
