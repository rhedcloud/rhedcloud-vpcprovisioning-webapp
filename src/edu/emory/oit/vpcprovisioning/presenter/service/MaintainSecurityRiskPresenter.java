package edu.emory.oit.vpcprovisioning.presenter.service;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.AWSServicePojo;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonPojo;
import edu.emory.oit.vpcprovisioning.shared.SecurityRiskPojo;
import edu.emory.oit.vpcprovisioning.shared.ServiceControlPojo;
import edu.emory.oit.vpcprovisioning.shared.ServiceSecurityAssessmentPojo;
import edu.emory.oit.vpcprovisioning.shared.ServiceSecurityAssessmentQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.ServiceSecurityAssessmentQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class MaintainSecurityRiskPresenter extends PresenterBase implements MaintainSecurityRiskView.Presenter {
	private final ClientFactory clientFactory;
	private EventBus eventBus;
	private ServiceSecurityAssessmentPojo assessment;
	private AWSServicePojo service;
	private SecurityRiskPojo securityRisk;
	private String securityRiskId;
	private DirectoryPersonPojo directoryPerson;
	private MaintainSecurityRiskView view;
//	private CounterMeasurePojo selectedCounterMeasure;
	private ServiceControlPojo selectedServiceControl;

	/**
	 * Indicates whether the activity is editing an existing case record or creating a
	 * new case record.
	 */
	private boolean isEditing;

	/**
	 * For creating a new ACCOUNT.
	 */
	public MaintainSecurityRiskPresenter(ClientFactory clientFactory, AWSServicePojo service, ServiceSecurityAssessmentPojo assessment) {
		this.isEditing = false;
		this.assessment = assessment;
		this.clientFactory = clientFactory;
		this.service = service;
		this.securityRisk = null;
		this.securityRiskId = null;
	}

	/**
	 * For editing an existing ACCOUNT.
	 */
	public MaintainSecurityRiskPresenter(ClientFactory clientFactory, AWSServicePojo service, ServiceSecurityAssessmentPojo assessment, SecurityRiskPojo risk) {
		this.isEditing = true;
		this.clientFactory = clientFactory;
		this.assessment = assessment;
		this.service = service;
		this.securityRisk = risk;
		this.securityRiskId = securityRisk.getSecurityRiskId();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		this.eventBus = eventBus;
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving Security Risk information...");
		
		GWT.log("Maintain Security Risk: service is: " + service);
		GWT.log("Maintain Security Risk: assessment is: " + assessment);

		if (securityRiskId == null) {
			clientFactory.getShell().setSubTitle("Create Security Risk");
			startCreate();
		} 
		else {
			clientFactory.getShell().setSubTitle("Edit Security Risk");
			startEdit();
		}

		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Exception retrieving user logged in", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the user logged in.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(final UserAccountPojo user) {
				getView().setUserLoggedIn(user);
//				getView().setServiceControls(securityRisk.getServiceControls());
				
				AsyncCallback<List<String>> rl_callback = new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						getView().hidePleaseWaitDialog();
						getView().hidePleaseWaitPanel();
						GWT.log("Exception retrieving risk level types", caught);
						getView().showMessageToUser("There was an exception on the " +
								"server retrieving status types.  Message " +
								"from server is: " + caught.getMessage());
					}

					@Override
					public void onSuccess(final List<String> riskLevelItems) {
						
						AsyncCallback<ServiceSecurityAssessmentQueryResultPojo> assessmentCB = new AsyncCallback<ServiceSecurityAssessmentQueryResultPojo>() {
							@Override
							public void onFailure(Throwable caught) {
							}

							@Override
							public void onSuccess(ServiceSecurityAssessmentQueryResultPojo result) {
								assessmentLoop: for (ServiceSecurityAssessmentPojo ssa : result.getResults()) {
									if (ssa.getServiceSecurityAssessmentId().equalsIgnoreCase(assessment.getServiceSecurityAssessmentId()) ) {
										assessment = ssa;
										break assessmentLoop;
									}
								}
								riskLoop: for (SecurityRiskPojo sr : assessment.getSecurityRisks()) {
									if (sr.getSequenceNumber() == securityRisk.getSequenceNumber()) {
										securityRisk = sr;
										securityRiskId = sr.getSecurityRiskId();
										break riskLoop;
									}
								}
//								isEditing = true;
								getView().setRiskLevelItems(riskLevelItems);
								if (securityRisk != null) {
									GWT.log("[MaintainSecurityRiskPresenter] there are " + 
											securityRisk.getServiceControls().size() + 
											" service controls associate to the risk.");
								}
								getView().initPage();
								getView().setFieldViolations(false);
								getView().setInitialFocus();
								
								// apply authorization mask
								if (user.isCentralAdmin()) {
									getView().applyCentralAdminMask();
								}
								else {
									getView().applyAWSAccountAuditorMask();
								}
								getView().hidePleaseWaitDialog();
								getView().hidePleaseWaitPanel();
							}
						};
						ServiceSecurityAssessmentQueryFilterPojo filter = new ServiceSecurityAssessmentQueryFilterPojo();
						filter.setAssessmentId(assessment.getServiceSecurityAssessmentId());
						VpcProvisioningService.Util.getInstance().getSecurityAssessmentsForFilter(filter, assessmentCB);
					}
				};
				// risk level types
				VpcProvisioningService.Util.getInstance().getRiskLevelTypeItems(rl_callback);

			}
		};
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(false, userCallback);
	}

	private void startCreate() {
		GWT.log("Maintain security risk: create");
		isEditing = false;
		getView().setEditing(false);
		securityRisk = new SecurityRiskPojo();
		securityRisk.setSequenceNumber(assessment.getSecurityRisks().size() + 1);
	}

	private void startEdit() {
		GWT.log("Maintain security risk: edit");
		isEditing = true;
		getView().setEditing(true);
		// Lock the display until the assessment is loaded.
		getView().setLocked(true);
	}

	@Override
	public void stop() {
		eventBus = null;
		clientFactory.getMaintainSecurityRiskView().setLocked(false);
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	private void doCancelSecurityRisk() {
		// go back to the maintain assessment page...
		ActionEvent.fire(eventBus, ActionNames.SECURITY_RISK_EDITING_CANCELED, assessment);
	}

	private void doDeleteSecurityRisk() {
		if (securityRisk == null) {
			return;
		}

		// TODO remove the security risk from the assessment and save the assessment
		
	}

	@Override
	public void saveAssessment(final boolean closeRiskDialog) {
		getView().showPleaseWaitDialog("Saving assessment...");
		List<Widget> fields = getView().getMissingRequiredFields();
		if (fields != null && fields.size() > 0) {
			getView().setFieldViolations(true);
			getView().applyStyleToMissingFields(fields);
			getView().hidePleaseWaitDialog();
			getView().hidePleaseWaitPanel();
			getView().showMessageToUser("Please provide data for the required fields.");
			return;
		}
		else {
			getView().setFieldViolations(false);
			getView().resetFieldStyles();
		}
		AsyncCallback<ServiceSecurityAssessmentPojo> callback = new AsyncCallback<ServiceSecurityAssessmentPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Exception saving the Security Assessment", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server saving the Security Assessment.  Message " +
						"from server is: " + caught.getMessage());
				assessment.getSecurityRisks().remove(getSecurityRisk());
				ActionEvent.fire(eventBus, ActionNames.MAINTAIN_SECURITY_ASSESSMENT, service, assessment);
			}

			@Override
			public void onSuccess(ServiceSecurityAssessmentPojo result) {
				if (closeRiskDialog) {
					getView().hidePleaseWaitDialog();
					getView().hidePleaseWaitPanel();
					ActionEvent.fire(eventBus, ActionNames.MAINTAIN_SECURITY_ASSESSMENT, service, assessment);
				}
				else {
					// TODO: need to just refresh the risk dialog page using the updated security risk
					// from the assessment now that it's been saved
					// I think we have to re-query for the assessment here to get the security risk 
					// with its id...
					AsyncCallback<ServiceSecurityAssessmentQueryResultPojo> assessmentCB = new AsyncCallback<ServiceSecurityAssessmentQueryResultPojo>() {
						@Override
						public void onFailure(Throwable caught) {
							
							
						}

						@Override
						public void onSuccess(ServiceSecurityAssessmentQueryResultPojo result) {
							assessmentLoop: for (ServiceSecurityAssessmentPojo ssa : result.getResults()) {
								if (ssa.getServiceSecurityAssessmentId().equalsIgnoreCase(assessment.getServiceSecurityAssessmentId()) ) {
									assessment = ssa;
									break assessmentLoop;
								}
							}
							riskLoop: for (SecurityRiskPojo sr : assessment.getSecurityRisks()) {
								if (sr.getSequenceNumber() == securityRisk.getSequenceNumber()) {
									securityRisk = sr;
									securityRiskId = sr.getSecurityRiskId();
									break riskLoop;
								}
							}
							isEditing = true;
							start(eventBus);
						}
					};
					getView().hidePleaseWaitDialog();
					getView().showPleaseWaitDialog("Retrieving Risk details...");
					ServiceSecurityAssessmentQueryFilterPojo filter = new ServiceSecurityAssessmentQueryFilterPojo();
					filter.setAssessmentId(assessment.getServiceSecurityAssessmentId());
					VpcProvisioningService.Util.getInstance().getSecurityAssessmentsForFilter(filter, assessmentCB);
				}
			}
		};
		if (!isEditing) {
//			getSecurityRisk().setSecurityRiskId(UUID.uuid());
			getSecurityRisk().setServiceId(service.getServiceId());
			this.assessment.getSecurityRisks().add(getSecurityRisk());
		}
		else {
			// TODO: have to find the security risk, remove it and then re-add this one to the list
		}
		// it's always an update
		VpcProvisioningService.Util.getInstance().updateSecurityAssessment(assessment, callback);
	}

	@Override
	public ServiceSecurityAssessmentPojo getSecurityAssessment() {
		return this.assessment;
	}

	public MaintainSecurityRiskView getView() {
		if (view == null) {
			view = clientFactory.getMaintainSecurityRiskView();
			view.setPresenter(this);
		}
		return view;
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public String getSecurityRiskId() {
		return securityRiskId;
	}

	public void setSecurityRiskId(String riskId) {
		this.securityRiskId = riskId;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void deleteSecurityRisk(SecurityRiskPojo selected) {
		if (isEditing) {
			doDeleteSecurityRisk();
		} else {
			doCancelSecurityRisk();
		}
	}

	@Override
	public AWSServicePojo getService() {
		return this.service;
	}

	@Override
	public void setService(AWSServicePojo service) {
		this.service = service;
	}

	@Override
	public void setSecurityAssessment(ServiceSecurityAssessmentPojo assessment) {
		this.assessment = assessment;
	}

	@Override
	public SecurityRiskPojo getSecurityRisk() {
		return this.securityRisk;
	}

	@Override
	public void setDirectoryPerson(DirectoryPersonPojo pojo) {
		this.directoryPerson = pojo;
	}

	public DirectoryPersonPojo getDirectoryPerson() {
		return directoryPerson;
	}

	@Override
	public void vpcpConfirmOkay() {
		securityRisk.getServiceControls().remove(selectedServiceControl);
//		refreshServiceControlList(userLoggedIn);
		// save the assessment
		// TODO: need to see if the risk is already in the list and if so, update it (remove/add)
		// otherwise, just add it
		riskLoop: for (SecurityRiskPojo risk : getSecurityAssessment().getSecurityRisks()) {
			if (getSecurityRiskId() != null) {
				if (risk.getSecurityRiskId().equalsIgnoreCase(getSecurityRiskId())) {
					getSecurityAssessment().getSecurityRisks().remove(risk);
					break riskLoop;
				}
			}
			else {
				GWT.log("null security risk id, can't do it...");
			}
		}
		getSecurityAssessment().getSecurityRisks().add(getSecurityRisk());
		saveAssessment(false);
	}

	@Override
	public void vpcpConfirmCancel() {
		getView().showStatus(getView().getStatusMessageSource(), "Operation cancelled.  Service Control " + 
				selectedServiceControl.getDescription() + " was not deleted.");
	}

	public void setSecurityRisk(SecurityRiskPojo securityRisk) {
		this.securityRisk = securityRisk;
	}

	@Override
	public void saveAssessmentAndMaintainControl(final ServiceControlPojo control) {
		GWT.log("[saveAssessmentAndMaintainControl]");
		getView().showPleaseWaitDialog("Saving assessment...");
		List<Widget> fields = getView().getMissingRequiredFields();
		if (fields != null && fields.size() > 0) {
			getView().setFieldViolations(true);
			getView().applyStyleToMissingFields(fields);
			getView().hidePleaseWaitDialog();
			getView().hidePleaseWaitPanel();
			getView().showMessageToUser("Please provide data for the required fields.");
			return;
		}
		else {
			getView().setFieldViolations(false);
			getView().resetFieldStyles();
		}
		AsyncCallback<ServiceSecurityAssessmentPojo> callback = new AsyncCallback<ServiceSecurityAssessmentPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Exception saving the Security Assessment", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server saving the Security Assessment.  Message " +
						"from server is: " + caught.getMessage());
				assessment.getSecurityRisks().remove(getSecurityRisk());
				ActionEvent.fire(eventBus, ActionNames.MAINTAIN_SECURITY_ASSESSMENT, service, assessment);
			}

			@Override
			public void onSuccess(ServiceSecurityAssessmentPojo result) {
				AsyncCallback<ServiceSecurityAssessmentQueryResultPojo> assessmentCB = new AsyncCallback<ServiceSecurityAssessmentQueryResultPojo>() {
					@Override
					public void onFailure(Throwable caught) {
						getView().hidePleaseWaitDialog();
					}

					@Override
					public void onSuccess(ServiceSecurityAssessmentQueryResultPojo result) {
						assessmentLoop: for (ServiceSecurityAssessmentPojo ssa : result.getResults()) {
							if (ssa.getServiceSecurityAssessmentId().equalsIgnoreCase(assessment.getServiceSecurityAssessmentId()) ) {
								assessment = ssa;
								break assessmentLoop;
							}
						}
						riskLoop: for (SecurityRiskPojo sr : assessment.getSecurityRisks()) {
							if (sr.getSequenceNumber() == securityRisk.getSequenceNumber()) {
								securityRisk = sr;
								securityRiskId = sr.getSecurityRiskId();
								break riskLoop;
							}
						}
						getView().hidePleaseWaitDialog();
						if (control == null) {
							// create control
							ActionEvent.fire(getEventBus(), ActionNames.CREATE_SERVICE_CONTROL, true, getService(), getSecurityAssessment(), getSecurityRisk());
						}
						else {
							// update control
							ActionEvent.fire(getEventBus(), ActionNames.MAINTAIN_SERVICE_CONTROL, true, getService(), getSecurityAssessment(), getSecurityRisk(), control);
						}
					}
				};
				getView().hidePleaseWaitDialog();
				getView().showPleaseWaitDialog("Retrieving Risk details...");
				ServiceSecurityAssessmentQueryFilterPojo filter = new ServiceSecurityAssessmentQueryFilterPojo();
				filter.setAssessmentId(assessment.getServiceSecurityAssessmentId());
				VpcProvisioningService.Util.getInstance().getSecurityAssessmentsForFilter(filter, assessmentCB);
			}
		};
		if (!isEditing) {
			GWT.log("[saveAssessmentAndMaintainControl] need to create the SecurityRisk");
			getSecurityRisk().setServiceId(service.getServiceId());
			this.assessment.getSecurityRisks().add(getSecurityRisk());
		}
		else {
			GWT.log("[saveAssessmentAndMaintainControl] need to update the SecurityRisk");
			// have to find the security risk, remove it and then re-add this one to the list
			int indexToRemove=0;
			boolean foundRisk=false;
			riskLoop: for (int i=0; i<assessment.getSecurityRisks().size(); i++) {
				SecurityRiskPojo risk = assessment.getSecurityRisks().get(i);
				if (risk.getSecurityRiskId().equalsIgnoreCase(getSecurityRisk().getSecurityRiskId())) {
					indexToRemove = i;
					foundRisk = true;
					break riskLoop;
				}
			}
			if (foundRisk) {
				assessment.getSecurityRisks().remove(indexToRemove);
				assessment.getSecurityRisks().add(getSecurityRisk());
			}
			else {
				assessment.getSecurityRisks().add(getSecurityRisk());
			}
		}
		// it's always an update
		VpcProvisioningService.Util.getInstance().updateSecurityAssessment(assessment, callback);
	}
}
