package edu.emory.oit.vpcprovisioning.presenter.service;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.presenter.View;
import edu.emory.oit.vpcprovisioning.shared.AWSServicePojo;
import edu.emory.oit.vpcprovisioning.shared.AWSServiceQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.ui.client.PresentsConfirmation;

public interface ListServiceView extends IsWidget, View {
	/**
	 * The presenter for this view.
	 */
	public interface Presenter extends PresentsConfirmation {
		void selectService(AWSServicePojo selected);
		public EventBus getEventBus();
		public AWSServiceQueryFilterPojo getFilter();
		public ClientFactory getClientFactory();
		/**
		 * Delete the current Vpc or cancel the creation of a Vpc.
		 */
		void deleteService(AWSServicePojo service);
		public void logMessageOnServer(final String message);
		void refreshList(final UserAccountPojo user);
		
		void filterByText(String filterBeingTyped);
		void clearFilter();
	}

	/**
	 * Clear the list of case records.
	 */
	void clearList();

	/**
	 * Sets the new presenter, and calls {@link Presenter#stop()} on the previous
	 * one.
	 */
	void initPage();
	void setPresenter(Presenter presenter);

	void setServices(List<AWSServicePojo> services);
	
	void setReleaseInfo(String releaseInfoHTML);
	void removeServiceFromView(AWSServicePojo service);
	void showNoResultsMessage();
	void hideNoResultsMessage();
}
