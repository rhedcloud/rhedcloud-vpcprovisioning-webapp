package edu.emory.oit.vpcprovisioning.presenter.vpn;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.common.VpcpConfirm;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.client.event.VpnConnectionProfileListUpdateEvent;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.presenter.vpc.ListVpcPresenter;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionDeprovisioningPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProfileAssignmentPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProfilePojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProfileQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProfileQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProfileSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionProvisioningSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.VpnConnectionRequisitionPojo;

public class ListVpnConnectionProfilePresenter extends PresenterBase implements ListVpnConnectionProfileView.Presenter {
	/**
	 * A boolean indicating that we should clear the Vpc list when started.
	 */
	private final boolean clearList;

	private final ClientFactory clientFactory;

	private EventBus eventBus;
	
	VpnConnectionProfileQueryFilterPojo filter;
	VpcPojo vpc;
	VpnConnectionProfileSummaryPojo selectedSummary;
	VpnConnectionProfileAssignmentPojo selectedAssignment;
	boolean isAssignmentDelete;
	int selectedRowNumber;
	List<VpnConnectionProfileSummaryPojo> selectedSummaries;
	VpnConnectionRequisitionPojo selectedVpnConnectionRequisition;
	String selectedVpcId;
	UserAccountPojo userLoggedIn;

	boolean showStatus = false;
	boolean startTimer = true;
	int deletedCount;
	int totalToDelete;
	StringBuffer deleteErrors;

	public ListVpnConnectionProfilePresenter(ClientFactory clientFactory, boolean clearList, VpnConnectionProfileQueryFilterPojo filter) {
		this.clientFactory = clientFactory;
		this.clearList = clearList;
		this.filter = filter;
		clientFactory.getListVpnConnectionProfileView().setPresenter(this);
	}

	/**
	 * Construct a new {@link ListVpcPresenter}.
	 * 
	 * @param clientFactory the {@link ClientFactory} of shared resources
	 * @param place configuration for this activity
	 */
	public ListVpnConnectionProfilePresenter(ClientFactory clientFactory, ListVpnConnectionProfilePlace place) {
		this(clientFactory, place.isListStale(), place.getFilter());
	}

	private ListVpnConnectionProfileView getView() {
		return clientFactory.getListVpnConnectionProfileView();
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		this.eventBus = eventBus;

		setReleaseInfo(clientFactory);
		getView().showPleaseWaitDialog("Retrieving User Logged In...");
		
		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitPanel();
                getView().hidePleaseWaitDialog();
                getView().disableButtons();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the user logged in.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(final UserAccountPojo user) {
				userLoggedIn = user;
				clientFactory.getShell().setTitle("VPC Provisioning App");
				clientFactory.getShell().setSubTitle("VPN Connection Profiles");
				getView().initPage();

				// Clear the Vpc list and display it.
				if (clearList) {
					getView().clearList();
				}

				getView().setUserLoggedIn(userLoggedIn);

				// Request the Vpc list now.
				refreshList(user);
			}
		};
		GWT.log("getting user logged in from server...");
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(false, userCallback);
	}

	@Override
	public void stop() {
		
		
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	@Override
	public void selectVpnConnectionProfile(VpnConnectionProfilePojo selected) {
		
		
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public VpnConnectionProfileQueryFilterPojo getFilter() {
		return filter;
	}

	public void setFilter(VpnConnectionProfileQueryFilterPojo filter) {
		this.filter = filter;
	}

	@Override
	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void deleteVpnConnectionProfile(VpnConnectionProfileSummaryPojo selected) {
		selectedSummary = selected;
		VpcpConfirm.confirm(
			ListVpnConnectionProfilePresenter.this, 
			"Confirm Delete VPN Connection Profile", 
			"Delete the VPN Connection Profile " + selectedSummary.getProfile().getVpcNetwork() + "?");
	}

	/**
	 * Refresh the CIDR list.
	 */
	public void refreshList(final UserAccountPojo user) {
		getView().showPleaseWaitDialog("Retrieving VPN Connection Profiles from the Network OPS service...");
		// use RPC to get all Vpcs for the current filter being used
		AsyncCallback<VpnConnectionProfileQueryResultPojo> callback = new AsyncCallback<VpnConnectionProfileQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
                getView().hidePleaseWaitDialog();
                getView().hidePleaseWaitPanel();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving your list of VPN Connection Profiles.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(VpnConnectionProfileQueryResultPojo result) {
				GWT.log("Got " + result.getResults().size() + " VPN Connection Profiles for " + result.getFilterUsed());
				setVpnConnectionProfileSummaryList(result.getResults());
				
				int totalProfiles = result.getResults().size();
				int assignedProfiles = 0;
				int unassignedProfiles = 0;
				for (VpnConnectionProfileSummaryPojo summary : result.getResults()) {
					if (summary.getAssignment() != null) {
						assignedProfiles++;
					}
					else {
						unassignedProfiles++;
					}
				}
				StringBuffer sbuf = new StringBuffer();
				sbuf.append("There are " + totalProfiles + " total VPN Connection Profiles listed below.</br>");
				sbuf.append(assignedProfiles + " profiles are already assigned.</br>");
				sbuf.append(unassignedProfiles + " profiles are unassigned.");
				getView().setProfileSummaryHTML(sbuf.toString());
				
				// apply authorization mask
				if (user.isNetworkAdmin()) {
					getView().applyNetworkAdminMask();
				}
				else if (user.isCentralAdmin()) {
					getView().applyCentralAdminMask();
				}
				else {
					getView().applyAWSAccountAuditorMask();
				}
                getView().hidePleaseWaitDialog();
                getView().hidePleaseWaitPanel();
			}
		};

		GWT.log("refreshing VPN Connection Profile list...");
		VpcProvisioningService.Util.getInstance().getVpnConnectionProfilesForFilter(filter, callback);
	}

	private void setVpnConnectionProfileSummaryList(List<VpnConnectionProfileSummaryPojo> list) {
		getView().setVpnConnectionProfileSummaries(list);
		if (eventBus != null) {
			eventBus.fireEventFromSource(new VpnConnectionProfileListUpdateEvent(list), this);
		}
	}

	public VpcPojo getVpc() {
		return vpc;
	}

	public void setVpc(VpcPojo vpc) {
		this.vpc = vpc;
	}

	@Override
	public void vpcpConfirmOkay() {
		if (selectedSummaries != null) {
			// VPN Connection Profile Delete
			showStatus = false;
			deletedCount = 0;
			totalToDelete = selectedSummaries.size();
			deleteErrors = new StringBuffer();
			for (int i=0; i<selectedSummaries.size(); i++) {
				final VpnConnectionProfileSummaryPojo summary = selectedSummaries.get(i);
				final int listCounter = i;
				
				AsyncCallback<VpnConnectionProfilePojo> callback = new AsyncCallback<VpnConnectionProfilePojo>() {
					@Override
					public void onFailure(Throwable caught) {
						deleteErrors.append("There was an exception on the " +
								"server deleting the VPN Connection Profile (id=" + summary.getProfile().getVpnConnectionProfileId() + ").  " +
								"<p>Message from server is: " + caught.getMessage() + "</p>");
						if (!showStatus) {
							deleteErrors.append("\n");
						}
						if (listCounter == totalToDelete - 1) {
							showStatus = true;
						}
					}
		
					@Override
					public void onSuccess(VpnConnectionProfilePojo result) {
						deletedCount++;
						if (listCounter == totalToDelete - 1) {
							showStatus = true;
						}
					}
				};
				VpcProvisioningService.Util.getInstance().deleteVpnConnectionProfile(summary.getProfile(), callback);
			}
			if (!showStatus) {
				// wait for all the creates to finish processing
				int delayMs = 500;
				Scheduler.get().scheduleFixedDelay(new Scheduler.RepeatingCommand() {			
					@Override
					public boolean execute() {
						if (showStatus) {
							startTimer = false;
							showDeleteListStatus();
						}
						return startTimer;
					}
				}, delayMs);
			}
			else {
				showDeleteListStatus();
			}
		}
		else if (selectedVpnConnectionRequisition != null){
			// NOTE:  this isn't used any more and we shouldn't get here
			// is a VPN Connection de-provision (generate)
//			AsyncCallback<VpnConnectionDeprovisioningPojo> callback = new AsyncCallback<VpnConnectionDeprovisioningPojo>() {
//				@Override
//				public void onFailure(Throwable caught) {
//					getView().hidePleaseWaitDialog();
//					GWT.log("Exception generating the VpnConnectionDeprovisioning", caught);
//					getView().showMessageToUser("There was an exception on the " +
//							"server generating the VpnConnectionDeprovisioning.  Message " +
//							"from server is: " + caught.getMessage());
//				}
//
//				@Override
//				public void onSuccess(VpnConnectionDeprovisioningPojo result) {
//					getView().hidePleaseWaitDialog();
//					// it was a generate, we'll take them to the VPNCP status view
//					final VpnConnectionDeprovisioningPojo vpncdp = result;
//					GWT.log("VPNCDP was generated on the server, showing status page.  "
//							+ "VPNCDP is: " + vpncdp);
//					VpnConnectionProvisioningSummaryPojo vpncpSummary = new VpnConnectionProvisioningSummaryPojo();
//					vpncpSummary.setDeprovisioning(vpncdp);
//					ActionEvent.fire(eventBus, ActionNames.VPNCDP_GENERATED, vpncpSummary);
//				}
//			};
//			getView().showPleaseWaitDialog("Generating VPC Deprovisioning object...");
//			VpcProvisioningService.Util.getInstance().generateVpnConnectionDeprovisioning(selectedVpnConnectionRequisition, callback);
		}
		else if (selectedVpcId != null) {
			// NOTE:  this isn't used any more and we shouldn't get here
//			getView().showPleaseWaitDialog("Deprovisioning VPN Connection for VPC " + selectedVpcId);
//			// VPN deprovision for a given vpcid
//			// do a VpnConnection.Query for the given vpc
//			// do a VpnConnectionDeprovisioning.Generate passing the VpnConnection that was returned.
//			
//			AsyncCallback<VpnConnectionQueryResultPojo> cb = new AsyncCallback<VpnConnectionQueryResultPojo>() {
//				@Override
//				public void onFailure(Throwable caught) {
//					getView().hidePleaseWaitDialog();
//					getView().showMessageToUser("There was an exception on the " +
//							"server retrieving the VPN Connection for VPC ID " + 
//							selectedVpcId + ".  Message " +
//							"from server is: " + caught.getMessage());
//				}
//
//				@Override
//				public void onSuccess(VpnConnectionQueryResultPojo result) {
//					if (result.getResults().size() == 0) {
//						getView().hidePleaseWaitDialog();
//						getView().showMessageToUser("Could not find a VPN Connection for the VPC "
//								+ "id " + selectedVpcId + ".  Processing cannot continue.");
//						return;
//					}
//					VpnConnectionPojo vpn = result.getResults().get(0);
//					AsyncCallback<VpnConnectionDeprovisioningPojo> de_cb = new AsyncCallback<VpnConnectionDeprovisioningPojo>() {
//						@Override
//						public void onFailure(Throwable caught) {
//							getView().hidePleaseWaitDialog();
//							GWT.log("Exception generating the VpnConnectionDeprovisioning", caught);
//							getView().showMessageToUser("There was an exception on the " +
//									"server generating the VpnConnectionDeprovisioning.  Message " +
//									"from server is: " + caught.getMessage());
//						}
//
//						@Override
//						public void onSuccess(VpnConnectionDeprovisioningPojo result) {
//							// go to the status page
//							getView().hidePleaseWaitDialog();
//							VpnConnectionProvisioningSummaryPojo vpncpSummary = new VpnConnectionProvisioningSummaryPojo();
//							vpncpSummary.setDeprovisioning(result);
//							ActionEvent.fire(eventBus, ActionNames.VPNCDP_GENERATED, vpncpSummary);
//						}
//					};
//					VpcProvisioningService.Util.getInstance().generateVpnConnectionDeprovisioning(vpn, de_cb);
//				}
//			};
//			VpnConnectionQueryFilterPojo filter = new VpnConnectionQueryFilterPojo();
//			filter.setVpcId(selectedVpcId);
//			VpcProvisioningService.Util.getInstance().getVpnConnectionsForFilter(filter, cb);
		}
		else if (selectedAssignment != null) {
			// generate VPN COnnection Deprovisioning passing a VpnConnectionProfileAssignment
			getView().showPleaseWaitDialog("Deprovisioning VPN Connection for VPC " + selectedAssignment.getOwnerId());
			AsyncCallback<VpnConnectionDeprovisioningPojo> de_cb = new AsyncCallback<VpnConnectionDeprovisioningPojo>() {
				@Override
				public void onFailure(Throwable caught) {
					getView().hidePleaseWaitDialog();
					GWT.log("Exception generating the VpnConnectionDeprovisioning", caught);
					getView().showMessageToUser("There was an exception on the " +
							"server generating the VpnConnectionDeprovisioning.  Message " +
							"from server is: " + caught.getMessage());
				}

				@Override
				public void onSuccess(VpnConnectionDeprovisioningPojo result) {
					// go to the status page
					getView().hidePleaseWaitDialog();
					VpnConnectionProvisioningSummaryPojo vpncpSummary = new VpnConnectionProvisioningSummaryPojo();
					vpncpSummary.setDeprovisioning(result);
					ActionEvent.fire(eventBus, ActionNames.VPNCDP_GENERATED, vpncpSummary);
				}
			};
			VpcProvisioningService.Util.getInstance().generateVpnConnectionDeprovisioning(selectedAssignment, de_cb);
		}
		else if (selectedSummary != null && isAssignmentDelete) {
			// query for a VpnConnection by assignment.ownerId
			// if a VpnConnection does not exist, delete the assignment
			// else show a message to the user

			// uncomment this just to test the row refresh
//			getView().showPleaseWaitDialog("Retrieving the VPN Connection Profile: " + selectedSummary.getProfile().getVpnConnectionProfileId());
//			GWT.log("[vpcpConfirmOkay] telling view to refresh rowNumber: " + selectedRowNumber);
//			selectedSummary.setAssignment(null);
//    		getView().refreshTableRow(selectedRowNumber, selectedSummary);
//          getView().hidePleaseWaitDialog();

            AsyncCallback<VpnConnectionQueryResultPojo> vpn_cb = new AsyncCallback<VpnConnectionQueryResultPojo>() {
				@Override
				public void onFailure(Throwable caught) {
					getView().hidePleaseWaitDialog();
					getView().showMessageToUser("There was an exception on the " +
							"server checking for a VpnConnection.  Message " +
							"from server is: " + caught.getMessage());
				}

				@Override
				public void onSuccess(VpnConnectionQueryResultPojo result) {
					if (result.getResults().size() == 0) {
						// it is safe to delete the assignment
						AsyncCallback<VpnConnectionProfileAssignmentPojo> delete_assignment_cb = new AsyncCallback<VpnConnectionProfileAssignmentPojo>() {
							@Override
							public void onFailure(Throwable caught) {
								getView().hidePleaseWaitDialog();
								getView().showMessageToUser("There was an exception on the " +
										"server deleting the VpnConnectionProfileAssignment.  Message " +
										"from server is: " + caught.getMessage());
							}

							@Override
							public void onSuccess(VpnConnectionProfileAssignmentPojo result) {
								// now get the summary for this profile again
								AsyncCallback<VpnConnectionProfileQueryResultPojo> vpn_summary_cb = new AsyncCallback<VpnConnectionProfileQueryResultPojo>() {
									@Override
									public void onFailure(Throwable caught) {
						                getView().hidePleaseWaitDialog();
						                getView().hidePleaseWaitPanel();
										getView().showMessageToUser("There was an exception on the " +
												"server retrieving the VPN Connection Profile.  " +
												"<p>Message from server is: " + caught.getMessage() + "</p>");
									}

									@Override
									public void onSuccess(VpnConnectionProfileQueryResultPojo result) {
										VpnConnectionProfileSummaryPojo summary = result.getResults().get(0);
										GWT.log("[vpcpConfirmOkay] telling view to refresh rowNumber: " 
											+ selectedRowNumber 
											+ " for profile id: " 
											+ summary.getProfile().getVpnConnectionProfileId());
						                getView().hidePleaseWaitDialog();
						                getView().hidePleaseWaitPanel();
						        		getView().refreshTableRow(selectedRowNumber, summary);
									}
								};

								VpnConnectionProfileQueryFilterPojo vcp_filter = new VpnConnectionProfileQueryFilterPojo();
								vcp_filter.setVpnConnectionProfileId(selectedSummary.getProfile().getVpnConnectionProfileId());
				                getView().hidePleaseWaitDialog();
								getView().showPleaseWaitDialog("Retrieving the VPN Connection Profile: " + vcp_filter.getVpnConnectionProfileId());
								VpcProvisioningService.Util.getInstance().getVpnConnectionProfilesForFilter(vcp_filter, vpn_summary_cb);
							}
						};
		                getView().hidePleaseWaitDialog();
						getView().showPleaseWaitDialog("Deleting the VPN Connection Profile Assignment for VPC " + selectedSummary.getAssignment().getOwnerId());
						VpcProvisioningService.Util.getInstance().deleteVpnConnectionProfileAssignment(selectedSummary.getAssignment(), delete_assignment_cb);
					}
					else {
						// show message to user
		                getView().hidePleaseWaitDialog();
						getView().showMessageToUser("This profile appears to have a VPN Connection associated "
							+ "to it.  Therefore, the assignment cannot be deleted until that connection "
							+ "has been de-provisioned.  If you still want to remove this assignment, please "
							+ "deprovision the VPN connection first.");
					}
				}
			};
			getView().showPleaseWaitDialog("Checking for a VpnConnection associated to VPC " + selectedSummary.getAssignment().getOwnerId());
			VpnConnectionQueryFilterPojo vpn_filter = new VpnConnectionQueryFilterPojo();
			vpn_filter.setVpcId(selectedSummary.getAssignment().getOwnerId());
			VpcProvisioningService.Util.getInstance().getVpnConnectionsForFilter(vpn_filter, vpn_cb);
		}
	}

	@Override
	public void vpcpConfirmCancel() {
		if (selectedSummaries != null) {
			getView().showStatus(getView().getStatusMessageSource(), "Operation cancelled.  VPN Connection Profile was NOT deleted");
		}
		else if (selectedVpcId != null){
			// is a de-provision
			getView().showStatus(getView().getStatusMessageSource(), "Operation cancelled.  VPN Connection was NOT de-provisioned");
		}
		else if (selectedAssignment != null) {
			// is a de-provision
			getView().showStatus(getView().getStatusMessageSource(), "Operation cancelled.  VPN Connection was NOT de-provisioned");
		}
	}

	@Override
	public void deleteVpnConnectionProfiles(List<VpnConnectionProfileSummaryPojo> summaries) {
		selectedVpnConnectionRequisition = null;
		selectedSummaries = summaries;
		VpcpConfirm.confirm(
			ListVpnConnectionProfilePresenter.this, 
			"Confirm Delete VPN Connection Profile", 
			"Delete the selected " + selectedSummaries.size() + " VPN Connection Profiles?");
	}

	void showDeleteListStatus() {
		if (deleteErrors.length() == 0) {
			getView().hidePleaseWaitDialog();
			getView().showStatus(null, deletedCount + " out of " + totalToDelete + " VPN Connection Profile(s) were deleted.");
		}
		else {
			getView().hidePleaseWaitDialog();
			deleteErrors.insert(0, deletedCount + " out of " + totalToDelete + " VPN Connection Profile(s) were deleted.  "
				+ "Below are the errors that occurred:</br>");
			getView().showMessageToUser(deleteErrors.toString());
		}
	}

	@Override
	public void setSelectedSummaries(List<VpnConnectionProfileSummaryPojo> summaries) {
		selectedSummaries = summaries;
	}

	@Override
	public void filterByVpcAddress(String vpcAddress) {
		GWT.log("vpn connection profile filtering by vpc address");
		if (vpcAddress == null || vpcAddress.length() == 0) {
			getView().hidePleaseWaitDialog();
			getView().showMessageToUser("Please enter a VPC Network");
			return;
		}

		getView().showFilteredStatus();
        getView().hidePleaseWaitDialog();
		getView().showPleaseWaitDialog("Filtering list by VPC Network " + vpcAddress + "...");
		
		filter = new VpnConnectionProfileQueryFilterPojo();
		filter.setVpcNetwork(vpcAddress);
		
		refreshList(userLoggedIn);
	}

	@Override
	public void filterByVpnConnectionProfileId(String profileId) {
		GWT.log("vpn connection profile filtering by profile id");
		if (profileId == null || profileId.length() == 0) {
			getView().hidePleaseWaitDialog();
			getView().showMessageToUser("Please enter a Profile ID");
			return;
		}

		getView().showFilteredStatus();
        getView().hidePleaseWaitDialog();
		getView().showPleaseWaitDialog("Filtering list by Profile ID " + profileId + "...");
		
		filter = new VpnConnectionProfileQueryFilterPojo();
		filter.setVpnConnectionProfileId(profileId);
		
		refreshList(userLoggedIn);
	}

	@Override
	public void clearFilter() {
		this.filter = null;
	}

//	@Override
//	public void deprovisionVpnConnection(VpnConnectionRequisitionPojo vpnConnectionRequisition) {
//		selectedSummaries = new java.util.ArrayList<VpnConnectionProfileSummaryPojo>();
//		selectedVpnConnectionRequisition = vpnConnectionRequisition;
//		
//		VpcpConfirm.confirm(
//				ListVpnConnectionProfilePresenter.this, 
//				"Confirm Deprovision VPN Connection", 
//				"Deprovisiong the VPN Connection " + selectedVpnConnectionRequisition.getProfile().getVpcNetwork() + "?");
//
//	}

//	@Override
//	public void deprovisionVpnConnectionForVpcId(String vpcId) {
//		selectedSummary = null;
//		selectedSummaries = null;
//		selectedVpnConnectionRequisition = null;
//		selectedVpcId = vpcId;
//		selectedAssignment = null;
//
//		VpcpConfirm.confirm(
//				ListVpnConnectionProfilePresenter.this, 
//				"Confirm Deprovision VPN Connection", 
//				"Deprovisiong the VPN Connection for VPC " + selectedVpcId + "?");
//	}

	@Override
	public void setSelectedAssignment(VpnConnectionProfileAssignmentPojo assignment) {
		this.selectedAssignment = assignment;
	}

	@Override
	public void deprovisionVpnConnectionForAssignment(VpnConnectionProfileAssignmentPojo assignment) {
		selectedSummary = null;
		selectedSummaries = null;
		selectedVpnConnectionRequisition = null;
		selectedVpcId = null;
		selectedAssignment = assignment;
		isAssignmentDelete = false;

		VpcpConfirm.confirm(
				ListVpnConnectionProfilePresenter.this, 
				"Confirm Deprovision VPN Connection", 
				"Deprovisiong the VPN Connection for VPC " + selectedAssignment.getOwnerId() + "?");
	}

	@Override
	public void deleteVpnConnectionProfileAssignment(final int rowNumber, final VpnConnectionProfileSummaryPojo summary) {
		selectedSummary = summary;
		selectedSummaries = null;
		selectedVpnConnectionRequisition = null;
		selectedVpcId = null;
		selectedAssignment = null;
		isAssignmentDelete = true;
		selectedRowNumber = rowNumber;
		GWT.log("[deleteVpnConnectionProfileAssignment] selectedRowNumber is: " + rowNumber);
		VpcpConfirm.confirm(
				ListVpnConnectionProfilePresenter.this, 
				"Confirm Delete VPN Connection Profile Assignment", 
				"Delete the Profile Assignment from Profile ID " + summary.getProfile().getVpnConnectionProfileId() + "?");
	}

	@Override
	public void getVpnStatusForVpc(String vpcId) {
		AsyncCallback<VpnConnectionQueryResultPojo> vpn_cb = new AsyncCallback<VpnConnectionQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().showMessageToUser("There was an exception on the " +
						"server checking for a VpnConnection.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(VpnConnectionQueryResultPojo result) {
                getView().hidePleaseWaitDialog();
				if (result.getResults().size() == 0) {
					getView().showMessageToUser("A VPN Connection DOES NOT exist for the selected profile/assignment");
				}
				else {
					// show message to user
					VpnConnectionPojo vpn1=null;
					VpnConnectionPojo vpn2=null;
					if (result.getResults().size() == 2) {
						vpn1 = result.getResults().get(0);
						vpn2 = result.getResults().get(1);
					}
					else if (result.getResults().size() == 1) {
						vpn1 = result.getResults().get(0);
					}
					
					// get the tunnel status from the not null vpn1 and/or vpn2 objects
					boolean tunnel1Good=false;
					boolean tunnel2Good=false;
					if (vpn1 != null) {
						if (vpn1.getTunnelInterfaces().size() > 0) {
							tunnel1Good = vpn1.getTunnelInterfaces().get(0).isOperational();
						}
					}
					if (vpn2 != null) {
						if (vpn2.getTunnelInterfaces().size() > 0) {
							tunnel2Good = vpn2.getTunnelInterfaces().get(0).isOperational();
						}
					}
					
					String tunnel1Html;
					if (tunnel1Good) {
						tunnel1Html = 
								"<table cellspacing=\"8\"><tr><td>"
								+ "Tunnel 1 is: Operational"
								+ "</td><td>"
								+ "<img src=\"images/green_circle_icon.png\" width=\"16\" height=\"16\"/>"
								+ "</td></tr></table";
					}
					else {
						tunnel1Html = 
								"<table cellspacing=\"8\"><tr><td>"
								+ "Tunnel 1 is: Not Operational"
								+ "</td><td>"
								+ "<img src=\"images/red_circle_icon.jpg\" width=\"16\" height=\"16\"/>"
								+ "</td></tr></table";
					}
							 
					String tunnel2Html;
					if (tunnel2Good) {
						tunnel2Html =
								"<table cellspacing=\"8\"><tr><td>"
								+ "Tunnel 2 is: Operational"
								+ "</td><td>"
								+ "<img src=\"images/green_circle_icon.png\" width=\"16\" height=\"16\"/>"
								+ "</td></tr></table";
					}
					else {
						tunnel2Html = 
								"<table cellspacing=\"8\"><tr><td>"
								+ "Tunnel 2 is: Not Operational"
								+ "</td><td>"
								+ "<img src=\"images/red_circle_icon.jpg\" width=\"16\" height=\"16\"/>"
								+ "</td></tr></table";
					}
					
					getView().showMessageToUser(
							"A VPN Connection DOES exist for the selected profile/assignment.  "
							+ "<p>"
							+ tunnel1Html
							+ tunnel2Html
							+ "</p>"
							+ "<p>For more information, please visit the VPC Details page for the VPC "
							+ "associated to this profile</p>");
				}
			}
		};
		getView().showPleaseWaitDialog("Checking for a VpnConnection associated to VPC " + vpcId);
		VpnConnectionQueryFilterPojo vpn_filter = new VpnConnectionQueryFilterPojo();
		vpn_filter.setVpcId(vpcId);
		VpcProvisioningService.Util.getInstance().getVpnConnectionsForFilter(vpn_filter, vpn_cb);
	}
}
