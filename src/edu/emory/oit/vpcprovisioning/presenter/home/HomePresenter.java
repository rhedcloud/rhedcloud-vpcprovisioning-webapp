package edu.emory.oit.vpcprovisioning.presenter.home;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.AccountProvisioningAuthorizationPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRolePojo;
import edu.emory.oit.vpcprovisioning.shared.ConsoleFeaturePojo;
import edu.emory.oit.vpcprovisioning.shared.ConsoleFeatureQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.ConsoleFeatureQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonPojo;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.PersonInfoSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class HomePresenter extends PresenterBase implements HomeView.Presenter {
	private final ClientFactory clientFactory;
	private EventBus eventBus;
	private UserAccountPojo userLoggedIn;
	private DirectoryPersonPojo directoryPerson;
	boolean validateFinancialAccounts = true;

//	public HomePresenter(ClientFactory clientFactory, UserAccountPojo user) {
//		this.userLoggedIn = user;
//		this.clientFactory = clientFactory;
//		clientFactory.getHomeView().setPresenter(this);
//	}
	public HomePresenter(ClientFactory clientFactory, UserAccountPojo user, boolean validateFinancialAccounts) {
		this.userLoggedIn = user;
		this.clientFactory = clientFactory;
		this.validateFinancialAccounts = validateFinancialAccounts;
		clientFactory.getHomeView().setPresenter(this);
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		this.eventBus = eventBus;
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();

		setReleaseInfo(clientFactory);
		
		if (getUserLoggedIn() == null) {
			getView().showPleaseWaitDialog("Retrieving User Logged In...");
			AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {

				@Override
				public void onFailure(Throwable caught) {
					getView().hidePleaseWaitDialog();
					getView().hidePleaseWaitPanel();
					getView().disableButtons();
					getView().showMessageToUser("There was an exception on the " +
							"server retrieving your user information.  " +
							"<p>Message from server is: " + caught.getMessage() + "</p>");
				}

				@Override
				public void onSuccess(final UserAccountPojo user) {
					userLoggedIn = user;
					setupView();
					
				}
			};
			VpcProvisioningService.Util.getInstance().getUserLoggedIn(userCallback);
		}
		else {
			setupView();
		}
	}
	
	private void setupView() {
		GWT.log("[HomePresenter.setupView]");
		getView().setUserLoggedIn(userLoggedIn);
		getView().enableButtons();
		getView().showPleaseWaitDialog("Retrieving account metadata from the AWS Account Service...");
		
		// TODO: speedtype validation in the background???
		AsyncCallback<Boolean> badStCb = new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception retrieving bad speed charts", caught);
				getView().hideSpeedTypeValidationNotice();
			}

			@Override
			public void onSuccess(Boolean result) {
				getView().hideSpeedTypeValidationNotice();
				if (result) {
					// when/if they have invalid speedtypes, they'll 
					// go to a new page instead of the home page 
					// they'll go to the ListUserFinancialAccountsView where they'll be able 
					// to update/fix any accounts that are in bad standing
					GWT.log("need to get List Financial Accounts Content.");
					// TODO: popup a dialog where they can go to the financial account maintenance page
					final PopupPanel popup = new PopupPanel(true, true);
					VerticalPanel vp = new VerticalPanel();
					vp.setSpacing(8);
					
					String message = "You are affiliated with account(s) that "
						+ "have bad or nearly bad financial accounts (speed "
						+ "types) associated to them.  You should update the "
						+ "financial accounts appropriately.";
					HTML h = new HTML(message);
					h.addStyleName("body");
					h.setWidth("350px");
					
					vp.add(h);
					
					Grid buttonGrid = new Grid(1, 2);
					buttonGrid.setCellSpacing(8);
					vp.add(buttonGrid);
					vp.setCellHorizontalAlignment(buttonGrid, HasHorizontalAlignment.ALIGN_CENTER);
					
					Button finAcctButton = new Button("View");
					finAcctButton.addStyleName("normalButton");
					buttonGrid.setWidget(0, 0, finAcctButton);
					finAcctButton.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							popup.hide();
							ActionEvent.fire(eventBus, ActionNames.GO_HOME_FINANCIAL_ACCOUNTS, userLoggedIn, true);
						}
					});
					
					Button dismissButton = new Button("Dismiss");
					dismissButton.addStyleName("normalButton");
					buttonGrid.setWidget(0, 1, dismissButton);
					dismissButton.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							popup.hide();
						}
					});
					
					popup.setWidget(vp);
					popup.addStyleDependentName(Constants.STYLE_INFO_POPUP_MESSAGE);
					popup.setAnimationEnabled(true);
					popup.setGlassEnabled(true);
			        popup.show();
			        popup.center();
				}
				else {
					// NOP
					GWT.log("No bad or nearly bad financial accounts for the user logged in.");
				}
			}
		};
		if (this.validateFinancialAccounts) {
			GWT.log("Validating financial accounts...");
			VpcProvisioningService.Util.getInstance().isUserAssociatedToBadSpeedTypes(userLoggedIn, badStCb);
		}
		else {
			GWT.log("Not validating financial accounts...");
		}

		// just to prime the pump for directory person results so the first page load
		// on the account details page won't take so long.  this could be done in a number
		// of places but since we have the account ids, we'll do it here (asynchronously)
		AsyncCallback<List<RoleAssignmentSummaryPojo>> callback = new AsyncCallback<List<RoleAssignmentSummaryPojo>>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception retrieving RoleAssignments", caught);
				getView().hideBackgroundWorkNotice();
			}

			@Override
			public void onSuccess(List<RoleAssignmentSummaryPojo> result) {
				GWT.log("DONE caching DirectoryPerson list for all user's role assignments...");
				getView().hideBackgroundWorkNotice();
			}
		};
		GWT.log("caching DirectoryPerson list for all user's role assignments...");
		List<String> accountIds = new java.util.ArrayList<String>();
		for (int i=0; i<userLoggedIn.getAccountRoles().size(); i++) {
			AccountRolePojo arp = userLoggedIn.getAccountRoles().get(i);
			if (arp.getAccountId() == null) {
				continue;
			}
			else {
				accountIds.add(arp.getAccountId());
			}
		}
		VpcProvisioningService.Util.getInstance().getRoleAssignmentsForAccounts(accountIds, callback);
		
		getView().initPage();
		String linkText = userLoggedIn.getPersonalName().toString() + " (" + userLoggedIn.getPublicId() + ")";
		clientFactory.getShell().setUserName(linkText);
		
		AsyncCallback<String> asCallback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error getting account series info", caught);
				getView().setAccountSeriesInfo("Error");
			}

			@Override
			public void onSuccess(String result) {
				getView().setAccountSeriesInfo(result);
			}
		};
		VpcProvisioningService.Util.getInstance().getAccountSeriesText(asCallback);

		getView().setAccountRoleList(userLoggedIn.getAccountRoles());
		// account affiliation count needs to be calculated because the accountRoles in the user 
		// could have multilple roles for the same account.  so, i need to go through and get a 
		// distinct account count.
		HashMap<String, Integer> accountMap = new HashMap<String, Integer>();
		int totalAccountCount = 0;
		for (AccountRolePojo arp : userLoggedIn.getAccountRoles()) {
			if (arp.getAccountId() != null) {
				Integer acctCount = accountMap.get(arp.getAccountId());
				if (acctCount != null) {
					acctCount++;
				}
				else {
					acctCount = 1;
					accountMap.put(arp.getAccountId(), acctCount);
				}
			}
		}
		Iterator<String> acctIter = accountMap.keySet().iterator();
		while (acctIter.hasNext()) {
			totalAccountCount += accountMap.get(acctIter.next());
		}
		StringBuffer roleInfoHTML = new StringBuffer("You are affiliated to " + totalAccountCount + " distinct account(s).<br/>");
		roleInfoHTML.append("You are assigned to " + userLoggedIn.getAccountRoles().size() + " total roles.<br/>");
		int adminCnt = 0;
		int auditorCnt = 0;
		for (AccountRolePojo arp : userLoggedIn.getAccountRoles()) {
			if (userLoggedIn.isAdminForAccount(arp.getAccountId())) {
				adminCnt++;
			}
			else if (userLoggedIn.isAuditorForAccount(arp.getAccountId())) {
				auditorCnt++;
			}
			else if (userLoggedIn.isCentralAdminForAccount(arp.getAccountId())) {
			}
		}
		roleInfoHTML.append("<p>");
		roleInfoHTML.append(
			"You " + (userLoggedIn.isCentralAdminManager() ? "are" : "are not") + " a central administrator manager.</br>"
			+ "You " + (userLoggedIn.isCentralAdmin() ? "are" : "are not") + " a central administrator.</br>"
			+ "You " + (userLoggedIn.isNetworkAdmin() ? "are" : "are not") + " a network administrator.</br>"
			+ "You are the administrator of " + adminCnt + " account(s).</br>"
			+ "You are the auditor of " + auditorCnt + " account(s).");
		roleInfoHTML.append("</p>");
		getView().setRoleInfoHTML(roleInfoHTML.toString());
		
		// TJ 1/28/2020
		// get all services
		AsyncCallback<ConsoleFeatureQueryResultPojo> svcCallback = new AsyncCallback<ConsoleFeatureQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("[HomePresenter] problem getting console services..." + caught.getMessage());
			}

			@Override
			public void onSuccess(ConsoleFeatureQueryResultPojo result) {
				GWT.log("[HomePresenter] got " + result.getResults().size() + " console services back.");
				if (result != null) {
					if (result.getResults() != null) {
						// set the console services on the view
						getView().setConsoleFeatures(result.getResults());
					}
				}
			}
		};
		GWT.log("[AwsServiceRpcSuggestOracle.constructor] getting services");
		ConsoleFeatureQueryFilterPojo filter = new ConsoleFeatureQueryFilterPojo();
		VpcProvisioningService.Util.getInstance().getConsoleFeaturesForFilter(filter, svcCallback);

		
		// TJ 1/28/2020
		// get recently visited services
		AsyncCallback<ConsoleFeatureQueryResultPojo> recentSvcsCB = new AsyncCallback<ConsoleFeatureQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("[HomePresenter] problem getting recently used console services..." + caught.getMessage());
			}

			@Override
			public void onSuccess(ConsoleFeatureQueryResultPojo result) {
				GWT.log("[HomePresenter] got " + result.getResults().size() + " recently used console services back.");
				if (result != null) {
					if (result.getResults() != null) {
						// set the console services on the view
						getView().setRecentlyUsedConsoleFeatures(result.getResults());
					}
				}
			}
		};
		GWT.log("[AwsServiceRpcSuggestOracle.constructor] getting services");
		VpcProvisioningService.Util.getInstance().getCachedConsoleFeaturesForUserLoggedIn(recentSvcsCB);
		
		getView().hidePleaseWaitDialog();
		getView().hidePleaseWaitPanel();
		getView().setFieldViolations(false);
		getView().setInitialFocus();
	}

	@Override
	public void stop() {
		eventBus = null;
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	private HomeView getView() {
		return clientFactory.getHomeView();
	}

	@Override
	public EventBus getEventBus() {
		return this.eventBus;
	}

	@Override
	public ClientFactory getClientFactory() {
		return this.clientFactory;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public UserAccountPojo getUserLoggedIn() {
		return userLoggedIn;
	}

	public void setUserLoggedIn(UserAccountPojo userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

	@Override
	public void viewAccountForId(String accountId) {
		AsyncCallback<AccountQueryResultPojo> callback = new AsyncCallback<AccountQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving account information.  " +
						"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(AccountQueryResultPojo result) {
				getView().hidePleaseWaitPanel();
				if (result != null) {
					if (result.getResults().size() == 1) {
						ActionEvent.fire(getEventBus(), ActionNames.MAINTAIN_ACCOUNT_FROM_HOME, result.getResults().get(0));
					}
					else {
						// error, incorrect number of accounts returned
					}
				}
				else {
					// error
				}
			}
		};
		getView().showPleaseWaitDialog("Retrieving account metadata from the AWS Account Service...");
		AccountQueryFilterPojo filter = new AccountQueryFilterPojo();
		filter.setAccountId(accountId);
		VpcProvisioningService.Util.getInstance().getAccountsForFilter(filter, callback);
	}

	@Override
	public String getDetailedRoleInfoHTML() {
		StringBuffer sbuf = new StringBuffer();
		boolean isFirst=true;
		for (AccountRolePojo arp : userLoggedIn.getAccountRoles()) {
			if (isFirst) {
				isFirst = false;
			}
			else {
				sbuf.append("<br/>");
			}
			sbuf.append("Account: " + (arp.getAccountId() != null ? (arp.getAccountId() + " (" + arp.getAccountName() + ")") : "N/A") + ", Role: " + arp.getRoleName());
		}
		return sbuf.toString();
	}

	@Override
	public String getDetailedDirectoryInfoHTML() {
		AsyncCallback<DirectoryPersonQueryResultPojo> srvrCallback = new AsyncCallback<DirectoryPersonQueryResultPojo>() {

			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				GWT.log("Exception retrieving DirectoryPerson information.", caught);
				getView().showDirectoryPersonInfoPopup("Exception retrieving DirectoryPerson info.  "
						+ "Exception: " + caught.getMessage());
			}

			@Override
			public void onSuccess(DirectoryPersonQueryResultPojo result) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				if (result == null) {
					getView().showDirectoryPersonInfoPopup("NULL DirectoryPerson "
							+ "object returned from server");
				}
				// There may be more than one returned but we'll just take the first one.
				// this is because now we're pulling back EHC people too which may lead 
				// to duplicate records returned for the same public id.  This is okay but 
				// we only need one of them.
//				else if (result.getResults().size() != 1) {
//					getView().showDirectoryPersonInfoPopup("Invalid number of DirectoryPerson objects "
//							+ "returned from server.  Got " + result.getResults().size() + " expected 1");
//				}
				else {
					DirectoryPersonPojo dp = result.getResults().get(0);
					getView().showDirectoryPersonInfoPopup(dp.toString());
				}
			}
			
		};
		DirectoryPersonQueryFilterPojo filter = new DirectoryPersonQueryFilterPojo();
		filter.setKey(userLoggedIn.getPublicId());
		getView().showPleaseWaitDialog("Retrieving Directory meta data from the Directory Service...");
		VpcProvisioningService.Util.getInstance().getDirectoryPersonsForFilter(filter, srvrCallback);
		return null;
	}

	@Override
	public String getDetailedPersonInfoHTML() {
		AsyncCallback<PersonInfoSummaryPojo> srvrCallback = new AsyncCallback<PersonInfoSummaryPojo>() {

			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				GWT.log("Exception retrieving Person Summary information.", caught);
				getView().showPersonSummaryPopup("Exception retrieving Person Summary info.  "
						+ "Exception: " + caught.getMessage());
			}

			@Override
			public void onSuccess(PersonInfoSummaryPojo result) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				if (result == null) {
					getView().showDirectoryPersonInfoPopup("NULL Person Summary "
							+ "info returned from server");
				}
				else {
					StringBuffer html = new StringBuffer();
					if (result.getFullPerson() != null) {
						html.append(result.getFullPerson().toString());
					}
					else {
						html.append("No FullPerson info available");
					}
					
					html.append("</br>");
					
					AccountProvisioningAuthorizationPojo apa = result.getAccountProvisioningAuthorization();
					if (apa != null) {
						if (apa.isAuthorized()) {
							html.append("<p>You ARE authorized to perform Account Provisioning");
							html.append("</br>");
							html.append(apa.getAuthorizedUserDescription() + "</p>");
						}
						else {
							html.append("<p>You ARE NOT authorized to perform Account Provisioning</p>");
							html.append("<ul>");
							for (String desc : apa.getUnauthorizedReasons()) {
								html.append("<li>" + desc + "</li>");
							}
							html.append("</ul>");
						}
					}
					else {
						html.append("No AccountProvisioningAuthorization info available.");
					}

					getView().showPersonSummaryPopup(html.toString());
				}
			}
			
		};
		GWT.log("Getting person info summary for public id: " + userLoggedIn.getPublicId());
		getView().showPleaseWaitDialog("Getting person meta data from the Identity Service...");
		VpcProvisioningService.Util.getInstance().getPersonInfoSummaryForPublicId(userLoggedIn.getPublicId(), srvrCallback);
		return null;
	}

	public DirectoryPersonPojo getDirectoryPerson() {
		return directoryPerson;
	}

	public void setDirectoryPerson(DirectoryPersonPojo directoryPerson) {
		this.directoryPerson = directoryPerson;
	}

	@Override
	public String lookupPersonInfoHTML(final DirectoryPersonPojo directoryPerson) {
		if (directoryPerson == null) {
			getView().showMessageToUser("Please enter a person's name to lookup.");
			return null;
		}
		AsyncCallback<PersonInfoSummaryPojo> srvrCallback = new AsyncCallback<PersonInfoSummaryPojo>() {

			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				GWT.log("Exception retrieving Person Summary information.", caught);
				getView().showPersonSummaryPopup("Exception retrieving Person Summary info.  "
						+ "Exception: " + caught.getMessage());
			}

			@Override
			public void onSuccess(PersonInfoSummaryPojo result) {
				getView().hidePleaseWaitPanel();
				getView().hidePleaseWaitDialog();
				if (result == null) {
					getView().showPersonSummaryLookupPopup("NULL Person Summary "
							+ "info returned from server");
				}
				else {
					StringBuffer html = new StringBuffer();
//					if (result.getFullPerson() != null) {
//						html.append(result.getFullPerson().toString());
//					}
//					else {
//						html.append("No FullPerson info available");
//					}
//					
//					html.append("</br>");
					
					AccountProvisioningAuthorizationPojo apa = result.getAccountProvisioningAuthorization();
					if (apa != null) {
						if (apa.isAuthorized()) {
							html.append("<p>" + directoryPerson.getFullName() + 
								" (" + directoryPerson.getKey() + ") IS authorized to perform Account Provisioning");
							html.append("</br>");
							html.append(apa.getAuthorizedUserDescription() + "</p>");
						}
						else {
							html.append("<p>" + directoryPerson.getFullName() + 
									" (" + directoryPerson.getKey() + ") IS NOT authorized to perform Account Provisioning");
							html.append("</br>");
							html.append(apa.getAuthorizedUserDescription() + "</p>");
//							html.append("<ul>");
//							for (String desc : apa.getUnauthorizedReasons()) {
//								html.append("<li>" + desc + "</li>");
//							}
//							html.append("</ul>");
						}
					}
					else {
						html.append("No AccountProvisioningAuthorization info available.");
					}

					getView().showPersonSummaryLookupPopup(html.toString());
				}
			}
			
		};
		GWT.log("Looking up person info summary for public id: " + directoryPerson.getKey());
		getView().showPleaseWaitDialog("Looking up person meta data from the Identity Service...");
		VpcProvisioningService.Util.getInstance().getPersonInfoSummaryForPublicId(directoryPerson.getKey(), srvrCallback);
		return null;
	}

	@Override
	public void saveConsoleFeatureInCacheForUser(ConsoleFeaturePojo service, UserAccountPojo user) {
		AsyncCallback<Void> cb = new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("error saving console service in the server's cache...", caught);
			}

			@Override
			public void onSuccess(Void result) {
				GWT.log("saved console service in the server's cache...");
			}
			
		};
		VpcProvisioningService.Util.getInstance().saveConsoleFeatureInCacheForUser(service, user, cb);
	}

	public boolean isValidateFinancialAccounts() {
		return validateFinancialAccounts;
	}

	public void setValidateFinancialAccounts(boolean validateFinancialAccounts) {
		this.validateFinancialAccounts = validateFinancialAccounts;
	}

}
