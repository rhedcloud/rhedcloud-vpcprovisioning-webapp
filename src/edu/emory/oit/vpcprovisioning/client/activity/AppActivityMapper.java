package edu.emory.oit.vpcprovisioning.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.presenter.account.ListAccountPlace;
import edu.emory.oit.vpcprovisioning.presenter.account.ListAccountPresenter;
import edu.emory.oit.vpcprovisioning.presenter.account.MaintainAccountPlace;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.AccountProvisioningStatusPlace;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.DeprovisionAccountPlace;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.ListAccountProvisioningPlace;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.ListAccountProvisioningPresenter;
import edu.emory.oit.vpcprovisioning.presenter.bill.BillSummaryPlace;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ListCentralAdminPlace;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ListCentralAdminPresenter;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.MaintainCentralAdminRoleAssignmentsPlace;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.MaintainCentralAdminRoleAssignmentsPresenter;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ManageSrdBehaviorPlace;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ManageSrdBehaviorPresenter;
import edu.emory.oit.vpcprovisioning.presenter.cidr.ListCidrPlace;
import edu.emory.oit.vpcprovisioning.presenter.cidr.ListCidrPresenter;
import edu.emory.oit.vpcprovisioning.presenter.cidr.MaintainCidrPlace;
import edu.emory.oit.vpcprovisioning.presenter.cidrassignment.ListCidrAssignmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.cidrassignment.ListCidrAssignmentPresenter;
import edu.emory.oit.vpcprovisioning.presenter.cidrassignment.MaintainCidrAssignmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.elasticip.ListElasticIpPlace;
import edu.emory.oit.vpcprovisioning.presenter.elasticip.ListElasticIpPresenter;
import edu.emory.oit.vpcprovisioning.presenter.elasticip.MaintainElasticIpPlace;
import edu.emory.oit.vpcprovisioning.presenter.elasticipassignment.ListElasticIpAssignmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.elasticipassignment.ListElasticIpAssignmentPresenter;
import edu.emory.oit.vpcprovisioning.presenter.elasticipassignment.MaintainElasticIpAssignmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.finacct.ListFinancialAccountsPlace;
import edu.emory.oit.vpcprovisioning.presenter.finacct.ListFinancialAccountsPresenter;
import edu.emory.oit.vpcprovisioning.presenter.firewall.ListFirewallRulePlace;
import edu.emory.oit.vpcprovisioning.presenter.firewall.ListFirewallRulePresenter;
import edu.emory.oit.vpcprovisioning.presenter.home.HomePlace;
import edu.emory.oit.vpcprovisioning.presenter.incident.MaintainIncidentPlace;
import edu.emory.oit.vpcprovisioning.presenter.notification.ListNotificationPlace;
import edu.emory.oit.vpcprovisioning.presenter.notification.ListNotificationPresenter;
import edu.emory.oit.vpcprovisioning.presenter.notification.MaintainAccountNotificationPlace;
import edu.emory.oit.vpcprovisioning.presenter.notification.MaintainNotificationPlace;
import edu.emory.oit.vpcprovisioning.presenter.resourcetagging.ListResourceTaggingProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.resourcetagging.ListResourceTaggingProfilePresenter;
import edu.emory.oit.vpcprovisioning.presenter.resourcetagging.MaintainResourceTaggingProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.role.ListRoleProvisioningPlace;
import edu.emory.oit.vpcprovisioning.presenter.role.ListRoleProvisioningPresenter;
import edu.emory.oit.vpcprovisioning.presenter.role.MaintainRoleProvisioningPlace;
import edu.emory.oit.vpcprovisioning.presenter.role.RoleProvisioningStatusPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.CalculateSecurityRiskPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ListSecurityRiskPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ListSecurityRiskPresenter;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceControlPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceControlPresenter;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceGuidelinePlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceGuidelinePresenter;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServicePlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServicePresenter;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainSecurityAssessmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainSecurityRiskPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceControlPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceGuidelinePlace;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServicePlace;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceTestPlanPlace;
import edu.emory.oit.vpcprovisioning.presenter.service.ServiceAssessmentReportPlace;
import edu.emory.oit.vpcprovisioning.presenter.srd.MaintainSrdPlace;
import edu.emory.oit.vpcprovisioning.presenter.staticnat.ListStaticNatProvisioningSummaryPlace;
import edu.emory.oit.vpcprovisioning.presenter.staticnat.ListStaticNatProvisioningSummaryPresenter;
import edu.emory.oit.vpcprovisioning.presenter.staticnat.StaticNatProvisioningStatusPlace;
import edu.emory.oit.vpcprovisioning.presenter.tou.MaintainTermsOfUseAgreementPlace;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayConnectionProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayConnectionProfilePresenter;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayPlace;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayPresenter;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.MaintainTransitGatewayConnectionProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.MaintainTransitGatewayPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpc.ListVpcPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpc.ListVpcPresenter;
import edu.emory.oit.vpcprovisioning.presenter.vpc.MaintainVpcPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpc.RegisterVpcPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.ListVpcpPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.ListVpcpPresenter;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.MaintainVpcpPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.VpcpStatusPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProfilePresenter;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProvisioningPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProvisioningPresenter;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProfileAssignmentPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProfilePlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProvisioningPlace;
import edu.emory.oit.vpcprovisioning.presenter.vpn.VpncpStatusPlace;

public class AppActivityMapper implements ActivityMapper {
	private final ClientFactory clientFactory;

	public AppActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}


	@Override
	public Activity getActivity(final Place place) {
		if (place instanceof ListFirewallRulePlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListFirewallRulePresenter presenter = new ListFirewallRulePresenter(clientFactory, (ListFirewallRulePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		if (place instanceof ListNotificationPlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListNotificationPresenter presenter = new ListNotificationPresenter(clientFactory, (ListNotificationPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		if (place instanceof ListServicePlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListServicePresenter presenter = new ListServicePresenter(clientFactory, (ListServicePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		if (place instanceof ListVpcPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListVpcPresenter presenter = new ListVpcPresenter(clientFactory, (ListVpcPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		if (place instanceof ListAccountPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListAccountPresenter presenter = new ListAccountPresenter(clientFactory, (ListAccountPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}
		if (place instanceof ListCidrPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListCidrPresenter presenter = new ListCidrPresenter(clientFactory, (ListCidrPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}

				/*
				 * Note no call to presenter.stop(). The CaseRecordListViews do that
				 * themselves as a side effect of setPresenter.
				 */
			};
		}
		if (place instanceof ListCidrAssignmentPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListCidrAssignmentPresenter presenter = new ListCidrAssignmentPresenter(clientFactory, (ListCidrAssignmentPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}

				/*
				 * Note no call to presenter.stop(). The CaseRecordListViews do that
				 * themselves as a side effect of setPresenter.
				 */
			};
		}

		if (place instanceof ListVpcpPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListVpcpPresenter presenter = new ListVpcpPresenter(clientFactory, (ListVpcpPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}

				/*
				 * Note no call to presenter.stop(). The CaseRecordListViews do that
				 * themselves as a side effect of setPresenter.
				 */
			};
		}

		if (place instanceof ListElasticIpPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListElasticIpPresenter presenter = new ListElasticIpPresenter(clientFactory, (ListElasticIpPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListElasticIpAssignmentPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListElasticIpAssignmentPresenter presenter = new ListElasticIpAssignmentPresenter(clientFactory, (ListElasticIpAssignmentPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListCentralAdminPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListCentralAdminPresenter presenter = new ListCentralAdminPresenter(clientFactory, (ListCentralAdminPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListSecurityRiskPlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListSecurityRiskPresenter presenter = new ListSecurityRiskPresenter(clientFactory, (ListSecurityRiskPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListServiceControlPlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListServiceControlPresenter presenter = new ListServiceControlPresenter(clientFactory, (ListServiceControlPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListServiceGuidelinePlace) {
			// The list of services
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListServiceGuidelinePresenter presenter = new ListServiceGuidelinePresenter(clientFactory, (ListServiceGuidelinePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListStaticNatProvisioningSummaryPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListStaticNatProvisioningSummaryPresenter presenter = new ListStaticNatProvisioningSummaryPresenter(clientFactory, (ListStaticNatProvisioningSummaryPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}

				/*
				 * Note no call to presenter.stop(). The CaseRecordListViews do that
				 * themselves as a side effect of setPresenter.
				 */
			};
		}

		if (place instanceof ListVpnConnectionProfilePlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListVpnConnectionProfilePresenter presenter = new ListVpnConnectionProfilePresenter(clientFactory, (ListVpnConnectionProfilePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListVpnConnectionProvisioningPlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListVpnConnectionProvisioningPresenter presenter = new ListVpnConnectionProvisioningPresenter(clientFactory, (ListVpnConnectionProvisioningPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}

				/*
				 * Note no call to presenter.stop(). The CaseRecordListViews do that
				 * themselves as a side effect of setPresenter.
				 */
			};
		}

		if (place instanceof ListResourceTaggingProfilePlace) {
			// The list of case records.
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListResourceTaggingProfilePresenter presenter = new ListResourceTaggingProfilePresenter(clientFactory, (ListResourceTaggingProfilePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListAccountProvisioningPlace) {
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListAccountProvisioningPresenter presenter = new ListAccountProvisioningPresenter(clientFactory, (ListAccountProvisioningPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListFinancialAccountsPlace) {
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListFinancialAccountsPresenter presenter = new ListFinancialAccountsPresenter(clientFactory, (ListFinancialAccountsPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListRoleProvisioningPlace) {
			// The list of role provisioning runs
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListRoleProvisioningPresenter presenter = new ListRoleProvisioningPresenter(clientFactory, (ListRoleProvisioningPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListTransitGatewayPlace) {
			// The list of role provisioning runs
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListTransitGatewayPresenter presenter = new ListTransitGatewayPresenter(clientFactory, (ListTransitGatewayPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ListTransitGatewayConnectionProfilePlace) {
			// The list of role provisioning runs
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ListTransitGatewayConnectionProfilePresenter presenter = new ListTransitGatewayConnectionProfilePresenter(clientFactory, (ListTransitGatewayConnectionProfilePlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof MaintainCentralAdminRoleAssignmentsPlace) {
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					MaintainCentralAdminRoleAssignmentsPresenter presenter = new MaintainCentralAdminRoleAssignmentsPresenter(clientFactory, (MaintainCentralAdminRoleAssignmentsPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof ManageSrdBehaviorPlace) {
			return new AbstractActivity() {
				@Override
				public void start(AcceptsOneWidget panel, EventBus eventBus) {
					ManageSrdBehaviorPresenter presenter = new ManageSrdBehaviorPresenter(clientFactory, (ManageSrdBehaviorPlace) place);
					presenter.start(eventBus);
					panel.setWidget(presenter);
				}
			};
		}

		if (place instanceof BillSummaryPlace) {
			// Generate/Maintain vpcp
			return new BillSummaryActivity(clientFactory, (BillSummaryPlace) place);
		}

		if (place instanceof VpcpStatusPlace) {
			// Generate/Maintain vpcp
			return new VpcpStatusActivity(clientFactory, (VpcpStatusPlace) place);
		}

		if (place instanceof MaintainVpcpPlace) {
			// Generate/Maintain vpcp
			return new MaintainVpcpActivity(clientFactory, (MaintainVpcpPlace) place);
		}

		if (place instanceof MaintainVpcPlace) {
			// Maintain vpc
			return new MaintainVpcActivity(clientFactory, (MaintainVpcPlace) place);
		}

		if (place instanceof RegisterVpcPlace) {
			// Maintain vpc
			return new RegisterVpcActivity(clientFactory, (RegisterVpcPlace) place);
		}

		if (place instanceof MaintainAccountPlace) {
			// Maintain account
			return new MaintainAccountActivity(clientFactory, (MaintainAccountPlace) place);
		}

		if (place instanceof MaintainCidrPlace) {
			// maintain cidr
			return new MaintainCidrActivity(clientFactory, (MaintainCidrPlace) place);
		}

		if (place instanceof MaintainCidrAssignmentPlace) {
			// maintain cidr assignment
			return new MaintainCidrAssignmentActivity(clientFactory, (MaintainCidrAssignmentPlace) place);
		}

		if (place instanceof MaintainElasticIpPlace) {
			// maintain cidr
			return new MaintainElasticIpActivity(clientFactory, (MaintainElasticIpPlace) place);
		}

		if (place instanceof MaintainElasticIpAssignmentPlace) {
			// maintain cidr
			return new MaintainElasticIpAssignmentActivity(clientFactory, (MaintainElasticIpAssignmentPlace) place);
		}

		if (place instanceof MaintainServicePlace) {
			// Maintain service
			return new MaintainServiceActivity(clientFactory, (MaintainServicePlace) place);
		}

		if (place instanceof MaintainNotificationPlace) {
			// Maintain service
			return new MaintainNotificationActivity(clientFactory, (MaintainNotificationPlace) place);
		}

		if (place instanceof HomePlace) {
			// Maintain service
			return new HomeActivity(clientFactory, (HomePlace) place);
		}

		if (place instanceof MaintainSecurityAssessmentPlace) {
			// Maintain service
			return new MaintainSecurityAssessmentActivity(clientFactory, (MaintainSecurityAssessmentPlace) place);
		}

		if (place instanceof MaintainSecurityRiskPlace) {
			// Maintain service
			return new MaintainSecurityRiskActivity(clientFactory, (MaintainSecurityRiskPlace) place);
		}

		if (place instanceof MaintainAccountNotificationPlace) {
			// Maintain account notification
			return new MaintainAccountNotificationActivity(clientFactory, (MaintainAccountNotificationPlace) place);
		}

		if (place instanceof MaintainServiceControlPlace) {
			// Maintain service control
			return new MaintainServiceControlActivity(clientFactory, (MaintainServiceControlPlace) place);
		}

		if (place instanceof MaintainServiceGuidelinePlace) {
			// Maintain service control
			return new MaintainServiceGuidelineActivity(clientFactory, (MaintainServiceGuidelinePlace) place);
		}

		if (place instanceof MaintainServiceTestPlanPlace) {
			// Maintain service control
			return new MaintainServiceTestPlanActivity(clientFactory, (MaintainServiceTestPlanPlace) place);
		}

		if (place instanceof MaintainSrdPlace) {
			// View/Maintain srd
			return new MaintainSrdActivity(clientFactory, (MaintainSrdPlace) place);
		}

		if (place instanceof MaintainTermsOfUseAgreementPlace) {
			// View/Maintain srd
			return new MaintainTermsOfUseAgreementActivity(clientFactory, (MaintainTermsOfUseAgreementPlace) place);
		}

		if (place instanceof MaintainIncidentPlace) {
			return new MaintainIncidentActivity(clientFactory, (MaintainIncidentPlace) place);
		}

		if (place instanceof MaintainVpnConnectionProfilePlace) {
			return new MaintainVpnConnectionProfileActivity(clientFactory, (MaintainVpnConnectionProfilePlace) place);
		}

		if (place instanceof MaintainVpnConnectionProfileAssignmentPlace) {
			return new MaintainVpnConnectionProfileAssignmentActivity(clientFactory, (MaintainVpnConnectionProfileAssignmentPlace) place);
		}

		if (place instanceof StaticNatProvisioningStatusPlace) {
			return new StaticNatStatusActivity(clientFactory, (StaticNatProvisioningStatusPlace) place);
		}

		if (place instanceof VpncpStatusPlace) {
			return new VpncpStatusActivity(clientFactory, (VpncpStatusPlace) place);
		}

		if (place instanceof MaintainVpnConnectionProvisioningPlace) {
			return new MaintainVpnConnectionProvisioningActivity(clientFactory, (MaintainVpnConnectionProvisioningPlace) place);
		}

		if (place instanceof ServiceAssessmentReportPlace) {
			return new ServiceAssessmentReportActivity(clientFactory, (ServiceAssessmentReportPlace) place);
		}

		if (place instanceof MaintainResourceTaggingProfilePlace) {
			return new MaintainResourceTaggingProfileActivity(clientFactory, (MaintainResourceTaggingProfilePlace) place);
		}

		if (place instanceof CalculateSecurityRiskPlace) {
			return new CalculateSecurityRiskActivity(clientFactory, (CalculateSecurityRiskPlace) place);
		}
		
		if (place instanceof DeprovisionAccountPlace) {
			return new DeprovisionAccountActivity(clientFactory, (DeprovisionAccountPlace) place);
		}
		
		if (place instanceof AccountProvisioningStatusPlace) {
			return new AccountProvisioningStatusActivity(clientFactory, (AccountProvisioningStatusPlace) place);
		}

		if (place instanceof RoleProvisioningStatusPlace) {
			return new RoleProvisioningStatusActivity(clientFactory, (RoleProvisioningStatusPlace) place);
		}

		if (place instanceof MaintainRoleProvisioningPlace) {
			return new MaintainRoleProvisioningActivity(clientFactory, (MaintainRoleProvisioningPlace) place);
		}

		if (place instanceof MaintainTransitGatewayPlace) {
			return new MaintainTransitGatewayActivity(clientFactory, (MaintainTransitGatewayPlace) place);
		}

		if (place instanceof MaintainTransitGatewayConnectionProfilePlace) {
			return new MaintainTransitGatewayConnectionProfileActivity(clientFactory, (MaintainTransitGatewayConnectionProfilePlace) place);
		}

		return null;
	}


	public ClientFactory getClientFactory() {
		return clientFactory;
	}

}
