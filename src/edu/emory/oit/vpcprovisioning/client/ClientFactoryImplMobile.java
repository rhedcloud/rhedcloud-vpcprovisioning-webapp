package edu.emory.oit.vpcprovisioning.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import edu.emory.oit.vpcprovisioning.client.activity.AppActivityMapper;
import edu.emory.oit.vpcprovisioning.client.activity.AppPlaceHistoryMapper;
import edu.emory.oit.vpcprovisioning.client.mobile.MobileAppShell;
import edu.emory.oit.vpcprovisioning.client.mobile.MobileHome;
import edu.emory.oit.vpcprovisioning.client.mobile.MobileListAccount;
import edu.emory.oit.vpcprovisioning.presenter.account.ListAccountView;
import edu.emory.oit.vpcprovisioning.presenter.account.MaintainAccountView;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.AccountProvisioningStatusView;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.DeprovisionAccountView;
import edu.emory.oit.vpcprovisioning.presenter.acctprovisioning.ListAccountProvisioningView;
import edu.emory.oit.vpcprovisioning.presenter.bill.BillSummaryView;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ListCentralAdminView;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.MaintainCentralAdminRoleAssignmentsView;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ManageSrdBehaviorView;
import edu.emory.oit.vpcprovisioning.presenter.cidr.ListCidrView;
import edu.emory.oit.vpcprovisioning.presenter.cidr.MaintainCidrView;
import edu.emory.oit.vpcprovisioning.presenter.cidrassignment.ListCidrAssignmentView;
import edu.emory.oit.vpcprovisioning.presenter.cidrassignment.MaintainCidrAssignmentView;
import edu.emory.oit.vpcprovisioning.presenter.elasticip.ListElasticIpView;
import edu.emory.oit.vpcprovisioning.presenter.elasticip.MaintainElasticIpView;
import edu.emory.oit.vpcprovisioning.presenter.elasticipassignment.ListElasticIpAssignmentView;
import edu.emory.oit.vpcprovisioning.presenter.elasticipassignment.MaintainElasticIpAssignmentView;
import edu.emory.oit.vpcprovisioning.presenter.finacct.ListFinancialAccountsView;
import edu.emory.oit.vpcprovisioning.presenter.firewall.ListFirewallRuleView;
import edu.emory.oit.vpcprovisioning.presenter.firewall.MaintainFirewallExceptionRequestView;
import edu.emory.oit.vpcprovisioning.presenter.home.HomeView;
import edu.emory.oit.vpcprovisioning.presenter.incident.MaintainIncidentView;
import edu.emory.oit.vpcprovisioning.presenter.notification.ListNotificationView;
import edu.emory.oit.vpcprovisioning.presenter.notification.MaintainAccountNotificationView;
import edu.emory.oit.vpcprovisioning.presenter.notification.MaintainNotificationView;
import edu.emory.oit.vpcprovisioning.presenter.resourcetagging.ListResourceTaggingProfileView;
import edu.emory.oit.vpcprovisioning.presenter.resourcetagging.MaintainResourceTaggingProfileView;
import edu.emory.oit.vpcprovisioning.presenter.role.ListRoleProvisioningView;
import edu.emory.oit.vpcprovisioning.presenter.role.MaintainRoleProvisioningView;
import edu.emory.oit.vpcprovisioning.presenter.role.RoleProvisioningStatusView;
import edu.emory.oit.vpcprovisioning.presenter.service.CalculateSecurityRiskView;
import edu.emory.oit.vpcprovisioning.presenter.service.ListSecurityRiskView;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceControlView;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceGuidelineView;
import edu.emory.oit.vpcprovisioning.presenter.service.ListServiceView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainSecurityAssessmentView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainSecurityRiskView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceControlView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceGuidelineView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceTestPlanView;
import edu.emory.oit.vpcprovisioning.presenter.service.MaintainServiceView;
import edu.emory.oit.vpcprovisioning.presenter.service.ServiceAssessmentReportView;
import edu.emory.oit.vpcprovisioning.presenter.srd.MaintainSrdView;
import edu.emory.oit.vpcprovisioning.presenter.staticnat.ListStaticNatProvisioningSummaryView;
import edu.emory.oit.vpcprovisioning.presenter.staticnat.StaticNatProvisioningStatusView;
import edu.emory.oit.vpcprovisioning.presenter.tou.MaintainTermsOfUseAgreementView;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayConnectionProfileView;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.ListTransitGatewayView;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.MaintainTransitGatewayConnectionProfileView;
import edu.emory.oit.vpcprovisioning.presenter.transitgateway.MaintainTransitGatewayView;
import edu.emory.oit.vpcprovisioning.presenter.vpc.ListVpcView;
import edu.emory.oit.vpcprovisioning.presenter.vpc.MaintainVpcView;
import edu.emory.oit.vpcprovisioning.presenter.vpc.RegisterVpcView;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.ListVpcpView;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.MaintainVpcpView;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.VpcpStatusView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProfileView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.ListVpnConnectionProvisioningView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProfileAssignmentView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProfileView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.MaintainVpnConnectionProvisioningView;
import edu.emory.oit.vpcprovisioning.presenter.vpn.VpncpStatusView;

public class ClientFactoryImplMobile implements ClientFactory {
	private final EventBus eventBus = new SimpleEventBus();
	private final PlaceController placeController = new PlaceController(eventBus);
	private AppShell shell;
	private ActivityManager activityManager;

	private final AppPlaceHistoryMapper historyMapper = GWT.create(AppPlaceHistoryMapper.class);

	/**
	 * The stock GWT class that ties the PlaceController to browser history,
	 * configured by our custom {@link #historyMapper}.
	 */
	private final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);

	private ListAccountView accountListView;
	private HomeView homeView;
	protected ActivityManager getActivityManager() {
        if (activityManager == null) {
            activityManager = new ActivityManager(createActivityMapper(), eventBus);
        }
        return activityManager;
    }
    /**
     * ActivityMapper determines an Activity to run for a particular place,
     * configures the {@link #getActivityManager()}
     */
    protected ActivityMapper createActivityMapper() {
        return new AppActivityMapper(this);
    }

	@Override
	public AppBootstrapper getApp() {
		return new AppBootstrapper(this, eventBus, getPlaceController(),
				getActivityManager(), historyMapper, historyHandler, 
				getShell());
	}

	@Override
	public EventBus getEventBus() {
		
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		
		return placeController;
	}

	@Override
	public AppShell getShell() {
		if (shell == null) {
			shell = createShell();
		}
		return shell;
	}

	protected AppShell createShell() {
		return new MobileAppShell(getEventBus(), this);
	}

	@Override
	public ListVpcView getListVpcView() {
		
		return null;
	}

	@Override
	public MaintainVpcView getMaintainVpcView() {
		
		return null;
	}

	@Override
	public ListAccountView getListAccountView() {
        if (accountListView == null) {
        	accountListView = createListAccountView();
        }
        return accountListView;
	}
    protected ListAccountView createListAccountView() {
        return new MobileListAccount();
    }

	@Override
	public MaintainAccountView getMaintainAccountView() {
		
		return null;
	}

	@Override
	public ListCidrView getListCidrView() {
		
		return null;
	}

	@Override
	public MaintainCidrView getMaintainCidrView() {
		
		return null;
	}

	@Override
	public ListCidrAssignmentView getListCidrAssignmentView() {
		
		return null;
	}

	@Override
	public MaintainCidrAssignmentView getMaintainCidrAssignmentView() {
		
		return null;
	}
	@Override
	public RegisterVpcView getRegisterVpcView() {
		
		return null;
	}
	@Override
	public ListVpcpView getListVpcpView() {
		
		return null;
	}
	@Override
	public MaintainVpcpView getMaintainVpcpView() {
		
		return null;
	}
	@Override
	public VpcpStatusView getVpcpStatusView() {
		
		return null;
	}
	@Override
	public BillSummaryView getBillSummaryView() {
		
		return null;
	}
	@Override
	public ListElasticIpView getListElasticIpView() {
		
		return null;
	}
	@Override
	public MaintainElasticIpView getMaintainElasticIpView() {
		
		return null;
	}
	@Override
	public MaintainServiceView getMaintainServiceView() {
		return null;
	}
	@Override
	public ListNotificationView getListNotificationView() {
		return null;
	}
	@Override
	public MaintainNotificationView getMaintainNotificationView() {
		return null;
	}
	@Override
	public ListElasticIpAssignmentView getListElasticIpAssignmentView() {
		
		return null;
	}
	@Override
	public MaintainElasticIpAssignmentView getMaintainElasticIpAssignmentView() {
		
		return null;
	}
	@Override
	public ListServiceView getListServiceView() {
		
		return null;
	}
	@Override
	public ListFirewallRuleView getListFirewallRuleView() {
		
		return null;
	}
	@Override
	public MaintainFirewallExceptionRequestView getMaintainFirewallExceptionRequestView() {
		
		return null;
	}
	@Override
	public HomeView getHomeView() {
		if (homeView == null) {
			homeView = createHomeView();
		}
		return homeView;
	}
	protected HomeView createHomeView() {
		return new MobileHome();
	}
	@Override
	public ListCentralAdminView getListCentralAdminView() {
		
		return null;
	}
	@Override
	public MaintainSecurityAssessmentView getMaintainSecurityAssessmentView() {
		
		return null;
	}
	@Override
	public ListSecurityRiskView getListSecurityRiskView() {
		
		return null;
	}
	@Override
	public MaintainAccountNotificationView getMaintainAccountNotificationView() {
		
		return null;
	}
	@Override
	public MaintainSecurityRiskView getMaintainSecurityRiskView() {
		
		return null;
	}
	@Override
	public ListServiceControlView getListServiceControlView() {
		
		return null;
	}
	@Override
	public MaintainServiceControlView getMaintainServiceControlView() {
		
		return null;
	}
	@Override
	public ListServiceGuidelineView getListServiceGuidelineView() {
		
		return null;
	}
	@Override
	public MaintainServiceGuidelineView getMaintainServiceGuidelineView() {
		
		return null;
	}
	@Override
	public MaintainServiceTestPlanView getMaintainServiceTestPlanView() {
		
		return null;
	}
	@Override
	public MaintainSrdView getMaintainSrdView() {
		
		return null;
	}
	@Override
	public MaintainTermsOfUseAgreementView getMaintainTermsOfUseAgreementView() {
		
		return null;
	}
	@Override
	public MaintainIncidentView getMaintainIncidentView() {
		
		return null;
	}
	@Override
	public ListStaticNatProvisioningSummaryView getListStaticNatProvisioningSummaryView() {
		
		return null;
	}
	@Override
	public ListVpnConnectionProfileView getListVpnConnectionProfileView() {
		
		return null;
	}
	@Override
	public MaintainVpnConnectionProfileView getMaintainVpnConnectionProfileView() {
		
		return null;
	}
	@Override
	public StaticNatProvisioningStatusView getStaticNatProvisioningStatusView() {
		
		return null;
	}
	@Override
	public MaintainVpnConnectionProfileAssignmentView getMaintainVpnConnectionProfileAssignmentView() {
		
		return null;
	}
	@Override
	public ListVpnConnectionProvisioningView getListVpnConnectionProvisioningView() {
		
		return null;
	}
	@Override
	public VpncpStatusView getVpncpStatusView() {
		
		return null;
	}
	@Override
	public MaintainVpnConnectionProvisioningView getMaintainVpnConnectionProvisioningView() {
		
		return null;
	}
	@Override
	public ServiceAssessmentReportView getServiceAssessmentReportView() {
		
		return null;
	}
	@Override
	public ListResourceTaggingProfileView getListResourceTaggingProfileView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaintainResourceTaggingProfileView getMaintainResourceTaggingProfileView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CalculateSecurityRiskView getCalculateSecurityRiskView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ListAccountProvisioningView getListAccountProvisioningView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public DeprovisionAccountView getDeprovisionAccountView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public AccountProvisioningStatusView getAccountProvisioningStatusView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ListFinancialAccountsView getListFinancialAccountsView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ListRoleProvisioningView getListRoleProvisioningView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public RoleProvisioningStatusView getRoleProvisioningStatusView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaintainRoleProvisioningView getMaintainRoleProvisioningView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ListTransitGatewayView getListTransitGatewayView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ListTransitGatewayConnectionProfileView getListTransitGatewayConnectionProfileView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaintainTransitGatewayView getMaintainTransitGatewayView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaintainTransitGatewayConnectionProfileView getMaintainTransitGatewayConnectionProfileView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaintainCentralAdminRoleAssignmentsView getMaintainCentralAdminRoleAssignmentsView() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ManageSrdBehaviorView getManageSrdBehaviorView() {
		// TODO Auto-generated method stub
		return null;
	}
}
