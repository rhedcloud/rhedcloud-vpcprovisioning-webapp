package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.client.common.AwsAccountRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.common.SrdDetectorNameRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ManageSrdBehaviorView;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.FilterStatusPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertyPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopManageSrdBehavior extends ViewImplBase implements ManageSrdBehaviorView {
	Presenter presenter;
	UserAccountPojo userLoggedIn;
	Widget statusMessageSource;
	String filterBeingTyped="";
	List<AccountPojo> accounts = new java.util.ArrayList<AccountPojo>();
	List<String> activeDetectorNames = new java.util.ArrayList<String>();
	boolean allowChanges=false;
	private SrdDetectorNameRpcSuggestOracle detectorNameSuggestions;
	private AwsAccountRpcSuggestOracle accountSuggestions;
    PopupPanel selectionPopup = new PopupPanel(true);
    PopupPanel runningStatusPopup = new PopupPanel(true);
    ScrollPanel runningStatusSP = new ScrollPanel();
    VerticalPanel runningStatusVP = new VerticalPanel();

	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField HTML introBodyHTML;
	@UiField HTML pleaseWaitHTML;
	@UiField PushButton refreshButton;
	@UiField Button clearFilterButton;
	@UiField TextBox filterTB;
	@UiField FlexTable accountsTable;
	@UiField FlexTable arnExemptionsTable;
	@UiField Button continueTopButton;
	@UiField Button continueBottomButton;
	@UiField HTML readOnlyHTML;
	@UiField TabLayoutPanel exemptionsTabPanel;
	@UiField Button initAccountPropertiesButton;
	@UiField HTML noResultsHTML;

	private static DesktopManageSrdBehaviorUiBinder uiBinder = GWT.create(DesktopManageSrdBehaviorUiBinder.class);

	interface DesktopManageSrdBehaviorUiBinder extends UiBinder<Widget, DesktopManageSrdBehavior> {
	}

	public DesktopManageSrdBehavior() {
		initWidget(uiBinder.createAndBindUi(this));
		setRefreshButtonImage(refreshButton);
	}

	@UiHandler("initAccountPropertiesButton")
	void initAccountPropertiesButtonClicked(ClickEvent e) {
		boolean confirmed = Window.confirm("Are you sure you want to "
			+ "initialize ALL accounts?  ** IMPORTANT *** MAKE SURE THE "
			+ "SRD SERVICE IS NOT RUNNING WHEN YOU DO THIS!!!");
		if (confirmed) {
			presenter.initializeSrdArnPropertiesForExistingAccounts();
		}
	}
	
	@UiHandler("exemptionsTabPanel")
	void tabSelection(SelectionEvent<Integer> e) {
		GWT.log("[UiHandler] selected tab: " + e.getSelectedItem());
		if (e.getSelectedItem() == 0) {
			presenter.start(presenter.getEventBus());
		}
		else {
			initializeSrdArnExemptionsPanel();
		}
	}
	@UiHandler("refreshButton")
	void refreshButtonClick(ClickEvent e) {
		presenter.start(presenter.getEventBus());
	}
	@UiHandler("clearFilterButton")
	void clearFilterButtonClick(ClickEvent e) {
		filterTB.setText("");
		filterBeingTyped="";
		presenter.filterByText(filterBeingTyped);
	}
	@UiHandler("filterTB")
	void filterTBBlur(BlurEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBMouseOver(MouseOverEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBKeyDown(KeyDownEvent e) {
		FilterStatusPojo status = checkFilterStatus(e, filterTB, filterBeingTyped);
		if (status.isValid() && status.isApplyFilter()) {
			filterBeingTyped = status.getFilteredText();
			GWT.log("filtering by: '" + status.getFilteredText());
			presenter.filterByText(status.getFilteredText());
		}
	}
	@UiHandler("continueTopButton")
	void continueTopButtonClicke(ClickEvent e) {
		ActionEvent.fire(presenter.getEventBus(), ActionNames.GO_HOME_CENTRAL_ADMIN, userLoggedIn);
	}
	@UiHandler("continueBottomButton")
	void continueBottomButtonClicke(ClickEvent e) {
		ActionEvent.fire(presenter.getEventBus(), ActionNames.GO_HOME_CENTRAL_ADMIN, userLoggedIn);
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	filterTB.setFocus(true);
	        }
	    });
	}

	@Override
	public Widget getStatusMessageSource() {
		return statusMessageSource;
	}

	@Override
	public void applyNetworkAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyCentralAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyAWSAccountAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetFieldStyles() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpPromptCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpConfirmOkay() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpConfirmCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableButtons() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableButtons() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initPage() {
		exemptionsTabPanel.selectTab(0);
		accountSuggestions = new AwsAccountRpcSuggestOracle(userLoggedIn, "account");
		initializeSrdArnExemptionsPanel();
		filterBeingTyped="";
		filterTB.setText("");
		filterTB.getElement().setPropertyString("placeholder", "enter filter text here (account id, name, owner)");
	}

	@Override
	public void setAllowChanges(boolean allow) {
		this.allowChanges = allow;
		if (!allow) {
			readOnlyHTML.setVisible(true);
		}
		else {
			readOnlyHTML.setVisible(false);
		}
	}
	@Override
	public boolean isAllowChanges() {
		return this.allowChanges;
	}
	
	@Override
	public void setAccounts(List<AccountPojo> accounts) {
		this.accounts = accounts;
		this.initializeAccountsPanel();
	}

	@Override
	public void setActiveDetectorNames(List<String> detectorNames) {
		this.activeDetectorNames = detectorNames;
		detectorNameSuggestions = new SrdDetectorNameRpcSuggestOracle(this.activeDetectorNames);
	}
	
	private void initializeSrdArnExemptionsPanel() {
		// SRD (sb and pushbutton)
		// ARN (ta)
		arnExemptionsTable.removeAllRows();

		// SRD name
		VerticalPanel srdVP = new VerticalPanel();
		srdVP.setStyleName("myFlexTable-grid");
		arnExemptionsTable.setWidget(0, 0, srdVP);
		arnExemptionsTable.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");
		
		Grid srdGrid = new Grid(1, 1);
		srdGrid.setCellSpacing(8);
		srdVP.add(srdGrid);
		
		final FlexTable whitelistedSrdArnTable = new FlexTable();
		whitelistedSrdArnTable.setCellSpacing(8);
		srdVP.add(whitelistedSrdArnTable);
				
		final SuggestBox detectorNameSearchSB = new SuggestBox(detectorNameSuggestions, new TextBox());
		detectorNameSearchSB.getElement().setPropertyString("placeholder", "<enter SRD name>");
		detectorNameSearchSB.setWidth("275px");
		detectorNameSearchSB.setTitle("Start typing the name of an SRD and select the one you'd like to ignore from the list.");
		if (userLoggedIn.isCentralAdminManager()) {
			detectorNameSearchSB.setEnabled(true);
		}
		else {
			detectorNameSearchSB.setEnabled(false);
		}
		srdGrid.setWidget(0, 0, detectorNameSearchSB);
		srdGrid.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");
		detectorNameSearchSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				final int rowNumber = whitelistedSrdArnTable.getRowCount() + 1;
				final String detectorName = detectorNameSearchSB.getText();
				GWT.log("selected detector name is: " + detectorName);
				if (detectorName != null && detectorName.length() > 0) {
					// TODO: add another row to the whitelistedSrdArnTable table
					// for this SRD
					final TextBox srdTB = new TextBox();
					srdTB.setEnabled(false);
					srdTB.setWidth("275px");
					srdTB.setValue(detectorName);
					srdTB.setTitle("This SRD will be ignored (not executed) by the SRD service for this account.");
					
					whitelistedSrdArnTable.setWidget(rowNumber, 0, srdTB);
					whitelistedSrdArnTable.getCellFormatter().setStyleName(rowNumber, 0, "myFlexTable-grid");
					
					final TextArea arnTA = new TextArea();
					arnTA.setWidth("500px");
					arnTA.setTitle("This ARN will be ignored by this/these SRD(s) for "
						+ "one or more accounts.");
					arnTA.setText("");
					arnTA.getElement().setPropertyString("placeholder", "<enter comma "
						+ "separated list of arns here>");
					
					whitelistedSrdArnTable.setWidget(rowNumber, 1, arnTA);
					whitelistedSrdArnTable.getCellFormatter().setStyleName(rowNumber, 1, "myFlexTable-grid");
					
					Image deleteImage = new Image("images/delete_icon.png");
					deleteImage.setWidth("20px");
					deleteImage.setHeight("20px");
					
					final PushButton removeSrdButton = new PushButton();
					removeSrdButton.setTitle("Remove this SRD from the exempt list.");
					removeSrdButton.getUpFace().setImage(deleteImage);
					// disable buttons if userLoggedIn is NOT a central admin
					if (userLoggedIn.isCentralAdminManager()) {
						removeSrdButton.setEnabled(true);
					}
					else {
						removeSrdButton.setEnabled(false);
					}
					removeSrdButton.addStyleName("glowing-border");
					removeSrdButton.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							// remove arn exemption from account and refresh
							if (rowNumber >= whitelistedSrdArnTable.getRowCount()) {
								whitelistedSrdArnTable.removeRow(1);
							}
							else {
								whitelistedSrdArnTable.removeRow(rowNumber);
							}
						}
					});
					whitelistedSrdArnTable.setWidget(rowNumber, 2, removeSrdButton);
					whitelistedSrdArnTable.getCellFormatter().
						setAlignment(rowNumber, 2, 
						HasHorizontalAlignment.ALIGN_CENTER, 
						HasVerticalAlignment.ALIGN_MIDDLE);
					whitelistedSrdArnTable.getCellFormatter().setStyleName(rowNumber, 2, "myFlexTable-grid");
					
					Button saveButton = new Button("Save Exemption");
					saveButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					saveButton.addStyleName("normalButton");
					saveButton.setWidth("150px");
					saveButton.addStyleName("glowing-border");
					saveButton.setTitle("Save the 'exempt list' for the SRDs and ARNs entered.");
					saveButton.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							String arnText = arnTA.getText();
							if (arnText == null || arnText.length() == 0) {
								showMessageToUser("Please enter a comma separated list of ARNs");
								arnTA.setFocus(true);
								return;
							}
							// parse the list of ARNs and create 
							// appropriate account properties then, save the 
							// account(s) that are impacted based on the ARN
							// should probably do that on the server side.
							presenter.parseAndApplyArnExemptionsForSrd(arnText, detectorName);
							whitelistedSrdArnTable.removeRow(rowNumber);
						}
					});
					whitelistedSrdArnTable.setWidget(rowNumber, 3, saveButton);
					whitelistedSrdArnTable.getCellFormatter().
						setAlignment(rowNumber, 3, 
						HasHorizontalAlignment.ALIGN_CENTER, 
						HasVerticalAlignment.ALIGN_MIDDLE);
					whitelistedSrdArnTable.getCellFormatter().setStyleName(rowNumber, 3, "myFlexTable-grid");
					
					detectorNameSearchSB.setText("");
					arnTA.setFocus(true);
				}
				else {
					
				}
			}
		});
	}
	
	private void initializeAccountsPanel() {
		// account id
		// account name
		// bad speed type (with meta data)
		// new speed type (validated, with meta data)
		//new CellTable<AccountSpeedChartPojo>(15, (CellTable.Resources)GWT.create(MyCellTableResources.class));
		
		accountsTable.removeAllRows();
		
		// add headers
		HTML acctIdHeader = new HTML("<h3>Account ID</h3>");
		HTML acctNameHeader = new HTML("<h3>Account Name</h3>");
		HTML acctAltNameHeader = new HTML("<h3>Alternate Name</h3>");
		HTML acctOwnerHeader = new HTML("<h3>Account Owner</h3>");
		HTML srdExemptHeader = new HTML("<h3>SRD Exempt</h3>");
		
		Grid srdWhitelistPanelHeader = new Grid(1, 2);
		srdWhitelistPanelHeader.setCellSpacing(8);
		srdWhitelistPanelHeader.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");
		srdWhitelistPanelHeader.getCellFormatter().setStyleName(0, 1, "myFlexTable-grid");
		
		HTML srdWhitelistHeader = new HTML("<h3>SRD Exemption List</h3>");

		Image clearSrdExemptionsImage = new Image("images/delete_icon.png");
		clearSrdExemptionsImage.setWidth("20px");
		clearSrdExemptionsImage.setHeight("20px");
		
		final PushButton clearSrdExemptionsButton = new PushButton();
		clearSrdExemptionsButton.setTitle("Remove ALL SRD Exemptions from ALL accounts.");
		clearSrdExemptionsButton.getUpFace().setImage(clearSrdExemptionsImage);
		clearSrdExemptionsButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		clearSrdExemptionsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean confirmed = Window.confirm("Are you sure you want to "
						+ "clear ALL SRD exemptions from ALL Accounts?");
				if (confirmed) {
					presenter.clearSrdExemptionsForAllAccounts();
				}
			}
		});
		srdWhitelistPanelHeader.setWidget(0, 0, srdWhitelistHeader);
		srdWhitelistPanelHeader.setWidget(0, 1, clearSrdExemptionsButton);
		srdWhitelistPanelHeader.getCellFormatter().setAlignment(0, 1, 
				HasHorizontalAlignment.ALIGN_CENTER, 
				HasVerticalAlignment.ALIGN_MIDDLE);

		
		Grid arnWhitelistPanelHeader = new Grid(1, 2);
		arnWhitelistPanelHeader.setCellSpacing(8);
		arnWhitelistPanelHeader.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");
		arnWhitelistPanelHeader.getCellFormatter().setStyleName(0, 1, "myFlexTable-grid");
		
		HTML arnWhitelistHeader = new HTML("<h3>ARN Exemption List</h3>");

		Image clearArnExemptionsImage = new Image("images/delete_icon.png");
		clearArnExemptionsImage.setWidth("20px");
		clearArnExemptionsImage.setHeight("20px");
		
		final PushButton clearArnExemptionsButton = new PushButton();
		clearArnExemptionsButton.setTitle("Remove ALL ARN Exemptions from ALL accounts.");
		clearArnExemptionsButton.getUpFace().setImage(clearArnExemptionsImage);
		clearArnExemptionsButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		clearArnExemptionsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean confirmed = Window.confirm("Are you sure you want to "
						+ "clear ALL SRD/ARN exemptions from ALL Accounts?");
				if (confirmed) {
					presenter.clearSrdArnExemptionsForAllAccounts();
				}
			}
		});
		arnWhitelistPanelHeader.setWidget(0, 0, arnWhitelistHeader);
		arnWhitelistPanelHeader.setWidget(0, 1, clearArnExemptionsButton);
		arnWhitelistPanelHeader.getCellFormatter().setAlignment(0, 1, 
				HasHorizontalAlignment.ALIGN_CENTER, 
				HasVerticalAlignment.ALIGN_MIDDLE);
		
		if (this.userLoggedIn.isCentralAdminManager()) {
			clearArnExemptionsButton.setEnabled(true);
			clearSrdExemptionsButton.setEnabled(true);
		}
		else {
			clearArnExemptionsButton.setEnabled(false);
			clearSrdExemptionsButton.setEnabled(false);
		}
		
		accountsTable.getRowFormatter().addStyleName(0, "myFlexTable-header");
		accountsTable.setWidget(0, 0, acctIdHeader);
		accountsTable.setWidget(0, 1, acctNameHeader);
		accountsTable.setWidget(0, 2, acctAltNameHeader);
		accountsTable.setWidget(0, 3, acctOwnerHeader);
		accountsTable.setWidget(0, 4, srdExemptHeader);
		accountsTable.setWidget(0, 5, srdWhitelistPanelHeader);
		accountsTable.getCellFormatter().setAlignment(0, 5, 
				HasHorizontalAlignment.ALIGN_CENTER, 
				HasVerticalAlignment.ALIGN_MIDDLE);
		accountsTable.setWidget(0, 6, arnWhitelistPanelHeader);
		accountsTable.getCellFormatter().setAlignment(0, 6, 
				HasHorizontalAlignment.ALIGN_CENTER, 
				HasVerticalAlignment.ALIGN_MIDDLE);

		for (AccountPojo pojo : this.accounts) {
			addAccountToPanel(pojo);
		}
	}

	private void addAccountToPanel(final AccountPojo account) {
		final int numRows = accountsTable.getRowCount();
		
		final HTML acctIdLabel = new HTML(account.getAccountId());
		final Label acctNameLabel = new Label(account.getAccountName());
		final Label altAcctNameLabel = new Label(account.getAlternateName());
		final Label acctOwnerLabel = 
			new Label(account.getAccountOwnerDirectoryMetaData().getFirstName() + 
			" " + account.getAccountOwnerDirectoryMetaData().getLastName());
		
		// - srdExempt column
		CheckBox srdExemptCB = new CheckBox();
		srdExemptCB.setValue(account.isSrdExempt());
		srdExemptCB.setTitle("If checked, the entire account will be ignored by the SRD service.");
		srdExemptCB.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				account.setSrdExempt(event.getValue(), userLoggedIn);
			}
		});
		
		// - SRD Whitelist (grid with the suggest box and an add button to add new ones
		// followed by text boxes and delete buttons for existing ones
		VerticalPanel srdWhitelistVP = new VerticalPanel();

		Grid srdWhitelistGrid = new Grid(1, 1);
		srdWhitelistGrid.setCellSpacing(8);
		srdWhitelistVP.add(srdWhitelistGrid);
		
		final FlexTable whitelistedSrdsTable = new FlexTable();
		whitelistedSrdsTable.setCellSpacing(8);
		initAccountWhitelistedSrdsTable(whitelistedSrdsTable, account);
		srdWhitelistVP.add(whitelistedSrdsTable);
				
		final SuggestBox detectorNameSearchSB = new SuggestBox(detectorNameSuggestions, new TextBox());
		detectorNameSearchSB.getElement().setPropertyString("placeholder", "<enter SRD name>");
		detectorNameSearchSB.setWidth("275px");
		detectorNameSearchSB.setTitle("Start typing the name of an SRD and select the one you'd like to ignore from the list.");
		srdWhitelistGrid.setWidget(0, 0, detectorNameSearchSB);
		srdWhitelistGrid.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");
		detectorNameSearchSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				String detectorName = detectorNameSearchSB.getText();
				GWT.log("selected detector name is: " + detectorName);
				if (detectorName != null && detectorName.length() > 0) {
					account.addWhitelistedSrd(detectorName, userLoggedIn);
					initAccountWhitelistedSrdsTable(whitelistedSrdsTable, account);
					detectorNameSearchSB.setText("");
				}
			}
		});

		// - Existing account level ARN Whitelists
		VerticalPanel arnWhitelistVP = new VerticalPanel();
		
		final FlexTable whitelistedArnsTable = new FlexTable();
		whitelistedArnsTable.setCellSpacing(8);
		initAccountWhitelistedArnsTable(whitelistedArnsTable, account);
		arnWhitelistVP.add(whitelistedArnsTable);

		// - save and apply buttons
		VerticalPanel buttonVP = new VerticalPanel();
		buttonVP.setSpacing(8);
		
		Button saveAccountButton = new Button("Save Account");
		saveAccountButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		saveAccountButton.addStyleName("normalButton");
		saveAccountButton.setWidth("150px");
		saveAccountButton.addStyleName("glowing-border");
		saveAccountButton.setTitle("Save the 'exempt list' for this account.");
		saveAccountButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.saveAccount(account, false, true);
			}
		});
		
		Button applyToAccountsButton = new Button("Apply To...");
		applyToAccountsButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		applyToAccountsButton.addStyleName("normalButton");
		applyToAccountsButton.setWidth("150px");
		applyToAccountsButton.addStyleName("glowing-border");
		applyToAccountsButton.setTitle("Apply this account's 'exempt list' to "
			+ "one or more other accounts.");
		applyToAccountsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.saveAccount(account, true, true);
			}
		});

		buttonVP.add(saveAccountButton);
		buttonVP.add(applyToAccountsButton);

		if (this.userLoggedIn.isCentralAdminManager()) {
			saveAccountButton.setEnabled(true);
			applyToAccountsButton.setEnabled(true);
			srdExemptCB.setEnabled(true);
			detectorNameSearchSB.setEnabled(true);
		}
		else {
			saveAccountButton.setEnabled(false);
			applyToAccountsButton.setEnabled(false);
			srdExemptCB.setEnabled(false);
			detectorNameSearchSB.setEnabled(false);
		}
		
	    accountsTable.setWidget(numRows, 0, acctIdLabel);
	    accountsTable.setWidget(numRows, 1, acctNameLabel);
	    accountsTable.setWidget(numRows, 2, altAcctNameLabel);
	    accountsTable.setWidget(numRows, 3, acctOwnerLabel);
	    accountsTable.setWidget(numRows, 4, srdExemptCB);
	    accountsTable.getCellFormatter().setAlignment(numRows, 4, 
	    		HasHorizontalAlignment.ALIGN_CENTER, 
	    		HasVerticalAlignment.ALIGN_MIDDLE);
	    accountsTable.setWidget(numRows, 5, srdWhitelistVP);
	    accountsTable.setWidget(numRows, 6, arnWhitelistVP);
	    accountsTable.setWidget(numRows, 7, buttonVP);
	    
	    if (isEven(numRows)) {
			accountsTable.getRowFormatter().addStyleName(numRows, "myFlexTable-evenRow");
	    }
	    else {
			accountsTable.getRowFormatter().addStyleName(numRows, "myFlexTable-oddRow");
	    }
	}
	
	private void initAccountWhitelistedArnsTable(final FlexTable whitelistedArnsTable, final AccountPojo account) {
		whitelistedArnsTable.removeAllRows();
		
		List<PropertyPojo> whitelistedArns = account.getWhitelistedArns();
		
		for (int i=0; i<whitelistedArns.size(); i++) {
			PropertyPojo prop = whitelistedArns.get(i);
			final TextArea arnTA = new TextArea();
			arnTA.setEnabled(false);
			arnTA.setWidth("275px");
			arnTA.setValue(prop.getName());
			arnTA.setTitle("This ARN will be ignored by the SRD service for this account.");
			
			whitelistedArnsTable.setWidget(i, 0, arnTA);
			whitelistedArnsTable.getCellFormatter().setStyleName(i, 0, "myFlexTable-grid");
			
			Image deleteImage = new Image("images/delete_icon.png");
			deleteImage.setWidth("20px");
			deleteImage.setHeight("20px");
			
			final PushButton removeArnButton = new PushButton();
			removeArnButton.setTitle("Remove this ARN from the exempt list.");
			removeArnButton.getUpFace().setImage(deleteImage);
			// disable buttons if userLoggedIn is NOT a central admin
			if (this.userLoggedIn.isCentralAdminManager()) {
				removeArnButton.setEnabled(true);
			}
			else {
				removeArnButton.setEnabled(false);
			}
			removeArnButton.addStyleName("glowing-border");
			removeArnButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					// remove srd exemption from account and refresh
					account.removeWhitelistedArn(arnTA.getText());
					initAccountWhitelistedArnsTable(whitelistedArnsTable, account);
				}
			});
			whitelistedArnsTable.setWidget(i, 1, removeArnButton);
			whitelistedArnsTable.getCellFormatter().setStyleName(i, 1, "myFlexTable-grid");
		}
	}

	private void initAccountWhitelistedSrdsTable(final FlexTable whitelistedSrdsTable, final AccountPojo account) {
		whitelistedSrdsTable.removeAllRows();

		List<PropertyPojo> whitelistedSrds = account.getWhitelistedSrds();
		
		for (int i=0; i<whitelistedSrds.size(); i++) {
			PropertyPojo prop = whitelistedSrds.get(i);
			final TextBox srdTB = new TextBox();
			srdTB.setEnabled(false);
			srdTB.setWidth("275px");
			String detectorName = prop.getName().substring(4);
			srdTB.setValue(detectorName);
			srdTB.setTitle("This SRD will be ignored (not executed) by the SRD service for this account.");
			
			whitelistedSrdsTable.setWidget(i, 0, srdTB);
			whitelistedSrdsTable.getCellFormatter().setStyleName(i, 0, "myFlexTable-grid");
			
			Image deleteImage = new Image("images/delete_icon.png");
			deleteImage.setWidth("20px");
			deleteImage.setHeight("20px");
			
			final PushButton removeSrdButton = new PushButton();
			removeSrdButton.setTitle("Remove this SRD from the exempt list.");
			removeSrdButton.getUpFace().setImage(deleteImage);
			// disable buttons if userLoggedIn is NOT a central admin
			if (this.userLoggedIn.isCentralAdminManager()) {
				removeSrdButton.setEnabled(true);
			}
			else {
				removeSrdButton.setEnabled(false);
			}
			removeSrdButton.addStyleName("glowing-border");
			removeSrdButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					// remove arn exemption from account and refresh
					account.removeWhitelistedSrd(srdTB.getText());
					initAccountWhitelistedSrdsTable(whitelistedSrdsTable, account);
				}
			});
			whitelistedSrdsTable.setWidget(i, 1, removeSrdButton);
			whitelistedSrdsTable.getCellFormatter().setStyleName(i, 1, "myFlexTable-grid");
		}
	}

	@Override
	public void applyAccountWhitelistsToOtherAccounts(final AccountPojo sourceAccount) {
		selectionPopup.clear();
		selectionPopup.setAutoHideEnabled(true);
		selectionPopup.setAnimationEnabled(true);
		selectionPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
		
		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(8);
		selectionPopup.add(vp);
		
		HTML heading = new HTML("<h2>Apply Exempt List to other Accounts</h2>");
		vp.add(heading);
		
		Grid grid = new Grid(1, 2);
		grid.setCellSpacing(8);
		vp.add(grid);

		VerticalPanel whitelistVP = new VerticalPanel();
		grid.setWidget(0, 0, whitelistVP);
		
		// srd exempt flag
		CheckBox cb = new CheckBox("SRD Exempt");
		cb.setValue(sourceAccount.isSrdExempt());
		cb.setEnabled(false);
		whitelistVP.add(cb);

		// srd whitelist
		Grid srdHeaderGrid = new Grid(1, 2);
		srdHeaderGrid.setCellSpacing(8);
		whitelistVP.add(srdHeaderGrid);
		
		HTML srdHTML = new HTML("<h3>SRD Exempt List</h3>");
		srdHeaderGrid.setWidget(0, 0, srdHTML);
		
		final CheckBox applySrdsCB = new CheckBox();
		applySrdsCB.setValue(true);
		applySrdsCB.setTitle("Uncheck if you DO NOT want to exempt the "
			+ "selected SRDs in the target account(s)");
		srdHeaderGrid.setWidget(0, 1, applySrdsCB);
		
		List<PropertyPojo> whiteListedSrds = sourceAccount.getWhitelistedSrds();
		for (PropertyPojo prop : whiteListedSrds) {
			CheckBox srdCB = new CheckBox(prop.getName().substring(4));
			srdCB.setValue(true);
			srdCB.setEnabled(false);
			whitelistVP.add(srdCB);
		}
		
		// potential target accounts
		VerticalPanel accountListVP = new VerticalPanel();
		grid.setWidget(0, 1, accountListVP);
		
		HTML accountListHeaderHTML = new HTML("<h3>Select one or more target accounts:</h3>");
		accountListVP.add(accountListHeaderHTML);

		final ListBox accountsLB = new ListBox();
		accountListVP.add(accountsLB);
		accountsLB.setMultipleSelect(true);
		for (AccountPojo acct : this.accounts) {
			if (!acct.getAccountId().equalsIgnoreCase(sourceAccount.getAccountId())) {
				String displayText = acct.getAccountId() + ": " + 
					acct.getAccountName() + ": " + 
					acct.getAccountOwnerDirectoryMetaData().getFirstName() + " " + 
					acct.getAccountOwnerDirectoryMetaData().getLastName();
				
				accountsLB.addItem(displayText, acct.getAccountId());
			}
		}

		// apply/cancel buttons
		Grid buttonGrid = new Grid(1, 2);
		buttonGrid.setCellSpacing(8);
		vp.add(buttonGrid);
		vp.setCellHorizontalAlignment(buttonGrid, HasHorizontalAlignment.ALIGN_CENTER);
		
		Button applyButton = new Button("Apply");
		buttonGrid.setWidget(0, 0, applyButton);
		applyButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		applyButton.addStyleName("normalButton");
		applyButton.setWidth("150px");
		applyButton.addStyleName("glowing-border");
		applyButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean atLeastOneSelected=false;
				for (int i=0; i<accountsLB.getItemCount(); i++) {
					if (accountsLB.isItemSelected(i)) {
						atLeastOneSelected = true;
						String acctId = accountsLB.getValue(i);
						AccountPojo targetAcct = getAccountFromList(accounts, acctId);
						if (targetAcct != null) {
							GWT.log("got account " + acctId + " from our list "
								+ "of accounts.  Would apply exempt list here.");
							targetAcct.setSrdExempt(sourceAccount.isSrdExempt(), userLoggedIn);
							
							// TODO: do we want to remove existing whitelist first?
//							targetAcct.clearWhitelist();
							
							if (applySrdsCB.getValue()) {
								List<PropertyPojo> whitelistedSrds = sourceAccount.getWhitelistedSrds();
								for (PropertyPojo sourceWhitelistedSrd : whitelistedSrds) {
									targetAcct.addWhitelistedSrd(sourceWhitelistedSrd.getName(), userLoggedIn);
								}
							}

//							if (applyArnsCB.getValue()) {
//								List<PropertyPojo> whitelistedArns = sourceAccount.getWhitelistedArns();
//								for (PropertyPojo sourceWhitelistedArn : whitelistedArns) {
//									String newArn = sourceWhitelistedArn.getName().
//										replaceAll(sourceAccount.getAccountId(), 
//										targetAcct.getAccountId());
//									targetAcct.addWhitelistedArn(newArn);
//								}
//							}
						}
						presenter.saveAccount(targetAcct, false, true);
					}
				}
				if (!atLeastOneSelected) {
					showMessageToUser("Please select one or more accounts "
						+ "to apply this exempt list to.");
				}
				else {
					selectionPopup.hide();
				}
			}
		});
		
		Button cancelButton = new Button("Cancel");
		buttonGrid.setWidget(0, 1, cancelButton);
		cancelButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		cancelButton.addStyleName("normalButton");
		cancelButton.setWidth("150px");
		cancelButton.addStyleName("glowing-border");
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectionPopup.hide();
			}
		});
		
		selectionPopup.show();
		selectionPopup.center();

	}
	
	private AccountPojo getAccountFromList(List<AccountPojo> accountList, String acctId) {
		for (AccountPojo acct : accountList) {
			if (acct.getAccountId().equalsIgnoreCase(acctId)) {
				return acct;
			}
		}
		return null;
	}

	@Override
	public void selectAccountsToApplyArnExemption(final List<AccountPojo> accountList, final String detectorName, final String arn) {
	    final PopupPanel accountSelectionPopup = new PopupPanel(true);

	    accountSelectionPopup.setAutoHideEnabled(true);
	    accountSelectionPopup.setAnimationEnabled(true);
	    accountSelectionPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
		
		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(8);
		accountSelectionPopup.add(vp);
		
		HTML heading = new HTML("<h3>Apply ARN Exemption to one or more Accounts</h3>");
		vp.add(heading);
		
		HTML body = new HTML("The ARN you have specified does not "
			+ "include an account id or a wildcard (*).  Therefore, you must "
			+ "select one or more accounts that you'd like to apply this ARN "
			+ "exemption to.");
		body.setWidth("450px");
		vp.add(body);
		
		Grid grid = new Grid(1, 1);
		grid.setCellSpacing(8);
		vp.add(grid);

		// potential target accounts
		VerticalPanel accountListVP = new VerticalPanel();
		grid.setWidget(0, 0, accountListVP);
		
		HTML accountListHeaderHTML = new HTML("<h4>Select one or more account(s):</h4>");
		accountListVP.add(accountListHeaderHTML);

		final ListBox accountsLB = new ListBox();
		accountsLB.setHeight("450px");
		accountsLB.setWidth("450px");
		accountListVP.add(accountsLB);
		accountsLB.setMultipleSelect(true);
		for (AccountPojo acct : accountList) {
			String displayText = acct.getAccountId() + ": " + 
				acct.getAccountName() + ": " + 
				acct.getAccountOwnerDirectoryMetaData().getFirstName() + " " + 
				acct.getAccountOwnerDirectoryMetaData().getLastName();
			
			accountsLB.addItem(displayText, acct.getAccountId());
		}

		// apply/cancel buttons
		Grid buttonGrid = new Grid(1, 2);
		buttonGrid.setCellSpacing(8);
		vp.add(buttonGrid);
		vp.setCellHorizontalAlignment(buttonGrid, HasHorizontalAlignment.ALIGN_CENTER);
		
		Button applyButton = new Button("Apply");
		buttonGrid.setWidget(0, 0, applyButton);
		applyButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		applyButton.addStyleName("normalButton");
		applyButton.setWidth("150px");
		applyButton.addStyleName("glowing-border");
		applyButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean atLeastOneSelected=false;
				for (int i=0; i<accountsLB.getItemCount(); i++) {
					if (accountsLB.isItemSelected(i)) {
						atLeastOneSelected = true;
						String acctId = accountsLB.getValue(i);
						AccountPojo targetAcct = getAccountFromList(accountList, acctId);
						if (targetAcct != null) {
							GWT.log("got account " + acctId + " from our list "
								+ "of accounts.  Would apply exempt list here.");
							targetAcct.addWhitelistedArnForDetector(arn, detectorName, userLoggedIn);
						}
						presenter.saveAccount(targetAcct, false, false);
					}
				}
				if (!atLeastOneSelected) {
					showMessageToUser("Please select one or more accounts "
						+ "to apply this ARN exemption to.");
				}
				else {
					accountSelectionPopup.hide();
				}
			}
		});
		
		Button cancelButton = new Button("Cancel");
		buttonGrid.setWidget(0, 1, cancelButton);
		cancelButton.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		cancelButton.addStyleName("normalButton");
		cancelButton.setWidth("150px");
		cancelButton.addStyleName("glowing-border");
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hidePleaseWaitDialog();
				accountSelectionPopup.hide();
			}
		});
		
		accountSelectionPopup.show();
		accountSelectionPopup.center();
	}

	@Override
	public void showInitializeAccountSrdArdPropertiesButton() {
		initAccountPropertiesButton.setVisible(true);
	}

	@Override
	public void hideInitializeAccountSrdArdPropertiesButton() {
		initAccountPropertiesButton.setVisible(false);
	}

	@Override
	public void showOrUpdateRunningStatusPanel(String message) {
		if (runningStatusPopup.isShowing()) {
			// update the content
			runningStatusVP.add(new HTML(message));
		}
		else {
			// create the content for the first time
			runningStatusPopup.setWidth("650px");
			runningStatusPopup.setHeight("650px");
			runningStatusPopup.clear();
			runningStatusPopup.setAutoHideEnabled(true);
			runningStatusPopup.setAnimationEnabled(true);
			runningStatusPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
			runningStatusVP.add(new HTML("<h2>Running status...</h2>"));
			runningStatusVP.add(new HTML(message));
			runningStatusSP.setWidget(runningStatusVP);
			runningStatusPopup.setWidget(runningStatusSP);
			runningStatusPopup.center();
			runningStatusPopup.show();
		}
	}

	@Override
	public void hideRunningStatusPanel() {
	}

	@Override
	public void showNoResultsMessage() {
		noResultsHTML.setVisible(true);
	}

	@Override
	public void hideNoResultsMessage() {
		noResultsHTML.setVisible(false);
	}
}
