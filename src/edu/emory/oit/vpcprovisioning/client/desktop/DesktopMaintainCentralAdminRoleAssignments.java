package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.MaintainCentralAdminRoleAssignmentsView;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountRoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.FilterStatusPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountRolePojo;

public class DesktopMaintainCentralAdminRoleAssignments extends ViewImplBase implements MaintainCentralAdminRoleAssignmentsView {
	Presenter presenter;
	AccountRoleAssignmentSummaryPojo acctRoleAssignmentSummary=null;
	UserAccountPojo userLoggedIn;
	UserAccountPojo userBeingMaintained;
	List<String> filterTypeItems;
	Widget statusMessageSource;
	String filterBeingTyped="";
	boolean allowChanges;

	@UiField VerticalPanel accountAssignmentsListPanel;
	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField HTML introBodyHTML;
	@UiField HTML pleaseWaitHTML;
	@UiField PushButton refreshButton;
	@UiField Button clearFilterButton;
	@UiField TextBox filterTB;
	@UiField FlexTable accountRoleAssignmentsTable;
	@UiField Button saveTopButton;
	@UiField Button saveBottomButton;
	@UiField Button cancelTopButton;
	@UiField Button cancelBottomButton;
	@UiField HTML readOnlyHTML;
	@UiField HTML noResultsHTML;

	@UiHandler("refreshButton")
	void refreshButtonClick(ClickEvent e) {
		presenter.start(presenter.getEventBus());
	}
	@UiHandler("clearFilterButton")
	void clearFilterButtonClick(ClickEvent e) {
		filterTB.setText("");
		filterBeingTyped="";
		presenter.filterByText(filterBeingTyped);
	}
	@UiHandler("filterTB")
	void filterTBBlur(BlurEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBMouseOver(MouseOverEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBKeyDown(KeyDownEvent e) {
		FilterStatusPojo status = checkFilterStatus(e, filterTB, filterBeingTyped);
		if (status.isValid() && status.isApplyFilter()) {
			filterBeingTyped = status.getFilteredText();
			GWT.log("filtering by: '" + status.getFilteredText());
			presenter.filterByText(status.getFilteredText());
		}
	}
	@UiHandler("cancelTopButton")
	void cancelTopButtonClicke(ClickEvent e) {
		ActionEvent.fire(presenter.getEventBus(), ActionNames.GO_HOME_CENTRAL_ADMIN, userLoggedIn);
	}
	@UiHandler("cancelBottomButton")
	void cancelBottomButtonClicke(ClickEvent e) {
		ActionEvent.fire(presenter.getEventBus(), ActionNames.GO_HOME_CENTRAL_ADMIN, userLoggedIn);
	}

	@UiHandler("saveTopButton")
	void saveTopButtonClicke(ClickEvent e) {
		presenter.saveCachedChanges();
	}
	@UiHandler("saveBottomButton")
	void saveBottomButtonClicke(ClickEvent e) {
		presenter.saveCachedChanges();
	}

	private static DesktopMaintainCentralAdminRoleAssignmentsUiBinder uiBinder = GWT
			.create(DesktopMaintainCentralAdminRoleAssignmentsUiBinder.class);

	interface DesktopMaintainCentralAdminRoleAssignmentsUiBinder
			extends UiBinder<Widget, DesktopMaintainCentralAdminRoleAssignments> {
	}

	public DesktopMaintainCentralAdminRoleAssignments() {
		initWidget(uiBinder.createAndBindUi(this));
		setRefreshButtonImage(refreshButton);
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Widget getStatusMessageSource() {
		return statusMessageSource;
	}

	@Override
	public void applyNetworkAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyCentralAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyAWSAccountAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetFieldStyles() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpPromptCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpConfirmOkay() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vpcpConfirmCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableButtons() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableButtons() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initPage() {
		filterBeingTyped="";
		filterTB.setText("");
		filterTB.getElement().setPropertyString("placeholder", "enter filter text here (account id, name, owner)");
	}

	@Override
	public void setAccountRoleAssignmentSummary(AccountRoleAssignmentSummaryPojo summary) {
		this.acctRoleAssignmentSummary = summary;
		this.initializeAccountRoleAssignmentsPanel();
	}

	private void initializeAccountRoleAssignmentsPanel() {
		// account id
		// account name
		// bad speed type (with meta data)
		// new speed type (validated, with meta data)
		//new CellTable<AccountSpeedChartPojo>(15, (CellTable.Resources)GWT.create(MyCellTableResources.class));
		
		accountRoleAssignmentsTable.removeAllRows();
		
		// add headers
		HTML acctIdHeader = new HTML("<h3>Account ID</h3>");
		HTML acctNameHeader = new HTML("<h3>Account Name</h3>");
		HTML acctAltNameHeader = new HTML("<h3>Account Alternate Name</h3>");
		HTML acctOwnerHeader = new HTML("<h3>Account Owner</h3>");
		HTML acctRolesHeader = new HTML("<h3>Account Level Roles for " + 
				acctRoleAssignmentSummary.getUser().getFullName() + "</h3>");
		acctRolesHeader.setTitle("To assign yourself to an account level "
			+ "role, check the relevant checkbox.  To remove yourself "
			+ "from an account level role, un-check the relevant checkbox.");

		accountRoleAssignmentsTable.getRowFormatter().addStyleName(0, "myFlexTable-header");
		accountRoleAssignmentsTable.setWidget(0, 0, acctIdHeader);
		accountRoleAssignmentsTable.setWidget(0, 1, acctNameHeader);
		accountRoleAssignmentsTable.setWidget(0, 2, acctAltNameHeader);
		accountRoleAssignmentsTable.setWidget(0, 3, acctOwnerHeader);
		accountRoleAssignmentsTable.setWidget(0, 4, acctRolesHeader);

		for (AccountPojo pojo : this.acctRoleAssignmentSummary.getAccounts()) {
			addAccountRoleAssignmentToPanel(pojo, acctRoleAssignmentSummary.getUser());
		}
	}

	private void addAccountRoleAssignmentToPanel(final AccountPojo account, UserAccountPojo user) {
		final int numRows = accountRoleAssignmentsTable.getRowCount();
		
		final Label acctIdLabel = new Label(account.getAccountId());
		final Label acctNameLabel = new Label(account.getAccountName());
		final Label altAcctNameLabel = new Label(account.getAlternateName());
		final Label acctOwnerLabel = 
			new Label(account.getAccountOwnerDirectoryMetaData().getFirstName() + 
			" " + account.getAccountOwnerDirectoryMetaData().getLastName());
		
	    // potential roles this ca could be assigned to in this account 
		// and their statuses (checked or unchecked
	    VerticalPanel vp = new VerticalPanel();
		vp.setTitle("To assign yourself to an account level "
				+ "role, check the relevant checkbox.  To remove yourself "
				+ "from an account level role, un-check the relevant checkbox.");

		Grid g = new Grid(4, 1);
		g.setCellSpacing(8);
		vp.add(g);
		vp.setCellVerticalAlignment(g, HasVerticalAlignment.ALIGN_TOP);
		vp.setCellHorizontalAlignment(g, HasHorizontalAlignment.ALIGN_CENTER);

		// account admin
		CheckBox accountAdminCB = createRoleCheckBox(user, account, Constants.STATIC_TEXT_ADMINISTRATOR, Constants.ROLE_NAME_RHEDCLOUD_AWS_ADMIN);
		g.setWidget(0, 0, accountAdminCB);
		g.getCellFormatter().setStyleName(0, 0, "myFlexTable-grid");

		// account auditor
		CheckBox accountAuditorCB = createRoleCheckBox(user, account, Constants.STATIC_TEXT_AUDITOR, Constants.ROLE_NAME_RHEDCLOUD_AUDITOR);
		g.setWidget(1, 0, accountAuditorCB);
		g.getCellFormatter().setStyleName(1, 0, "myFlexTable-grid");
	    
		// account central admin
		CheckBox accountCentralAdminCB = createRoleCheckBox(user, account, Constants.STATIC_TEXT_ACCOUNT_CENTRAL_ADMIN, Constants.ROLE_NAME_RHEDCLOUD_AWS_CENTRAL_ADMIN);
		g.setWidget(2, 0, accountCentralAdminCB);
		g.getCellFormatter().setStyleName(2, 0, "myFlexTable-grid");
	    
		// account incident response
		CheckBox accountIncidentResponseCB = createRoleCheckBox(user, account, Constants.STATIC_TEXT_INCIDENT_RESPONSE, Constants.ROLE_NAME_RHEDCLOUD_AWS_INCIDENT_RESPONSE);
		g.setWidget(3, 0, accountIncidentResponseCB);
		g.getCellFormatter().setStyleName(3, 0, "myFlexTable-grid");
	    
	    accountRoleAssignmentsTable.setWidget(numRows, 0, acctIdLabel);
	    accountRoleAssignmentsTable.setWidget(numRows, 1, acctNameLabel);
	    accountRoleAssignmentsTable.setWidget(numRows, 2, altAcctNameLabel);
	    accountRoleAssignmentsTable.setWidget(numRows, 3, acctOwnerLabel);
	    accountRoleAssignmentsTable.setWidget(numRows, 4, vp);
	    
	    if (isEven(numRows)) {
	    	accountRoleAssignmentsTable.getRowFormatter().addStyleName(numRows, "myFlexTable-evenRow");
	    }
	    else {
	    	accountRoleAssignmentsTable.getRowFormatter().addStyleName(numRows, "myFlexTable-oddRow");
	    }
	}
	
	private CheckBox createRoleCheckBox(final UserAccountPojo user, final AccountPojo account, final String rolePrettyName, final String roleName) {
		final CheckBox roleCheckBox = new CheckBox(rolePrettyName);
		if (user.isAssignedRoleInAccount(account.getAccountId(), roleName) ) {
			roleCheckBox.setValue(true);
		}
		roleCheckBox.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (roleCheckBox.getValue()) {
					// generate role assignment for account central admin
					// add an AccountRole to the user
					statusMessageSource = roleCheckBox;
					UserAccountRolePojo uar = new UserAccountRolePojo();
					uar.setUser(user);
					uar.setAccount(account);
					uar.setRoleName(roleName);
					presenter.cacheAccountRoleAssignment(uar);
				}
				else {
					// delete role assignment for account central admin
					// remove the AccountRole from the user
					statusMessageSource = roleCheckBox;
					UserAccountRolePojo uar = new UserAccountRolePojo();
					uar.setUser(user);
					uar.setAccount(account);
					uar.setRoleName(roleName);
					presenter.cacheAccountRoleUnassignment(uar);
				}
			}
		});
		roleCheckBox.setEnabled(isAllowChanges());
		return roleCheckBox;
	}
	@Override
	public void setAllowChanges(boolean allow) {
		this.allowChanges = allow;
		if (!allow) {
			readOnlyHTML.setVisible(true);
			saveTopButton.setEnabled(false);
			saveBottomButton.setEnabled(false);
		}
		else {
			readOnlyHTML.setVisible(false);
			saveTopButton.setEnabled(true);
			saveBottomButton.setEnabled(true);
		}
	}
	@Override
	public boolean isAllowChanges() {
		return this.allowChanges;
	}
	@Override
	public void setUserBeingMaintained(UserAccountPojo user) {
		this.userBeingMaintained = user;
	}
	@Override
	public void showNoResultsMessage() {
		noResultsHTML.setVisible(true);
	}

	@Override
	public void hideNoResultsMessage() {
		noResultsHTML.setVisible(false);
	}
}
