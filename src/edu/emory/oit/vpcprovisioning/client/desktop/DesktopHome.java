package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.common.ConsoleFeatureRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.common.ConsoleFeatureSuggestion;
import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonSuggestion;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.home.HomeView;
import edu.emory.oit.vpcprovisioning.shared.AccountRolePojo;
import edu.emory.oit.vpcprovisioning.shared.ConsoleFeaturePojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopHome extends ViewImplBase implements HomeView {
	Presenter presenter;
	UserAccountPojo userLoggedIn;
	List<AccountRolePojo> accountRoles;
	private final DirectoryPersonRpcSuggestOracle personSuggestions = new DirectoryPersonRpcSuggestOracle(Constants.SUGGESTION_TYPE_DIRECTORY_PERSON_NAME);
	private ConsoleFeatureRpcSuggestOracle consoleFeatureSuggestions = new ConsoleFeatureRpcSuggestOracle(userLoggedIn, Constants.SUGGESTION_TYPE_CONSOLE_FEATURE);

	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField HTML introBodyHTML;
	@UiField HTML emoryAwsInfoHTML;
	@UiField HTML directoryInfoHTML;
	@UiField HTML personInfoHTML;
	@UiField Button roleInfoButton;
	@UiField Button directoryInfoButton;
	@UiField Button personInfoButton;
	@UiField Element accountSeriesElem;
	@UiField HTML backgroundNoticeHTML;
	@UiField HTML speedTypeValidationNoticeHTML;
	@UiField HTML iccDataMigrationNoticeHTML;
	
	@UiField(provided=true) SuggestBox serviceSearchSB = new SuggestBox(consoleFeatureSuggestions, new TextBox());
	@UiField VerticalPanel recentlyUsedConsoleFeaturesPanel;
	@UiField VerticalPanel allConsoleFeaturesPanel;
	@UiField DisclosurePanel allConsoleFeaturesDP;

	@UiHandler ("roleInfoButton")
	void roleInfoButtonClicked(ClickEvent e) {
		HTML h = new HTML(presenter.getDetailedRoleInfoHTML());
		h.addStyleName("body");
		PopupPanel p = new PopupPanel();
	    p.setAutoHideEnabled(true);
	    p.setAnimationEnabled(true);
	    p.getElement().getStyle().setBackgroundColor("#f1f1f1");
		p.setWidget(h);
		p.showRelativeTo(roleInfoButton);
	}
	@UiHandler ("directoryInfoButton")
	void directoryInfoButtonClicked(ClickEvent e) {
		presenter.getDetailedDirectoryInfoHTML();
	}
	@UiHandler ("personInfoButton")
	void personInfoButtonClicked(ClickEvent e) {
		presenter.getDetailedPersonInfoHTML();
	}

	private static DesktopHomeUiBinder uiBinder = GWT.create(DesktopHomeUiBinder.class);

	interface DesktopHomeUiBinder extends UiBinder<Widget, DesktopHome> {
	}

	public DesktopHome() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}

	@Override
	public void applyAWSAccountAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		
		return null;
	}

	@Override
	public void resetFieldStyles() {
		
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		
		return null;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void initPage() {
		GWT.log("replacing FIRST_NAME with " + userLoggedIn.getPersonalName().getFirstName());
		String intro = introBodyHTML.
				getHTML().
				replace("FIRST_NAME", userLoggedIn.getPersonalName().getFirstName());
		introBodyHTML.setHTML(intro);
		allConsoleFeaturesDP.setOpen(false);
		serviceSearchSB.setText("");
		serviceSearchSB.getElement().setPropertyString("placeholder", "Example: Accounts, VPC, VPN etc.");
		serviceSearchSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				ConsoleFeatureSuggestion suggestion = (ConsoleFeatureSuggestion)event.getSelectedItem();
				if (suggestion.getService() != null) {
					presenter.saveConsoleFeatureInCacheForUser(suggestion.getService(), userLoggedIn);
					ActionEvent.fire(presenter.getEventBus(), suggestion.getService().getActionName());
				}
			}
		});
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setAccountRoleList(List<AccountRolePojo> accountRoles) {
		this.accountRoles = accountRoles;
//		refreshAccountRolePanel();
	}

//	private void refreshAccountRolePanel() {
//		// build a table with account role information in it
//		accountRolePanel.clear();
//		HTML html = new HTML("Here's a list of the accounts you're affiliated with and the "
//			+ "roles you hold for those accounts:");
//		html.addStyleName("body");
//		
//		accountRolePanel.add(html);
//		for (final AccountRolePojo ar : this.accountRoles) {
//			if (ar.getAccountId() != null) {
//				Anchor accountRoleAnchor = new Anchor(ar.getAccountId() + "-" + ar.getRoleName());
//				accountRoleAnchor.addStyleName("productAnchor");
//				accountRoleAnchor.setTitle("View/Maintain this account");
//				accountRoleAnchor.addClickHandler(new ClickHandler() {
//					@Override
//					public void onClick(ClickEvent event) {
//						// TODO fire an event to maintain the account indicated by the id associated to this account
//						presenter.viewAccountForId(ar.getAccountId());
//					}
//				});
//				accountRolePanel.add(accountRoleAnchor);
//			}
//		}
//	}

	@Override
	public void applyCentralAdminMask() {
		
		
	}
	@Override
	public void setRoleInfoHTML(String roleInfo) {
		emoryAwsInfoHTML.setHTML(roleInfo);
	}
	@Override
	public void setPersonInfoHTML(String personInfo) {
		personInfoHTML.setHTML(personInfo);
	}
	@Override
	public void setDirectoryInfoHTML(String directoryInfo) {
		directoryInfoHTML.setHTML(directoryInfo);
	}
	@Override
	public void showDirectoryPersonInfoPopup(String directoryPersonInfoHTML) {
		HTML h = new HTML(directoryPersonInfoHTML);
		h.addStyleName("body");
		PopupPanel p = new PopupPanel();
	    p.setAutoHideEnabled(true);
	    p.setAnimationEnabled(true);
	    p.getElement().getStyle().setBackgroundColor("#f1f1f1");
		p.setWidget(h);
		p.showRelativeTo(directoryInfoButton);
	}
	@Override
	public void showPersonSummaryPopup(String personSummaryHTML) {
		HTML h2 = new HTML("<b>Lookup Someone Else</b>");
		HTML h = new HTML(personSummaryHTML);
		h.addStyleName("body");
		final PopupPanel p = new PopupPanel();
		p.setWidth("750px");
	    p.setAutoHideEnabled(true);
	    p.setAnimationEnabled(true);
	    p.getElement().getStyle().setBackgroundColor("#f1f1f1");
	    VerticalPanel vp = new VerticalPanel();
	    vp.setSpacing(4);
		p.setWidget(vp);
		vp.add(h2);
		vp.add(h);
		Grid g = new Grid(2,2);
		vp.add(g);
		g.setWidget(0, 0, h2);
		final SuggestBox directoryLookupSB = new SuggestBox(personSuggestions, new TextBox());
		directoryLookupSB.addStyleName("field");
		directoryLookupSB.addStyleName("glowing-border");
		directoryLookupSB.getElement().setPropertyString("placeholder", "<enter name or netid>");
		directoryLookupSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					presenter.setDirectoryPerson(dp_suggestion.getDirectoryPerson());
					directoryLookupSB.setTitle(presenter.getDirectoryPerson().toString());
				}
			}
		});
		g.setWidget(1, 0, directoryLookupSB);
		Button lookupButton = new Button("Lookup");
		lookupButton.addStyleName("actionButton");
		lookupButton.addStyleName("glowing-border");
		lookupButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				p.hide();
				presenter.lookupPersonInfoHTML(presenter.getDirectoryPerson());
			}
		});
		g.setWidget(1, 1, lookupButton);
		p.showRelativeTo(personInfoButton);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	directoryLookupSB.setFocus(true);
	        }
	    });

	}
	@Override
	public void showPersonSummaryLookupPopup(String personInfoHTML) {
		HTML hl = new HTML("<b>Lookup Results</b>");
		HTML h = new HTML(personInfoHTML);
		h.addStyleName("body");
		PopupPanel p = new PopupPanel();
		p.setWidth("750px");
	    p.setAutoHideEnabled(true);
	    p.setAnimationEnabled(true);
	    p.getElement().getStyle().setBackgroundColor("#f1f1f1");
	    VerticalPanel vp = new VerticalPanel();
	    vp.setSpacing(8);
		p.setWidget(vp);
		vp.add(hl);
		vp.add(h);
		p.showRelativeTo(personInfoButton);
	}
	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}
	@Override
	public void vpcpPromptCancel() {
		
		
	}
	@Override
	public void vpcpConfirmOkay() {
		
		
	}
	@Override
	public void vpcpConfirmCancel() {
		
		
	}
	@Override
	public void setAccountSeriesInfo(String seriesInfo) {
		accountSeriesElem.setInnerHTML(seriesInfo);
	}
	@Override
	public void hideBackgroundWorkNotice() {
		backgroundNoticeHTML.setVisible(false);
	}
	@Override
	public void disableButtons() {
		roleInfoButton.setEnabled(false);
		directoryInfoButton.setEnabled(false);
		personInfoButton.setEnabled(false);
	}
	@Override
	public void enableButtons() {
		roleInfoButton.setEnabled(true);
		directoryInfoButton.setEnabled(true);
		personInfoButton.setEnabled(true);
	}
	@Override
	public void lockView() {
		roleInfoButton.setEnabled(false);
		directoryInfoButton.setEnabled(false);
		personInfoButton.setEnabled(false);
	}
	@Override
	public void applyNetworkAdminMask() {
		
		
	}
	@Override
	public void setConsoleFeatures(List<ConsoleFeaturePojo> features) {
		// create a grid for all services and their descriptions
		// Name (link)
		// Description
		
		int numRows = (features.size() / 3) + 1;
		Grid featuresGrid = new Grid(numRows, 3);
		allConsoleFeaturesPanel.clear();
		int rowCounter = 0;
		int columnCounter = 0;
		for (int i=0; i<features.size(); i++) {
			final ConsoleFeaturePojo service = features.get(i);
			Anchor serviceAnchor = new Anchor(service.getName());
			serviceAnchor.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			serviceAnchor.setTitle(service.getDescription() + " action:" + service.getActionName() + " isPopular=" + service.isPopular());
			serviceAnchor.ensureDebugId(service.getName() + "-allFeatures");
			serviceAnchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					presenter.saveConsoleFeatureInCacheForUser(service, userLoggedIn);
					getAppShell().clearBreadCrumbs();
					getAppShell().addBreadCrumb(service.getName(), service.getActionName(), null);
					ActionEvent.fire(presenter.getEventBus(), service.getActionName());
				}
			});
			Grid g = new Grid(2,1);
			g.setWidget(0, 0, serviceAnchor);
			g.setWidget(1, 0, new HTML("<i>" + service.getDescription() + "</i>"));
			featuresGrid.setWidget(rowCounter, columnCounter, g);
			featuresGrid.getCellFormatter().setWidth(rowCounter, columnCounter, "350px");
			if (columnCounter >= 2) {
				columnCounter = 0;
				rowCounter++;
			}
			else {
				columnCounter++;
			}
		}
		allConsoleFeaturesPanel.add(featuresGrid);
	}
	@Override
	public void setRecentlyUsedConsoleFeatures(List<ConsoleFeaturePojo> features) {
		int numRows = (features.size() / 3) + 1;
		Grid featuresGrid = new Grid(numRows, 3);
		recentlyUsedConsoleFeaturesPanel.clear();
		int rowCounter = 0;
		int columnCounter = 0;
		for (int i=0; i<features.size(); i++) {
			final ConsoleFeaturePojo service = features.get(i);
			Anchor serviceAnchor = new Anchor(service.getName());
			serviceAnchor.setTitle(service.getDescription() + " action:" + service.getActionName() + " isPopular=" + service.isPopular());
			serviceAnchor.ensureDebugId(service.getName() + "-recentFeatures");
			serviceAnchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					presenter.saveConsoleFeatureInCacheForUser(service, userLoggedIn);
					getAppShell().clearBreadCrumbs();
					getAppShell().addBreadCrumb(service.getName(), service.getActionName(), null);
					ActionEvent.fire(presenter.getEventBus(), service.getActionName());
				}
			});
			Grid g = new Grid(1,1);
			g.setWidget(0, 0, serviceAnchor);
			featuresGrid.setWidget(rowCounter, columnCounter, g);
			featuresGrid.getCellFormatter().setWidth(rowCounter, columnCounter, "250px");
			if (columnCounter >= 2) {
				columnCounter = 0;
				rowCounter++;
			}
			else {
				columnCounter++;
			}
		}
		if (features.size() == 0) {
			recentlyUsedConsoleFeaturesPanel.add(new HTML("No recently used features to diplay."));
		}
		else {
			recentlyUsedConsoleFeaturesPanel.add(featuresGrid);
		}
	}
	@Override
	public void hideSpeedTypeValidationNotice() {
		speedTypeValidationNoticeHTML.setVisible(false);
	}
	@Override
	public void hideIccDataMigrationNotice() {
		iccDataMigrationNoticeHTML.setVisible(false);
	}
	@Override
	public void showIccDataMigrationNotice() {
		iccDataMigrationNoticeHTML.setVisible(true);
	}
	@Override
	public void showIccDataMigrationResults(final boolean isMigration, final boolean isEcs, String results) {
		final PopupPanel popup = new PopupPanel(false, true);
		final HTML h = new HTML(results);

		VerticalPanel vp = new VerticalPanel();
		vp.setWidth("100%");
		vp.setHeight("100%");
		
		final ScrollPanel sp = new ScrollPanel();
		sp.setHeight("99%");
		sp.setWidth("99%");
		sp.setWidget(vp);
		
		HorizontalPanel hpTop = new HorizontalPanel();
		hpTop.setSpacing(8);
		
		// add a button
		Button topCloseButton = new Button("Close");
		hpTop.add(topCloseButton);
		topCloseButton.addStyleName("normalButton");
		topCloseButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				popup.hide();
			}
		});
		final Button topRefreshButton = new Button("Refresh");
		hpTop.add(topRefreshButton);
		topRefreshButton.addStyleName("normalButton");
		topRefreshButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				refreshMigrationAndSyncStatus(isMigration, isEcs, topRefreshButton, h);
				sp.scrollToBottom();
			}
		});
		
		vp.add(hpTop);
		
		// add html
		vp.add(h);

		HorizontalPanel hpBottom = new HorizontalPanel();
		hpBottom.setSpacing(8);
		Button bottomCloseButton = new Button("Close");
		hpBottom.add(bottomCloseButton);
		bottomCloseButton.addStyleName("normalButton");
		bottomCloseButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				popup.hide();
			}
		});

		final Button bottomRefreshButton = new Button("Refresh");
		hpBottom.add(bottomRefreshButton);
		bottomRefreshButton.addStyleName("normalButton");
		bottomRefreshButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				refreshMigrationAndSyncStatus(isMigration, isEcs, bottomRefreshButton, h);
				sp.scrollToBottom();
			}
		});
		vp.add(hpBottom);

		popup.setWidth("1200px");
		popup.setHeight("800px");
		popup.setWidget(sp);
		popup.addStyleDependentName(Constants.STYLE_INFO_POPUP_MESSAGE);
		popup.getElement().getStyle().setPadding(3.0, Unit.EM);
		popup.setAnimationEnabled(true);
		popup.setGlassEnabled(true);
        popup.show();
        popup.center();
	}
	
	public void refreshMigrationAndSyncStatus(boolean isMigration, boolean isEcs, final Button theButton, final HTML h) {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Exception performing IAM Identity Center data migration."; 
				GWT.log(msg, caught);
				showMessageToUser(msg + 
					"<p>Message from server is: " + caught.getMessage() + "</p>");
			}

			@Override
			public void onSuccess(final String result) {
			    h.setHTML(result);
			}
		};
		// show iic migration status 
		if (isEcs) {
			VpcProvisioningService.Util.getInstance().getEcsIicSynchronzationStatus(callback);
		}
		else {
			if (isMigration) {
				VpcProvisioningService.Util.getInstance().getIicDataMigrationStatus(callback);
			}
			else {
				VpcProvisioningService.Util.getInstance().getIicSychronizationStatus(callback);
			}
		}
	}
}
