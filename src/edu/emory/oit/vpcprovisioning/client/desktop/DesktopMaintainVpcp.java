package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonSuggestion;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.vpcp.MaintainVpcpView;
import edu.emory.oit.vpcprovisioning.shared.AWSRegionPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;
import edu.emory.oit.vpcprovisioning.shared.VpcRequisitionPojo;

public class DesktopMaintainVpcp  extends ViewImplBase implements MaintainVpcpView {
	Presenter presenter;
	boolean editing;
	boolean locked;
	List<String> complianceClassTypes;
	List<String> vpcTypes;
	List<AWSRegionPojo> regionTypes;
	List<AccountPojo> accounts;
	UserAccountPojo userLoggedIn;
	int netIdRowNum = 0;
	int netIdColumnNum = 0;
	int removeButtonColumnNum = 1;
	String speedTypeBeingTyped=null;
	boolean speedTypeConfirmed = false;
	private final DirectoryPersonRpcSuggestOracle personSuggestions = new DirectoryPersonRpcSuggestOracle(Constants.SUGGESTION_TYPE_DIRECTORY_PERSON_NAME);

	private static DesktopMaintainVpcpUiBinder uiBinder = GWT.create(DesktopMaintainVpcpUiBinder.class);

	interface DesktopMaintainVpcpUiBinder extends UiBinder<Widget, DesktopMaintainVpcp> {
	}

	@UiField
	Button okayButton;
	Button cancelButton;
	@UiField HorizontalPanel pleaseWaitPanel;

	// used for maintaining vpc (irrelevant for now i think)
	@UiField Grid maintainVpcpGrid;
	@UiField TextBox provisioningIdTB;
	@UiField ListBox vpcTypeLB;
	
	// used when generating vpc
	@UiField VerticalPanel generateVpcpPanel;
	@UiField(provided=true) SuggestBox requestorLookupSB = new SuggestBox(personSuggestions, new TextBox());
	@UiField(provided=true) SuggestBox ownerLookupSB = new SuggestBox(personSuggestions, new TextBox());
	@UiField TextBox vpcpReqSpeedTypeTB;
	@UiField ListBox vpcpReqTypeLB;
	@UiField ListBox vpcpReqComplianceClassLB;
	@UiField CheckBox vpcpReqNotifyAdminsCB;
	@UiField ListBox accountLB;
	@UiField HTML speedTypeHTML;
	@UiField TextArea vpcpReqPurposeTA;

	@UiField VerticalPanel adminVP;
	@UiField(provided=true) SuggestBox adminLookupSB = new SuggestBox(personSuggestions, new TextBox());
	@UiField Button addAdminButton;
	@UiField FlexTable adminTable;
	@UiField ListBox regionLB;
	@UiField CheckBox provisionWithVpcCB;
	@UiField VerticalPanel vpcpReqTypePanel;
	@UiField HorizontalPanel regionPanel;

	@UiHandler ("provisionWithVpcCB")
	void provisionWithVpcChanged(ClickEvent e) {
		if (provisionWithVpcCB.getValue()) {
			vpcpReqTypeLB.setVisible(true);
			regionPanel.setVisible(true);
			vpcpReqPurposeTA.setVisible(true);
			vpcpReqTypeLB.setEnabled(true);
		}
		else {
			vpcpReqTypeLB.setVisible(false);
			regionPanel.setVisible(false);
			vpcpReqPurposeTA.setVisible(false);
		}
	}
	
	@UiHandler ("vpcpReqSpeedTypeTB")
	void speedTypeMouseOver(MouseOverEvent e) {
		String acct = vpcpReqSpeedTypeTB.getText();
		presenter.setSpeedChartStatusForKeyOnWidget(acct, vpcpReqSpeedTypeTB, false);
	}
	
	@UiHandler ("vpcpReqSpeedTypeTB")
	void speedTypeBlur(BlurEvent e) {
		presenter.setSpeedChartStatusForKey(vpcpReqSpeedTypeTB.getText(), speedTypeHTML, true);
	}
	
	@UiHandler ("vpcpReqSpeedTypeTB")
	void speedTypeKeyDown(KeyDownEvent e) {
		this.setSpeedTypeConfirmed(false);
		int keyCode = e.getNativeKeyCode();
		char ccode = (char)keyCode;

		if (keyCode == KeyCodes.KEY_BACKSPACE) {
			if (speedTypeBeingTyped != null) {
				if (speedTypeBeingTyped.length() > 0) {
					speedTypeBeingTyped = speedTypeBeingTyped.substring(0, speedTypeBeingTyped.length() - 1);
				}
				presenter.setSpeedChartStatusForKey(speedTypeBeingTyped, speedTypeHTML, false);
				return;
			}
		}
		
		if (!isValidKey(keyCode, e.isAnyModifierKeyDown())) {
			GWT.log("[speedTypeKeyPressed] invalid key: " + keyCode);
			return;
		}
		else {
			if (speedTypeBeingTyped == null) {
				speedTypeBeingTyped = "";
			}
			speedTypeBeingTyped += String.valueOf(ccode);
		}

		presenter.setSpeedChartStatusForKey(speedTypeBeingTyped, speedTypeHTML, false);
	}

	@UiHandler ("adminLookupSB")
	void adminLookupSBKeyPressed(KeyPressEvent e) {
        int keyCode = e.getNativeEvent().getKeyCode();
        if (keyCode == KeyCodes.KEY_ENTER) {
    		presenter.addAdminDirectoryPersonToVpcp();
        }
	}
	@UiHandler ("addAdminButton")
	void addAdminButtonClick(ClickEvent e) {
		presenter.addAdminDirectoryPersonToVpcp();
	}
	@UiHandler ("cancelButton")
	void cancelButtonClicked(ClickEvent e) {
		GWT.log("VPCP generation canceled...");
		ActionEvent.fire(presenter.getEventBus(), ActionNames.VPCP_EDITING_CANCELED);
	}

	public DesktopMaintainVpcp() {
		initWidget(uiBinder.createAndBindUi(this));

		okayButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// populate vpcrequisition that will be used as seed data
				// for the vpcp.generate
				presenter.getVpcRequisition().setAuthenticatedRequestorUserId(presenter.getRequestorDirectoryPerson().getKey());
				presenter.getVpcRequisition().setAccountOwnerUserId(presenter.getOwnerDirectoryPerson().getKey());
				presenter.getVpcRequisition().setSpeedType(vpcpReqSpeedTypeTB.getText());
				presenter.getVpcRequisition().setComplianceClass(vpcpReqComplianceClassLB.getSelectedValue());
				presenter.getVpcRequisition().setNotifyAdmins(vpcpReqNotifyAdminsCB.getValue());
				presenter.getVpcRequisition().setAccountId(accountLB.getSelectedValue());
				if (vpcpReqTypeLB.getSelectedValue() == null || 
					vpcpReqTypeLB.getSelectedValue().length() == 0) {
					// they're not provisioning with a VPC
					presenter.getVpcRequisition().setType("0");
				}
				else {
					presenter.getVpcRequisition().setType(vpcpReqTypeLB.getSelectedValue());
				}
				presenter.getVpcRequisition().setPurpose(vpcpReqPurposeTA.getText());
				presenter.getVpcRequisition().setRegion(regionLB.getSelectedValue());
				// customer admin net id list is already maintained as they add/remove them
				
				presenter.saveVpcp();
				GWT.log("VPCP saved...");
			}
		}, ClickEvent.getType());
		
		accountLB.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				// if they select an account
				// check the provisionWithVpcCB, show vpcpReqTypePanel, 
				// and select a type1 vpc from the list (vpcTypeLB)
				// provisionWithVpcCB and vpcTypeLB should also be disabled
				// because they really can't change those
				if (accountLB.getSelectedValue() != null && 
					accountLB.getSelectedValue().length() > 0) {

					provisionWithVpcCB.setValue(true, true);
					vpcpReqTypeLB.setVisible(true);
					regionPanel.setVisible(true);
					vpcpReqPurposeTA.setVisible(true);
					provisionWithVpcCB.setEnabled(false);
					vpcpReqTypeLB.setSelectedIndex(1);
					vpcpReqTypeLB.setEnabled(false);
				}
				else {
					provisionWithVpcCB.setValue(false, true);
					provisionWithVpcCB.setEnabled(true);
					vpcpReqTypeLB.setVisible(false);
					vpcpReqTypeLB.setSelectedIndex(0);
					vpcpReqTypeLB.setEnabled(true);
					regionPanel.setVisible(false);
					vpcpReqPurposeTA.setVisible(false);
				}
			}
		});
	}

	private void registerHandlers() {
		adminLookupSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					presenter.setAdminDirectoryPerson(dp_suggestion.getDirectoryPerson());
					adminLookupSB.setTitle(presenter.getAdminDirectoryPerson().toString());
				}
			}
		});
		ownerLookupSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					presenter.setOwnerDirectoryPerson(dp_suggestion.getDirectoryPerson());
					ownerLookupSB.setTitle(presenter.getOwnerDirectoryPerson().toString());
				}
			}
		});
		requestorLookupSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					presenter.setRequestorDirectoryPerson(dp_suggestion.getDirectoryPerson());
					requestorLookupSB.setTitle(presenter.getOwnerDirectoryPerson().toString());
				}
			}
		});
	}

	@Override
	public void setInitialFocus() {
		if (editing) {
			Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
		        public void execute () {
		        	provisioningIdTB.setFocus(true);
		        }
		    });
		}
		else {
			Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
		        public void execute () {
		        	accountLB.setFocus(true);
		        }
		    });
		}
	}

	private void addAdminToPanel(final String userId, String title) {
		int numRows = adminTable.getRowCount();
		final Label netIdLabel = new Label(userId);
		netIdLabel.setTitle(title);
		netIdLabel.addStyleName("emailLabel");
		netIdLabel.addMouseOverHandler(new MouseOverHandler() {
			@Override
			public void onMouseOver(MouseOverEvent event) {
				presenter.setDirectoryMetaDataTitleOnWidget(userId, netIdLabel);
			}
		});
		final Button removeNetIdButton = new Button("Remove");
		// disable remove button if userLoggedIn is NOT an admin		
		if (userLoggedIn.isAdminForAccount(presenter.getVpcRequisition().getAccountId()) || 
			userLoggedIn.isCentralAdmin()) {
			
			removeNetIdButton.setEnabled(true);
		}
		else {
			removeNetIdButton.setEnabled(false);
		}
		removeNetIdButton.addStyleName("glowing-border");
		removeNetIdButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.getVpcRequisition().getCustomerAdminUserIdList().remove(userId);
				adminTable.remove(netIdLabel);
				adminTable.remove(removeNetIdButton);
			}
		});
		adminLookupSB.setText("");
		if (numRows > 6) {
			if (netIdRowNum > 5) {
				netIdRowNum = 0;
				netIdColumnNum = netIdColumnNum + 2;
				removeButtonColumnNum = removeButtonColumnNum + 2;
			}
			else {
				netIdRowNum ++;
			}
		}
		else {
			netIdRowNum = numRows;
		}
		adminTable.setWidget(netIdRowNum, netIdColumnNum, netIdLabel);
		adminTable.setWidget(netIdRowNum, removeButtonColumnNum, removeNetIdButton);
	}

	void initializeNetIdPanel() {
		adminTable.removeAllRows();
//		if (presenter.getVpcRequisition() != null) {
//			GWT.log("Adding " + presenter.getVpcRequisition().getCustomerAdminUserIdList().size() + " net ids to the panel (update).");
//			for (String netId : presenter.getVpcRequisition().getCustomerAdminUserIdList()) {
//				addAdminToPanel(netId);
//			}
//		}
	}


	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}

	@Override
	public void applyAWSAccountAdminMask() {
		okayButton.setEnabled(true);
		provisioningIdTB.setEnabled(true);
		vpcTypeLB.setEnabled(true);
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		okayButton.setEnabled(false);
		provisioningIdTB.setEnabled(false);
		vpcTypeLB.setEnabled(false);
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public void setEditing(boolean isEditing) {
		this.editing = isEditing;
	}

	@Override
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setVpcpIdViolation(String message) {
		
		
	}

	@Override
	public void initPage() {
		this.setFieldViolations(false);
		this.registerHandlers();
		adminLookupSB.setText("");
		adminLookupSB.getElement().setPropertyString("placeholder", "<enter name or netid>");

		vpcpReqPurposeTA.setText("");
		vpcpReqPurposeTA.getElement().setPropertyString("placeholder", "enter a purpose for this VPC");
		
		vpcpReqTypePanel.getElement().getStyle().setLineHeight(2.6, Unit.EM);
		vpcpReqTypeLB.setVisible(false);
		vpcpReqTypeLB.setEnabled(true);
		regionPanel.setVisible(false);
		vpcpReqPurposeTA.setVisible(false);
		provisionWithVpcCB.setValue(false, true);
		provisionWithVpcCB.setEnabled(true);
		
		if (editing) {
			GWT.log("maintain VPCP view initPage.  editing");
			// hide generate grid, show maintain grid
			generateVpcpPanel.setVisible(false);
			maintainVpcpGrid.setVisible(true);
			// clear the page
			provisioningIdTB.setText("");
			vpcTypeLB.setSelectedIndex(0);

			if (presenter.getVpcp() != null) {
				GWT.log("maintain VPCP view initPage.  VPCP: " + presenter.getVpcp().getProvisioningId());
				provisioningIdTB.setText(presenter.getVpcp().getProvisioningId());
				if (presenter.getVpcp().getVpcRequisition().getType() != null) {
					provisionWithVpcCB.setValue(true);
					vpcpReqTypeLB.setVisible(true);
					regionPanel.setVisible(true);
				}
			}
		}
		else {
			GWT.log("maintain VPCP view initPage.  generate");
			initializeNetIdPanel();

			// hide maintain grid, show generate grid
			maintainVpcpGrid.setVisible(false);
			generateVpcpPanel.setVisible(true);
			ownerLookupSB.setText("");
			ownerLookupSB.getElement().setPropertyString("placeholder", "<enter name or netid>");
			vpcpReqSpeedTypeTB.setText("");
//			vpcpReqTicketIdTB.setText("");
			requestorLookupSB.setText("");
			requestorLookupSB.getElement().setPropertyString("placeholder", "<enter name or netid>");
			vpcpReqPurposeTA.setText("");
			
			vpcpReqTypeLB.setSelectedIndex(0);
			accountLB.setSelectedIndex(0);
			vpcpReqComplianceClassLB.setSelectedIndex(0);
			vpcpReqNotifyAdminsCB.setValue(false);
//			accountCP.clear();
		}
		presenter.setSpeedChartStatusForKey(presenter.getVpcRequisition().getSpeedType(), speedTypeHTML, false);
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setVpcTypeItems(List<String> vpcTypes) {
		this.vpcTypes = vpcTypes;
		if (editing) {
			vpcTypeLB.clear();
			if (vpcTypes != null) {
				int i=0;
				for (String type : vpcTypes) {
					vpcTypeLB.addItem("Type: " + type, type);
					if (presenter.getVpcp() != null) {
						if (presenter.getVpcp().getVpcRequisition().getType() != null) {
							if (presenter.getVpcp().getVpcRequisition().getType().equals(type)) {
								vpcTypeLB.setSelectedIndex(i);
							}
						}
					}
					i++;
				}
			}
		}
		else {
			vpcpReqTypeLB.clear();
			vpcpReqTypeLB.addItem("-- Select VPC Type --", "");
			if (vpcTypes != null) {
				int i=1;
				for (String type : vpcTypes) {
					vpcpReqTypeLB.addItem("Type: " + type, type);
					if (presenter.getVpcRequisition() != null) {
						if (presenter.getVpcRequisition().getType() != null) {
							if (presenter.getVpcRequisition().getType().equals(type)) {
								vpcpReqTypeLB.setSelectedIndex(i);
							}
						}
					}
					i++;
				}
			}
		}
	}
	@Override
	public void setAccountItems(List<AccountPojo> accounts) {
		this.accounts = accounts;
		accountLB.clear();
		accountLB.addItem("-- Select --", "");
		if (accounts != null) {
			for (AccountPojo account : accounts) {
				accountLB.addItem(account.getAccountId() + "-" + account.getAccountName(), account.getAccountId());
			}
		}
	}
	@Override
	public void setComplianceClassItems(List<String> complianceClassTypes) {
		this.complianceClassTypes = complianceClassTypes;
		vpcpReqComplianceClassLB.clear();
		vpcpReqComplianceClassLB.addItem("-- Select --", "");
		if (complianceClassTypes != null) {
			for (String type : complianceClassTypes) {
				vpcpReqComplianceClassLB.addItem(type, type);
			}
		}
		
	}

	@Override
	public Widget getSpeedTypeWidget() {
		return vpcpReqSpeedTypeTB;
	}
	@Override
	public List<Widget> getMissingRequiredFields() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		VpcRequisitionPojo vpcr = presenter.getVpcRequisition();
		if (vpcr.getAuthenticatedRequestorUserId() == null || vpcr.getAuthenticatedRequestorUserId().length() == 0) {
			this.setFieldViolations(true);
			fields.add(requestorLookupSB);
		}
		if (vpcr.getAccountOwnerUserId() == null || vpcr.getAccountOwnerUserId().length() == 0) {
			this.setFieldViolations(true);
			fields.add(ownerLookupSB);
		}
		if (vpcr.getSpeedType() == null || vpcr.getSpeedType().length() == 0) {
			this.setFieldViolations(true);
			fields.add(vpcpReqSpeedTypeTB);
		}
		if (vpcr.getCustomerAdminUserIdList() == null || vpcr.getCustomerAdminUserIdList().size() == 0) {
			// no customer admins were added so we'll just put the owner id in the list as the on
			// customer admin
			vpcr.getCustomerAdminUserIdList().add(vpcr.getAccountOwnerUserId());
		}
		if (vpcr.getType() == null || vpcr.getType().length() == 0) {
			this.setFieldViolations(true);
			fields.add(vpcTypeLB);
		}
		if (vpcr.getComplianceClass() == null || vpcr.getComplianceClass().length() == 0) {
			this.setFieldViolations(true);
			fields.add(vpcpReqComplianceClassLB);
		}
		// if provision with VPC is checked, they have to specify a region.
		// otherwise, we'll just select the first one from the list in the region drop down
		if (provisionWithVpcCB.getValue()) {
			if (vpcr.getRegion() == null || vpcr.getRegion().length() == 0) {
				this.setFieldViolations(true);
				fields.add(regionLB);
			}
		}
		else {
			vpcr.setRegion(regionLB.getValue(1));
		}
		if (provisionWithVpcCB.getValue()) {
			if (vpcr.getPurpose() == null || vpcr.getPurpose().length() == 0) {
				this.setFieldViolations(true);
				fields.add(vpcpReqPurposeTA);
			}
		}
		else {
			vpcr.setPurpose("Not applicable (no VPC)");
		}
		return fields;
	}
	@Override
	public void resetFieldStyles() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		fields.add(requestorLookupSB);
		fields.add(ownerLookupSB);
		fields.add(vpcpReqSpeedTypeTB);
		fields.add(adminLookupSB);
		fields.add(vpcTypeLB);
		fields.add(vpcpReqComplianceClassLB);
		fields.add(regionLB);
		this.resetFieldStyles(fields);
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		return cancelButton;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		return okayButton;
	}
	@Override
	public void setSpeedTypeStatus(String status) {
		speedTypeHTML.setHTML(status);
	}
	@Override
	public void setSpeedTypeColor(String color) {
		speedTypeHTML.getElement().getStyle().setColor(color);
	}
	@Override
	public void setSpeedTypeConfirmed(boolean confirmed) {
		this.speedTypeConfirmed = confirmed;
	}
	@Override
	public boolean isSpeedTypeConfirmed() {
		return this.speedTypeConfirmed;
	}
	@Override
	public void addAdminUserId(String userId, String title) {
		this.addAdminToPanel(userId, title);
	}
	@Override
	public void applyCentralAdminMask() {
		okayButton.setEnabled(true);
		provisioningIdTB.setEnabled(true);
		vpcTypeLB.setEnabled(true);
	}
	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}
	@Override
	public void vpcpPromptCancel() {
		
		
	}
	@Override
	public void vpcpConfirmOkay() {
		
		
	}
	@Override
	public void vpcpConfirmCancel() {
		
		
	}
	@Override
	public void disableButtons() {
		
		
	}
	@Override
	public void enableButtons() {
		
		
	}
	@Override
	public void applyNetworkAdminMask() {
		
		
	}
	@Override
	public void setAwsRegionItems(List<AWSRegionPojo> regionTypes) {
		this.regionTypes = regionTypes;
		regionLB.clear();
		regionLB.addItem("-- Select Region --", "");
		if (regionTypes != null) {
			for (AWSRegionPojo region : regionTypes) {
				regionLB.addItem(region.getValue(), region.getCode());
			}
		}
	}

	@Override
	public void addSpeedTypeStyle(String styleName) {
		speedTypeHTML.setStyleName(styleName);
	}
}
