package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.account.ListAccountView;
import edu.emory.oit.vpcprovisioning.shared.AccountDeprovisioningRequisitionPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountExtraMetaDataPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.FilterStatusPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopListAccount extends ViewImplBase implements ListAccountView {
	Presenter presenter;
	private ListDataProvider<AccountPojo> dataProvider = new ListDataProvider<AccountPojo>();
	private SingleSelectionModel<AccountPojo> selectionModel;
	List<AccountPojo> accountList = new java.util.ArrayList<AccountPojo>();
	UserAccountPojo userLoggedIn;
    PopupPanel actionsPopup = new PopupPanel(true);
	String filterBeingTyped="";

	/*** FIELDS ***/
	@UiField(provided=true) SimplePager accountListPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField(provided=true) SimplePager topListPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField Button addAccountButton;
	@UiField Button actionsButton;
	@UiField(provided=true) CellTable<AccountPojo> accountListTable = new CellTable<AccountPojo>(15, (CellTable.Resources)GWT.create(MyCellTableResources.class));
	@UiField VerticalPanel accountListPanel;
	@UiField HorizontalPanel pleaseWaitPanel;

	@UiField Button clearFilterButton;
	@UiField TextBox filterTB;
	@UiField PushButton refreshButton;
	@UiField HTML noResultsHTML;
	
	@UiHandler("refreshButton")
	void refreshButtonClicked(ClickEvent e) {
		presenter.refreshList(userLoggedIn);
	}

	private static DesktopListAccountUiBinder uiBinder = GWT.create(DesktopListAccountUiBinder.class);

	interface DesktopListAccountUiBinder extends UiBinder<Widget, DesktopListAccount> {
	}

	public interface MyCellTableResources extends CellTable.Resources {

	     @Source({CellTable.Style.DEFAULT_CSS, "cellTableStyles.css" })
	     public CellTable.Style cellTableStyle();
	 }
	
	public DesktopListAccount() {
		initWidget(uiBinder.createAndBindUi(this));

		setRefreshButtonImage(refreshButton);
		
		addAccountButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Should go to maintain account here...");
				hidePleaseWaitDialog();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_ACCOUNT);
			}
		}, ClickEvent.getType());
	}

	@UiHandler("clearFilterButton")
	void clearFilterButtonClicked(ClickEvent e) {
		// clear filter
		filterTB.setText("");
		presenter.clearFilter();
	}
	@UiHandler("filterTB")
	void filterTBBlur(BlurEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBMouseOver(MouseOverEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBKeyDown(KeyDownEvent e) {
		FilterStatusPojo status = checkFilterStatus(e, filterTB, filterBeingTyped);
		if (status.isValid() && status.isApplyFilter()) {
			filterBeingTyped = status.getFilteredText();
			GWT.log("filtering by: '" + status.getFilteredText());
			presenter.filterByText(status.getFilteredText());
		}
	}
	
	@UiHandler("actionsButton")
	void actionsButtonClicked(ClickEvent e) {
		actionsPopup.clear();
	    actionsPopup.setAutoHideEnabled(true);
	    actionsPopup.setAnimationEnabled(true);
	    actionsPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
	    Grid grid = new Grid(6, 1);
	    grid.setCellSpacing(8);
	    actionsPopup.add(grid);
	    
	    // anchors for:
	    // - view/edit
	    // - delete
	    // - view bill summaries?
	    String anchorText = "View/Maintain Account";

		Anchor maintainAnchor = new Anchor(anchorText);
		maintainAnchor.addStyleName("productAnchor");
		maintainAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		maintainAnchor.setTitle("View/Maintain selected Account");
		maintainAnchor.ensureDebugId(anchorText);
		maintainAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					getAppShell().addBreadCrumb("Maintain Account", ActionNames.MAINTAIN_ACCOUNT, m);
					ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT, m);
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(0, 0, maintainAnchor);

		Anchor deleteAnchor = new Anchor("Delete Account Metadata");
		deleteAnchor.addStyleName("productAnchor");
		deleteAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		deleteAnchor.setTitle("Delete the metadata for the selected account.  NOTE:  this is different than the 'Terminate Account' action as this only deletes the account metadata and does NOT remove the account from AWS.");
		deleteAnchor.ensureDebugId(deleteAnchor.getText());
		deleteAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					if (userLoggedIn.isCentralAdmin()) {
						presenter.deleteAccount(m);
					}
					else {
						showMessageToUser("You are not authorized to perform this function for this account.");
					}
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(1, 0, deleteAnchor);

		Anchor billSummaryAnchor = new Anchor("View Bill Summary");
		billSummaryAnchor.addStyleName("productAnchor");
		billSummaryAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		billSummaryAnchor.setTitle("View bill summary for selected Account");
		billSummaryAnchor.ensureDebugId(billSummaryAnchor.getText());
		billSummaryAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					// just use a popup here and not try to show the "normal" CidrAssignment
					// maintenance view.  This is handled in the AppBootstrapper when the events are registered.
					if (userLoggedIn.isCentralAdmin() || userLoggedIn.isAdminForAccount(m.getAccountId())) {
						// show billing information for this account
						ActionEvent.fire(presenter.getEventBus(), ActionNames.SHOW_BILL_SUMMARY_FOR_ACCOUNT, m);
					}
					else {
						showMessageToUser("You are not authorized to perform this function for this account.");
					}
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(2, 0, billSummaryAnchor);

		Anchor createAccountNotificationAnchor = new Anchor("Create Account Notification");
		createAccountNotificationAnchor.addStyleName("productAnchor");
		createAccountNotificationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		createAccountNotificationAnchor.setTitle("Create Account Notification");
		createAccountNotificationAnchor.ensureDebugId(createAccountNotificationAnchor.getText());
		createAccountNotificationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					if (userLoggedIn.isCentralAdmin()) {
						// dialog for creating a service account
						ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_ACCOUNT_NOTIFICATION, m);
					}
					else {
						showMessageToUser("You are not authorized to perform this function for this account.");
					}
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(3, 0, createAccountNotificationAnchor);

		Anchor createServiceAccountAnchor = new Anchor("Create Service Account");
		createServiceAccountAnchor.addStyleName("productAnchor");
		createServiceAccountAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		createServiceAccountAnchor.setTitle("Create Service Account");
		createServiceAccountAnchor.ensureDebugId(createServiceAccountAnchor.getText());
		createServiceAccountAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					if (userLoggedIn.isCentralAdmin() || userLoggedIn.isAdminForAccount(m.getAccountId())) {
						// dialog for creating a service account
						ActionEvent.fire(presenter.getEventBus(), ActionNames.INCIDENT_CREATE_SERVICE_ACCOUNT, m);
					}
					else {
						showMessageToUser("You are not authorized to perform this function for this account.");
					}
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(4, 0, createServiceAccountAnchor);

		// this will change to Deprovision or Close account
		Anchor terminateAnchor = new Anchor("Permanently Close Account");
		terminateAnchor.addStyleName("productAnchor");
		terminateAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		terminateAnchor.setTitle("Permanently close the selected Account");
		terminateAnchor.ensureDebugId(terminateAnchor.getText());
		terminateAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AccountPojo m = selectionModel.getSelectedObject();
				if (m != null) {
					if (userLoggedIn.isCentralAdmin() || userLoggedIn.isAdminForAccount(m.getAccountId())) {
						// dialog for terminating account
						AccountDeprovisioningRequisitionPojo req = new AccountDeprovisioningRequisitionPojo();
						req.setAccountId(m.getAccountId());
						req.setRequestorId(userLoggedIn.getPublicId());
						ActionEvent.fire(presenter.getEventBus(), ActionNames.SHOW_ACCOUNT_DEPROVISIONING_CONFIRMATION, req, m);
					}
					else {
						showMessageToUser("You are not authorized to perform this function for this account.");
					}
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(5, 0, terminateAnchor);

		actionsPopup.showRelativeTo(actionsButton);
	}

	@Override
	public void clearList() {
		accountListTable.setVisibleRangeAndClearData(accountListTable.getVisibleRange(), true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setAccounts(List<AccountPojo> accounts) {
		GWT.log("view Setting accounts.");
		this.accountList = accounts;
		this.initializeAccountListTable();
	    accountListPager.setDisplay(accountListTable);
	    topListPager.setDisplay(accountListTable);
	}
	private Widget initializeAccountListTable() {
		GWT.log("initializing ACCOUNT list table...");
		accountListTable.setTableLayoutFixed(false);
		accountListTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		
		// set range to display
		accountListTable.setVisibleRange(0, 15);
		
		// create dataprovider
		dataProvider = new ListDataProvider<AccountPojo>();
		dataProvider.addDataDisplay(accountListTable);
		dataProvider.getList().clear();
		dataProvider.getList().addAll(this.accountList);
		
		selectionModel = 
	    	new SingleSelectionModel<AccountPojo>(AccountPojo.KEY_PROVIDER);
		accountListTable.setSelectionModel(selectionModel);
	    
	    selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	    	@Override
	    	public void onSelectionChange(SelectionChangeEvent event) {
	    		AccountPojo m = selectionModel.getSelectedObject();
	    		GWT.log("Selected account is: " + m.getAccountId());
	    	}
	    });

	    ListHandler<AccountPojo> sortHandler = 
	    	new ListHandler<AccountPojo>(dataProvider.getList());
	    accountListTable.addColumnSortHandler(sortHandler);

	    if (accountListTable.getColumnCount() == 0) {
		    initAccountListTableColumns(sortHandler);
	    }
		
		return accountListTable;
	}
	
	private void initAccountListTableColumns(ListHandler<AccountPojo> sortHandler) {
		GWT.log("initializing ACCOUNT list table columns...");
		
		// Checkbox column. This table will uses a checkbox column for selection.
	    // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
	    // mouse selection.
	    Column<AccountPojo, Boolean> checkColumn = new Column<AccountPojo, Boolean>(
	        new CheckboxCell(true, false)) {
	      @Override
	      public Boolean getValue(AccountPojo object) {
	        // Get the value from the selection model.
	        return selectionModel.isSelected(object);
	      }
	    };
	    accountListTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
	    accountListTable.setColumnWidth(checkColumn, 40, Unit.PX);
	    
		// ACCOUNT id column
		Column<AccountPojo, String> acctIdColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getAccountId();
			}
		};
		acctIdColumn.setSortable(true);
		acctIdColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(acctIdColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				return o1.getAccountId().compareTo(o2.getAccountId());
			}
		});
		accountListTable.addColumn(acctIdColumn, "Account ID");
		
		// account name
		Column<AccountPojo, String> acctNameColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getAccountName();
			}
		};
		acctNameColumn.setSortable(true);
		sortHandler.setComparator(acctNameColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				// we really want to sort by the "sequence" part of the name,
				// if one exists...
				String s_seq1 = extractNumberFromString(o1.getAccountName());
				String s_seq2 = extractNumberFromString(o2.getAccountName());
				if (s_seq1 == null || 
					s_seq1.length() == 0 || 
					s_seq2 == null || 
					s_seq2.length() == 0) {
					
					return o1.getAccountName().compareTo(o2.getAccountName());
				}
				else {
					int seq1 = Integer.parseInt(s_seq1);
					int seq2 = Integer.parseInt(s_seq2);
					if (seq1 == seq2) {
						return 0;
					}
					if (seq1 > seq2) {
						return -1;
					}
					return 1;
				}
			}
		});
		accountListTable.addColumn(acctNameColumn, "Account Name");
		
		// alternate name
		Column<AccountPojo, String> acctAltNameColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getAlternateName();
			}
		};
		acctAltNameColumn.setSortable(true);
		sortHandler.setComparator(acctAltNameColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				if (o1.getAlternateName() != null && o2.getAlternateName() != null) {
					return o1.getAlternateName().compareTo(o2.getAlternateName());
				}
				return -1;
			}
		});
		accountListTable.addColumn(acctAltNameColumn, "Alternate Name");

		// Account Type
		Column<AccountPojo, String> acctTypeColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getAccountType();
			}
		};
		acctTypeColumn.setSortable(true);
		sortHandler.setComparator(acctTypeColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				return o1.getAccountType().compareTo(o2.getAccountType());
			}
		});
		accountListTable.addColumn(acctTypeColumn, "Type");

		// account owner
		Column<AccountPojo, String> ownerColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				String firstName;
				String lastName;
				if (object.getAccountOwnerDirectoryMetaData().getFirstName() != null) {
					firstName = object.getAccountOwnerDirectoryMetaData().getFirstName();
				}
				else {
					firstName = "Unknown";
				}
				if (object.getAccountOwnerDirectoryMetaData().getLastName() != null) {
					lastName = object.getAccountOwnerDirectoryMetaData().getLastName();
				}
				else {
					lastName = "Unknown";
				}
				return firstName + " " + lastName;
			}
		};
		ownerColumn.setSortable(true);
		ownerColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(ownerColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				if (o1.getAccountOwnerDirectoryMetaData().getNetId() != null && o2.getAccountOwnerDirectoryMetaData().getNetId() != null) {
					return o1.getAccountOwnerDirectoryMetaData().getNetId().compareTo(o2.getAccountOwnerDirectoryMetaData().getNetId());
				}
				return -1;
			}
		});
		accountListTable.addColumn(ownerColumn, "Account Owner");
		
		// compliance class
		Column<AccountPojo, String> complianceClassColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getComplianceClass();
			}
		};
		complianceClassColumn.setSortable(true);
		complianceClassColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(complianceClassColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				return o1.getComplianceClass().compareTo(o2.getComplianceClass());
			}
		});
		accountListTable.addColumn(complianceClassColumn, "Compliance Class");
		
		// speed type (financial account number)
		Column<AccountPojo, String> finAcctColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				return object.getSpeedType();
			}
		};
		finAcctColumn.setSortable(true);
		finAcctColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(finAcctColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				return o1.getSpeedType().compareTo(o2.getSpeedType());
			}
		});
		accountListTable.addColumn(finAcctColumn, "Financial Acct Number");
		
		// unit/school
		Column<AccountPojo, String> unitColumn = 
				new Column<AccountPojo, String> (new TextCell()) {

			@Override
			public String getValue(AccountPojo object) {
				if (object.getExtraMetaData() != null) {
					if (object.getExtraMetaData().getUnitOrSchool() != null) {
						return object.getExtraMetaData().getUnitOrSchool();
					}
					else {
						return "Unknown";
					}
				}
				else {
					return "Unknown";
				}
			}
		};
		unitColumn.setSortable(true);
		unitColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(unitColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				if (o1.getExtraMetaData() != null && o2.getExtraMetaData() != null) {
					return o1.getExtraMetaData().getUnitOrSchool().
						compareTo(o2.getExtraMetaData().getUnitOrSchool());
				}
				else {
					return 0;
				}
			}
		});
		accountListTable.addColumn(unitColumn, "Unit/School");
		
		// department
		Column<AccountPojo, String> deptColumn = 
				new Column<AccountPojo, String> (new TextCell()) {

			@Override
			public String getValue(AccountPojo object) {
				if (object.getExtraMetaData() != null) {
					if (object.getExtraMetaData().getDepartmentName() != null) {
						return object.getExtraMetaData().getDepartmentName();
					}
					else {
						return "Unknown";
					}
				}
				else {
					return "Unknown";
				}
			}
		};
		deptColumn.setSortable(true);
		deptColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(deptColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				if (o1.getExtraMetaData() != null && o2.getExtraMetaData() != null) {
					return o1.getExtraMetaData().getDepartmentName().
						compareTo(o2.getExtraMetaData().getDepartmentName());
				}
				else {
					return 0;
				}
			}
		});
		accountListTable.addColumn(deptColumn, "Department");
		
		// principal investigator
		Column<AccountPojo, String> piColumn = 
			new Column<AccountPojo, String> (new TextCell()) {
			
			@Override
			public String getValue(AccountPojo object) {
				String firstName="Unknown";
				String lastName="Unknown";
				AccountExtraMetaDataPojo aemd = object.getExtraMetaData();
				if (aemd != null) {
					if (aemd.getPrincipalInvestigatorMetaData().getFirstName() != null) {
						firstName = aemd.getPrincipalInvestigatorMetaData().getFirstName();
					}
					if (aemd.getPrincipalInvestigatorMetaData().getLastName() != null) {
						lastName = aemd.getPrincipalInvestigatorMetaData().getLastName();
					}
					return firstName + " " + lastName;
				}
				return "Unknown";
			}
		};
		piColumn.setSortable(true);
		piColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(piColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
				AccountExtraMetaDataPojo aemd1 = o1.getExtraMetaData();
				AccountExtraMetaDataPojo aemd2 = o2.getExtraMetaData();
				if (aemd1 != null && aemd2 != null) {
					return aemd1.getPrincipalInvestigatorMetaData().getNetId().compareTo(aemd2.getPrincipalInvestigatorMetaData().getNetId());
				}
				else {
					return 0;
				}
			}
		});
		accountListTable.addColumn(piColumn, "Principal Investigator");

		// create time
		Column<AccountPojo, String> createTimeColumn = new Column<AccountPojo, String>(
				new TextCell()) {

			@Override
			public String getValue(AccountPojo object) {
				Date createTime = object.getCreateTime();
				return createTime != null ? dateFormat.format(createTime) : "Unknown";
			}
		};
		createTimeColumn.setSortable(true);
		sortHandler.setComparator(createTimeColumn, new Comparator<AccountPojo>() {
			public int compare(AccountPojo o1, AccountPojo o2) {
					Date c1 = o1.getCreateTime();
					Date c2 = o2.getCreateTime();
					if (c1 == null || c2 == null) {
						return 0;
					}
					return c1.compareTo(c2);
			}
		});
		accountListTable.addColumn(createTimeColumn, "Date/Time Provisioned");
		
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public void removeAccountFromView(AccountPojo account) {
		dataProvider.getList().remove(account);
	}

	@Override
	public Widget getStatusMessageSource() {
		return refreshButton;
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
		GWT.log("userLoggedIn is: " + this.userLoggedIn);
	}

	@Override
	public void applyAWSAccountAdminMask() {
		addAccountButton.setEnabled(true);
		actionsButton.setEnabled(true);
		clearFilterButton.setEnabled(true);
		filterTB.setEnabled(true);
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		// disable add account button
		addAccountButton.setEnabled(false);
		actionsButton.setEnabled(true);
		clearFilterButton.setEnabled(false);
		filterTB.setEnabled(false);
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		
		return null;
	}

	@Override
	public void resetFieldStyles() {
		
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		return null;
	}

	@Override
	public void initPage() {
		filterTB.setText("");
		filterTB.getElement().setPropertyString("placeholder", "enter text to filter list");
	}

	@Override
	public void applyCentralAdminMask() {
		addAccountButton.setEnabled(true);
		actionsButton.setEnabled(true);
		clearFilterButton.setEnabled(true);
		filterTB.setEnabled(true);
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}

	@Override
	public void vpcpPromptCancel() {
		
		
	}

	@Override
	public void vpcpConfirmOkay() {
		
		
	}

	@Override
	public void vpcpConfirmCancel() {
		
		
	}

	@Override
	public void disableButtons() {
		clearFilterButton.setEnabled(false);
		actionsButton.setEnabled(false);
		addAccountButton.setEnabled(false);
	}

	@Override
	public void enableButtons() {
		clearFilterButton.setEnabled(true);
		actionsButton.setEnabled(true);
		addAccountButton.setEnabled(true);
	}

	@Override
	public void applyNetworkAdminMask() {
		
		
	}

	@Override
	public void showNoResultsMessage() {
		noResultsHTML.setVisible(true);
	}

	@Override
	public void hideNoResultsMessage() {
		noResultsHTML.setVisible(false);
	}
}
