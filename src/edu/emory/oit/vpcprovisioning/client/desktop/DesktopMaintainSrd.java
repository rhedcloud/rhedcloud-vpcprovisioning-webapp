package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.srd.MaintainSrdView;
import edu.emory.oit.vpcprovisioning.shared.DetectedSecurityRiskPojo;
import edu.emory.oit.vpcprovisioning.shared.ErrorPojo;
import edu.emory.oit.vpcprovisioning.shared.RemediationResultPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopMaintainSrd extends ViewImplBase implements MaintainSrdView {
	Presenter presenter;
	boolean editing;
	UserAccountPojo userLoggedIn;
	Tree cdr_tree;
	Tree detectionResultErrors_tree;


	private static DesktopMaintainSrdUiBinder uiBinder = GWT.create(DesktopMaintainSrdUiBinder.class);

	interface DesktopMaintainSrdUiBinder extends UiBinder<Widget, DesktopMaintainSrd> {
	}

	public DesktopMaintainSrd() {
		initWidget(uiBinder.createAndBindUi(this));
//		Image expandImg = new Image("images/expand_icon.png");
//		expandImg.setWidth("30px");
//		expandImg.setHeight("30px");
//		expandButton.getUpFace().setImage(expandImg);
		
//		Image collapseImg = new Image("images/collapse_icon.png");
//		collapseImg.setWidth("30px");
//		collapseImg.setHeight("30px");
//		collapseButton.getUpFace().setImage(collapseImg);
	}

	@UiField Button okayButton;
	@UiField Label createInfoLabel;
	@UiField Label updateInfoLabel;
	@UiField TextBox accountIdTB;
	@UiField TextBox detectorTB;
	@UiField TextBox remediatorTB;
	@UiField ScrollPanel detectedRisksPanel;
	@UiField Button expandButton;
	@UiField Button collapseButton;
	@UiField TextBox detectionTypeTB;
	@UiField TextBox detectionStatusTB;
	@UiField ScrollPanel detectionResultErrorPanel;

	@UiHandler("expandButton")
	void expandButtonClicked(ClickEvent e) {
		// TODO: expand all child tree items
//		if (cdr_tree != null && cdr_tree.getItemCount() > 0) {
//			int rootItemCount = cdr_tree.getItemCount();
//			for (int i=0; i<rootItemCount; i++) {
//				TreeItem ti = cdr_tree.getItem(i);
//				ti.getChild(0).getChild(0).setState(true, false);
//				ti.getChild(0).setState(true, false);
//				ti.setState(true, false);
////				expandAll(ti);
//			}
//		}
		showMessageToUser("This functionality is comming soon.........");
	}
//	void expandAll(TreeItem ti) {
//		int childCount = ti.getChildCount();
//		if (childCount == 0) {
//			GWT.log("no children, returning");
//			ti.getParentItem().setState(true, false);
//			return;
//		}
//		for (int i=0; i<childCount; i++) {
//			TreeItem ti2 = ti.getChild(i);
//			GWT.log("has children, continuing");
//			expandAll(ti2);
//		}
//	}
	
	@UiHandler("collapseButton")
	void collapseButtonClicked(ClickEvent e) {
		// TODO: collapse all child tree items
		showMessageToUser("This functionality is comming soon.........");
	}
	@Override
	public void setWidget(IsWidget w) {
		
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		
		
	}

	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		
		
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}

	@Override
	public void applyCentralAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		
		return null;
	}

	@Override
	public void resetFieldStyles() {
		
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		return okayButton;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		return okayButton;
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}

	@Override
	public void vpcpPromptCancel() {
		
		
	}

	@Override
	public void vpcpConfirmOkay() {
		
		
	}

	@Override
	public void vpcpConfirmCancel() {
		
		
	}

	@Override
	public void disableButtons() {
		
		
	}

	@Override
	public void enableButtons() {
		
		
	}

	@Override
	public void setEditing(boolean isEditing) {
		
		
	}

	@Override
	public void setLocked(boolean locked) {
		
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void initPage() {
		detectedRisksPanel.clear();
		accountIdTB.setText(presenter.getSrd().getAccountId());
		detectorTB.setText(presenter.getSrd().getSecurityRiskDetector());
		remediatorTB.setText(presenter.getSrd().getSecurityRiskRemediator());
		if (presenter.getSrd().getDetectionResult() != null) {
			detectionTypeTB.setText(presenter.getSrd().getDetectionResult().getType());
			detectionStatusTB.setText(presenter.getSrd().getDetectionResult().getStatus());
			Tree detectionResultErrorsTree = createDetectionResultErrorsTree();
			detectionResultErrorPanel.setWidget(detectionResultErrorsTree);
		}
		
		Tree detectedRisksTree = createDetectedRisksTree();
		detectedRisksPanel.setWidget(detectedRisksTree);
		
		String createInfo = "Created by " + presenter.getSrd().getCreateUser() + 
				" at " + dateFormat.format(presenter.getSrd().getCreateTime());
		createInfoLabel.setText(createInfo);
		String updateInfo = "Never Updated";
		if (presenter.getSrd().getUpdateTime() != null) {
			updateInfo = "Updated by " + presenter.getSrd().getUpdateUser() + 
					" at " + dateFormat.format(presenter.getSrd().getUpdateTime());
		}
		updateInfoLabel.setText(updateInfo);;
	}

	private Tree createDetectionResultErrorsTree() {
		detectionResultErrors_tree = new Tree();
		
		if (presenter.getSrd().getDetectionResult() == null || presenter.getSrd().getDetectionResult().getErrors().size() == 0) {
			TreeItem dsrItem = detectionResultErrors_tree.addTextItem("No Errors Found");
			dsrItem.setTitle(Integer.toString(0));
			dsrItem.addTextItem("");
			return detectionResultErrors_tree;
		}
		
		int i=0;
		for (String error : presenter.getSrd().getDetectionResult().getErrors()) {
			TreeItem errorItem = detectionResultErrors_tree.addTextItem("Error: " + i);
			errorItem.setTitle(Integer.toString(i));

				TextArea ta = new TextArea();
				ta.setEnabled(false);
				ta.setText(error);
				ta.setWidth("500px");
				ta.setHeight("50px");
				errorItem.addItem(ta);
			i++;
		}
		
		return detectionResultErrors_tree;
	}
	private Tree createDetectedRisksTree() {
		cdr_tree = new Tree();
		
		int i=0;
		for (DetectedSecurityRiskPojo dsr : presenter.getSrd().getDetectedSecurityRisks()) {
			TreeItem dsrItem = cdr_tree.addTextItem(dsr.getType() + ":" + dsr.getAmazonResourceName());
			dsrItem.setTitle(Integer.toString(i));
			i++;
			dsrItem.addTextItem("");
		}
		
		cdr_tree.addOpenHandler(new OpenHandler<TreeItem>() {
			@Override
			public void onOpen(OpenEvent<TreeItem> event) {
				TreeItem dsrItem = event.getTarget();
				if (dsrItem.getChildCount() == 1) {
					dsrItem.setState(false, false);
				
					int index = Integer.parseInt(dsrItem.getTitle());
					DetectedSecurityRiskPojo dsr = presenter.getSrd().getDetectedSecurityRisks().get(index);
					final RemediationResultPojo rr = dsr.getRemediationResult();
					
					final TreeItem remediatorItem = dsrItem.addTextItem(rr.getStatus() + ": " + rr.getDescription());
					
					boolean addedError = false;
					for (ErrorPojo error : rr.getErrors()) {
						if (error.getDescription() != null && error.getDescription().length() > 0) {
							addedError=true;
							TextArea ta = new TextArea();
							ta.setEnabled(false);
							ta.setText(error.getDescription());
							ta.setWidth("500px");
							ta.setHeight("100px");
							remediatorItem.addItem(ta);
						}
						else {
							GWT.log("No Error Description");
						}
					}
					if (addedError) {
						remediatorItem.addTextItem("");
					}
					else {
						GWT.log("No Errors");
						remediatorItem.addTextItem("No Error Information Available");
						remediatorItem.addTextItem("");
					}
					
					dsrItem.getChild(0).remove();
					
					dsrItem.setState(true, false);
				}
			}
		});
		
		return cdr_tree;
	}
	
	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void applyNetworkAdminMask() {
		
		
	}

}
