package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonRpcSuggestOracle;
import edu.emory.oit.vpcprovisioning.client.common.DirectoryPersonSuggestion;
import edu.emory.oit.vpcprovisioning.client.common.RoleSelectionPopup;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.account.MaintainAccountView;
import edu.emory.oit.vpcprovisioning.shared.AccountExtraMetaDataPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.CustomRolePojo;
import edu.emory.oit.vpcprovisioning.shared.DirectoryMetaDataPojo;
import edu.emory.oit.vpcprovisioning.shared.EmailPojo;
import edu.emory.oit.vpcprovisioning.shared.FilterStatusPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertiesPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertyPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopMaintainAccount extends ViewImplBase implements MaintainAccountView {
	Presenter presenter;
	List<String> emailTypes;
	List<String> complianceClassTypes;
	UserAccountPojo userLoggedIn;
	boolean editing;
	int adminRowNum = 0;
	int adminColumnNum = 0;
	int removeButtonColumnNum = 1;
	String speedTypeBeingTyped=null;
	boolean speedTypeConfirmed = false;
	private final DirectoryPersonRpcSuggestOracle personSuggestions = new DirectoryPersonRpcSuggestOracle(Constants.SUGGESTION_TYPE_DIRECTORY_PERSON_NAME);
	private final DirectoryPersonRpcSuggestOracle ownerIdSuggestions = new DirectoryPersonRpcSuggestOracle(Constants.SUGGESTION_TYPE_DIRECTORY_PERSON_NAME);
	PopupPanel adminPleaseWaitDialog;
	PopupPanel waitForNotificationsDialog;
	List<String> filterTypeItems;
	boolean isCimpInstance=false;
	String filterBeingTyped="";

	private ListDataProvider<AccountNotificationPojo> dataProvider = new ListDataProvider<AccountNotificationPojo>();
	private MultiSelectionModel<AccountNotificationPojo> selectionModel;
	List<AccountNotificationPojo> pojoList = new java.util.ArrayList<AccountNotificationPojo>();
	PopupPanel actionsPopup = new PopupPanel(true);
	private List<CustomRolePojo> existingCustomRoles = new java.util.ArrayList<CustomRolePojo>();

	public interface MyCellTableResources extends CellTable.Resources {

	     @Source({CellTable.Style.DEFAULT_CSS, "cellTableStyles.css" })
	     public CellTable.Style cellTableStyle();
	}
	@UiField VerticalPanel notificationListPanel;
	@UiField(provided=true) SimplePager listPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField(provided=true) CellTable<AccountNotificationPojo> listTable = new CellTable<AccountNotificationPojo>(10, (CellTable.Resources)GWT.create(MyCellTableResources.class));

	@UiField VerticalPanel customRoleListPanel;
	@UiField(provided=true) SimplePager customRoleListPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField(provided=true) CellTable<CustomRolePojo> customRoleListTable = new CellTable<CustomRolePojo>(10, (CellTable.Resources)GWT.create(MyCellTableResources.class));
	private ListDataProvider<CustomRolePojo> customRoleDataProvider = new ListDataProvider<CustomRolePojo>();
	private SingleSelectionModel<CustomRolePojo> customRoleSelectionModel;

	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField Button billSummaryButton;
	@UiField Button okayButton;
	@UiField Button cancelButton;
	@UiField TextBox accountIdTB;
	@UiField TextBox accountNameTB;
	@UiField TextBox alternateNameTB;
	@UiField TextBox passwordLocationTB;
	@UiField TextBox speedTypeTB;

	// AWS Account associated emails
	@UiField VerticalPanel emailsVP;
	@UiField TextBox addEmailTF;
	@UiField ListBox addEmailTypeLB;
	@UiField Button addEmailButton;
	@UiField FlexTable emailTable;
	
	// AWS Account Administrator (net ids)
	@UiField VerticalPanel adminVP;
	@UiField FlexTable adminTable;
	@UiField Button addAdminButton;
	@UiField Label adminLabel;
	
	@UiField HTML speedTypeHTML;
	@UiField ListBox complianceClassLB;

	@UiField(provided=true) SuggestBox directoryLookupSB = new SuggestBox(personSuggestions, new TextBox());
	@UiField(provided=true) SuggestBox ownerIdSB = new SuggestBox(ownerIdSuggestions, new TextBox());
	@UiField PushButton refreshButton;
	
	@UiField Button clearFilterButton;
	@UiField TextBox filterTB;
	@UiField HTML filteredHTML;

	// AWS Account associated properties
	@UiField VerticalPanel propertiesVP;
	@UiField TextBox propertyKeyTF;
	@UiField TextBox propertyValueTF;
	@UiField Button addPropertyButton;
	@UiField FlexTable propertiesTable;
	@UiField Label speedTypeLabel;
	
	// custom roles associated to this account
	@UiField PushButton refreshCustomRolesButton;
    @UiField Button actionsButton;
    @UiField Button generateRoleButton;
	@UiField HTML noResultsHTML;
	
	// extra account meta-data fields
	@UiField DisclosurePanel extraProjectDataDP;
	@UiField ListBox unitOrSchoolLB;
	@UiField TextBox departmentTB;
	private final DirectoryPersonRpcSuggestOracle piSuggestions = new DirectoryPersonRpcSuggestOracle(Constants.SUGGESTION_TYPE_DIRECTORY_PERSON_NAME);
	@UiField(provided=true) SuggestBox piIdSB = new SuggestBox(piSuggestions, new TextBox());
	@UiField ListBox awsExperienceLevelLB;
	@UiField ListBox referrerLB;
	@UiField CheckBox cliExperienceCB;
	// primary
	@UiField TextBox primaryProjectNameTB;
	@UiField TextBox primaryProjectDataTypeTB;
	@UiField TextBox primaryProjectSoftwareTB;
	@UiField TextBox primaryProjectDomainAreaTB;
	@UiField ListBox primaryProjectPurposeLB;
	@UiField ListBox primaryProjectPlatformsLB;
	@UiField Button deleteAllMetaDataButton;
	// secondary
	@UiField Button deleteSecondaryProjectInfoButton;
	@UiField TextBox secondaryProjectNameTB;
	@UiField TextBox secondaryProjectDataTypeTB;
	@UiField TextBox secondaryProjectSoftwareTB;
	@UiField TextBox secondaryProjectDomainAreaTB;
	@UiField ListBox secondaryProjectPurposeLB;
	@UiField ListBox secondaryProjectPlatformsLB;

	List<String> primaryProjectPurposes = new java.util.ArrayList<String>();
	List<String> primaryProjectPlatforms = new java.util.ArrayList<String>();
	List<String> secondaryProjectPurposes = new java.util.ArrayList<String>();
	List<String> secondaryProjectPlatforms = new java.util.ArrayList<String>();
	List<String> unitsAndSchools = new java.util.ArrayList<String>();
	List<String> referrers = new java.util.ArrayList<String>();
//	List<String> awsExperienceLevels = new java.util.ArrayList<String>();
	PropertiesPojo awsExperienceLevels;

	@UiHandler ("deleteAllMetaDataButton")
	void deleteAllMetaDataButtonClick(ClickEvent e) {
		// TODO confirmation prompt
		
		// TODO: AccountExtraMetadata.delete in the presenter
		boolean confirmed = Window.confirm("You are about to delete "
				+ "ALL extra account meta-data.  Are you "
				+ "sure you wish to continue?");

		if (confirmed) {
			presenter.deleteAccountExtraMetaData();
		}
	}
	
	@UiHandler ("deleteSecondaryProjectInfoButton")
	void deleteSecondaryProjectInfoButtonClick(ClickEvent e) {
		// clear all secondary info fields and remove any
		// secondary info from the extrametadata object in the presenter
		
		if (presenter.getAccountExtraMetaData() != null) {
			presenter.getAccountExtraMetaData().setSecondaryProjectDataType(null);
			presenter.getAccountExtraMetaData().setSecondaryProjectDomainArea(null);
			presenter.getAccountExtraMetaData().setSecondaryProjectName(null);
			presenter.getAccountExtraMetaData().setSecondaryProjectPlatform(new java.util.ArrayList<String>());
			presenter.getAccountExtraMetaData().setSecondaryProjectPurpose(new java.util.ArrayList<String>());
			presenter.getAccountExtraMetaData().setSecondaryProjectSoftware(null);
		}
//		departmentTB.setText("");
//		piIdSB.setText("");
//		cliExperienceCB.setValue(false);
//		primaryProjectNameTB.setText("");
//		primaryProjectDataTypeTB.setText("");
//		primaryProjectSoftwareTB.setText("");
//		primaryProjectDomainAreaTB.setText("");
		secondaryProjectNameTB.setText("");
		secondaryProjectDataTypeTB.setText("");
		secondaryProjectSoftwareTB.setText("");
		secondaryProjectDomainAreaTB.setText("");

		// do these AFTER we've cleared out the secondary info in 
		// the presenter (in the object)
//		setReferrerItems(referrers);
//		setAwsExperienceLevelItems(awsExperienceLevels);
		setSecondaryPlatformItems(secondaryProjectPlatforms);
//		setPrimaryPlatformItems(primaryProjectPlatforms);
		setSecondaryPurposeItems(secondaryProjectPurposes);
//		setPrimaryPurposeItems(primaryProjectPlatforms);
//		setUnitOrSchoolItems(unitsAndSchools);


		// TODO: remind user that the account still has to be saved
		// OR: just do a extrameta data update here?
		presenter.saveAccountExtraMetaData();
	}
	
	@UiHandler ("addPropertyButton")
	void addPropertyButtonClick(ClickEvent e) {
		propertyKeyTF.setEnabled(true);
		if (addPropertyButton.getText().equalsIgnoreCase("Add")) {
			addProperty();
		}
		else {
			// update existing property
			PropertyPojo property = createPropertyFromFormData();
			property.setUpdateUser(userLoggedIn.getPublicId());
			property.setUpdateTime(new java.util.Date(System.currentTimeMillis()));
			presenter.updateProperty(property);
			initializeAccountPropertiesPanel();
		}
		propertyKeyTF.setText("");
		propertyValueTF.setText("");
	}
	
	private void initializeAccountPropertiesPanel() {
		addPropertyButton.setText("Add");
		propertyKeyTF.setEnabled(true);
		propertiesTable.removeAllRows();
		for (PropertyPojo prop : presenter.getAccount().getProperties()) {
			this.addPropertyToPanel(prop);
		}
	}

	private void addPropertyToPanel(final PropertyPojo prop) {
		final int numRows = propertiesTable.getRowCount();
		
		final Label keyLabel = new Label(prop.getName());
		keyLabel.addStyleName("emailLabel");
		String keyTitle="";
		if (prop.getCreateUser() != null) {
			keyTitle = "Created by: " + prop.getCreateUser();
		}
		if (prop.getCreateTime() != null) {
			keyTitle = keyTitle + ", Created at: " + prop.getCreateTime();
		}
		if (prop.getUpdateUser() != null) {
			if (keyTitle.length() > 0) {
				keyTitle = keyTitle + "\n";
			}
			keyTitle = keyTitle + "Last updated by: " + prop.getUpdateUser();
		}
		if (prop.getUpdateTime() != null) {
			keyTitle = keyTitle + ", Last updated at: " + prop.getUpdateTime();
		}
		if (prop.getUserComment() != null) {
			if (keyTitle.length() > 0) {
				keyTitle = keyTitle + "\n";
			}
			keyTitle = keyTitle + "Comments: " + prop.getUserComment();
		}
		if (prop.getEffectiveDate() != null || prop.getExpirationDate() != null) {
			if (keyTitle.length() > 0) {
				keyTitle = keyTitle + "\n";
			}
			keyTitle = keyTitle + "Effective Date: " + prop.getEffectiveDate();
		}
		if (prop.getExpirationDate() != null) {
			keyTitle = keyTitle + ", Expiration Date: " + prop.getExpirationDate();
		}
		if (keyTitle.length() > 0) {
			keyLabel.setTitle(keyTitle);
		}
		
		final Label valueLabel = new Label(prop.getValue());
		valueLabel.addStyleName("emailLabel");
		
		final PushButton editButton = new PushButton();
		editButton.setTitle("Edit this Property.");
		Image editImage = new Image("images/edit_icon.png");
		editImage.setWidth("30px");
		editImage.setHeight("30px");
		editButton.getUpFace().setImage(editImage);
		if (this.userLoggedIn.isCentralAdminManager()) {
			editButton.setEnabled(true);
		}
		else {
			editButton.setEnabled(false);
		}
		editButton.addStyleName("glowing-border");
		editButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.setSelectedProperty(prop);
				propertyKeyTF.setText(prop.getName());
				propertyKeyTF.setEnabled(false);
				propertyValueTF.setText(prop.getValue());
				addPropertyButton.setText("Update");
				Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
			        public void execute () {
			        	propertyValueTF.setFocus(true);
			        }
			    });
			}
		});
		
		final PushButton removeButton = new PushButton();
		removeButton.setTitle("Remove this Property.");
		Image removeImage = new Image("images/delete_icon.png");
		removeImage.setWidth("30px");
		removeImage.setHeight("30px");
		removeButton.getUpFace().setImage(removeImage);
		// disable buttons if userLoggedIn is NOT a central admin
		if (this.userLoggedIn.isCentralAdminManager()) {
			removeButton.setEnabled(true);
		}
		else {
			removeButton.setEnabled(false);
		}
		removeButton.addStyleName("glowing-border");
		removeButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.getAccount().getProperties().remove(prop);
				initPage();
			}
		});
		
		propertiesTable.setWidget(numRows, 0, keyLabel);
		propertiesTable.setWidget(numRows, 1, new Label("="));
		propertiesTable.setWidget(numRows, 2, valueLabel);
		propertiesTable.setWidget(numRows, 3, editButton);
		propertiesTable.setWidget(numRows, 4, removeButton);
	}

	private void addProperty() {
		List<Widget> fields = getMissingPropertyFields();
		if (fields != null && fields.size() > 0) {
			setFieldViolations(true);
			applyStyleToMissingFields(fields);
			showMessageToUser("Please provide data for the required fields.");
			return;
		}
		else {
			setFieldViolations(false);
			resetFieldStyles();
		}
		
		// check to make sure the property doesn't already exist
		PropertyPojo property = createPropertyFromFormData();
		if (presenter.getAccount().hasProperty(property.getName())) {
			// show an error
			showMessageToUser("The property '" + property.getName() 
				+ "' already exists in this account, please use a "
				+ "unique property key/name");
		}
		else {
			property.setCreateUser(userLoggedIn.getPublicId());
			property.setCreateTime(new java.util.Date(System.currentTimeMillis()));
			presenter.getAccount().getProperties().add(property);
			addPropertyToPanel(property);
		}
		this.resetFieldStyles();
	}

	private PropertyPojo createPropertyFromFormData() {
		PropertyPojo prop = new PropertyPojo();
		prop.setName(propertyKeyTF.getText());
		prop.setValue(propertyValueTF.getText());
		return prop;
	}

	private List<Widget> getMissingPropertyFields() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		if (propertyKeyTF.getText() == null || propertyKeyTF.getText().length() == 0) {
			fields.add(propertyKeyTF);
		}
		if (propertyValueTF.getText() == null || propertyValueTF.getText().length() == 0) {
			fields.add(propertyValueTF);
		}
		return fields;
	}

	@UiHandler("filterTB")
	void filterTBBlur(BlurEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBMouseOver(MouseOverEvent e) {
		filterBeingTyped="";
	}
	@UiHandler("filterTB")
	void filterTBKeyDown(KeyDownEvent e) {
		FilterStatusPojo status = checkFilterStatus(e, filterTB, filterBeingTyped);
		if (status.isValid() && status.isApplyFilter()) {
			filterBeingTyped = status.getFilteredText();
			GWT.log("filtering by: '" + status.getFilteredText());
			presenter.filterByText(status.getFilteredText());
		}
	}

	@UiHandler("clearFilterButton")
	void clearFilterButtonClicked(ClickEvent e) {
		filterTB.setText("");
		presenter.setAccountNotificationFilter(null);
		presenter.refreshAccountNotificationList(userLoggedIn);
		this.hideFilteredStatus();
	}

	@UiHandler("refreshButton")
	void refreshButtonClicked(ClickEvent e) {
		presenter.refreshAccountNotificationList(userLoggedIn);
	}

	@UiHandler("refreshCustomRolesButton")
	void refreshCustomRolesButtonClicked(ClickEvent e) {
		presenter.refreshCustomRoleList(userLoggedIn);
	}

	@UiHandler ("speedTypeTB")
	void speedTypeBlur(BlurEvent e) {
		// check CIMP status.  If we are a CIMP instance, we don't need to confirm speed type
		if (isCimpInstance) {
			return;
		}
		presenter.setSpeedChartStatusForKey(speedTypeTB.getText(), speedTypeHTML, true);
	}
	@UiHandler ("speedTypeTB")
	void speedTypeMouseOver(MouseOverEvent e) {
		// check CIMP status.  If we are a CIMP instance, we don't need to confirm speed type
		if (isCimpInstance) {
			return;
		}
		String acct = speedTypeTB.getText();
		presenter.setSpeedChartStatusForKeyOnWidget(acct, speedTypeTB, false);
	}
	@UiHandler ("speedTypeTB")
	void speedTypeKeyDown(KeyDownEvent e) {
		// check CIMP status.  If we are a CIMP instance, we don't need to confirm speed type
		if (isCimpInstance) {
			return;
		}
		this.setSpeedTypeConfirmed(false);
		int keyCode = e.getNativeKeyCode();
		char ccode = (char)keyCode;

		if (keyCode == KeyCodes.KEY_BACKSPACE) {
			if (speedTypeBeingTyped != null) {
				if (speedTypeBeingTyped.length() > 0) {
					speedTypeBeingTyped = speedTypeBeingTyped.substring(0, speedTypeBeingTyped.length() - 1);
				}
				presenter.setSpeedChartStatusForKey(speedTypeBeingTyped, speedTypeHTML, false);
				return;
			}
		}
		
		if (!isValidKey(keyCode, e.isAnyModifierKeyDown())) {
			GWT.log("[speedTypeKeyPressed] invalid key: " + keyCode);
			return;
		}
		else {
			if (speedTypeBeingTyped == null) {
				speedTypeBeingTyped = "";
			}
			speedTypeBeingTyped += String.valueOf(ccode);
		}

		presenter.setSpeedChartStatusForKey(speedTypeBeingTyped, speedTypeHTML, false);
	}
	
	@UiHandler ("billSummaryButton")
	void billSummaryButtonClick(ClickEvent e) {
		// show billing information for this account
		ActionEvent.fire(presenter.getEventBus(), ActionNames.SHOW_BILL_SUMMARY_FOR_ACCOUNT, presenter.getAccount());
	}
	
	@UiHandler ("addAdminButton")
	void addAdminButtonClick(ClickEvent e) {
		if (accountIdTB.getText() == null || accountIdTB.getText().trim().length() == 0) {
			showMessageToUser("Please enter a valid account ID.");
			return;
		}
		if (directoryLookupSB.getText() == null || directoryLookupSB.getText().trim().length() == 0) {
			showMessageToUser("Please enter the name of a person you want to assign a role to for this account.");
			return;
		}
		// present a dialog where user must select a role
		// then pass that role to the add method
		final RoleSelectionPopup rsp = new RoleSelectionPopup(true);
		// the popup will retrieve the custom roles itself
//		rsp.setExistingCustomRoles(presenter.getExistingCustomRoles());
		rsp.setUserLoggedIn(userLoggedIn);
		rsp.setAccount(presenter.getAccount());
		rsp.setEventBus(presenter.getEventBus());
		rsp.setAssigneeName(directoryLookupSB.getText());
		rsp.setAssignee(presenter.getDirectoryPerson());
		rsp.initPanel();
		rsp.showRelativeTo(addAdminButton);
		rsp.addCloseHandler(new CloseHandler<PopupPanel>() {
			@SuppressWarnings("rawtypes")
			@Override
			public void onClose(CloseEvent event) {
				GWT.log("selected role is: " + rsp.getSelectedRoleName());
				if (!rsp.isCanceled() && !rsp.isGenerate()) {
					if (rsp.isRoleSelected()) {
						if (rsp.getSelectedRoleName() != null) {
							if (rsp.getSelectedRoleName().equalsIgnoreCase(Constants.ROLE_NAME_RHEDCLOUD_AWS_CENTRAL_ADMIN)) {
								// have to check that the user logged in is a 
								// central admin and the person they're adding to 
								// this role is themselves.
								String assigneePpid = presenter.getDirectoryPerson().getKey();
								String userPpid = userLoggedIn.getPublicId();
								if (!assigneePpid.equalsIgnoreCase(userPpid)) {
									// they can't assign another person to the 
									// account level ca role
									showMessageToUser("You ONLY assign yourself to the account level central admin role.");
								}
								else {
									// all is good, they're assigning themselves
									addDirectoryPersonInRoleToAccount(rsp.getSelectedRoleName());
								}
							}
							else {
								addDirectoryPersonInRoleToAccount(rsp.getSelectedRoleName());
							}
						}
						else {
							showMessageToUser("You must select a role to assign this person to.");
						}
					}
					else {
						showMessageToUser("You must select a role to assign this person to.");
					}
				}
			}
		});
		
	}

	@UiHandler ("addEmailTF")
	void addEmailTFKeyPressed(KeyPressEvent e) {
        int keyCode = e.getNativeEvent().getKeyCode();
        if (keyCode == KeyCodes.KEY_ENTER) {
    		addEmailToAccount(addEmailTF.getText(), addEmailTypeLB.getSelectedValue());
        }
	}
	@UiHandler ("addEmailButton")
	void addEmailButtonClick(ClickEvent e) {
		addEmailToAccount(addEmailTF.getText(), addEmailTypeLB.getSelectedValue());
	}
	@UiHandler ("okayButton")
	void okayButtonClick(ClickEvent e) {
		presenter.getAccount().setAccountId(accountIdTB.getText());
		presenter.getAccount().setAccountName(accountNameTB.getText());
		String alternateName = alternateNameTB.getText();
		if (alternateName != null && alternateName.trim().length() > 0) {
			if (!isAlphaNumeric(alternateName)) {
				setFieldViolations(true);
				List<Widget> fields = new java.util.ArrayList<Widget>();
				fields.add(alternateNameTB);
				applyStyleToMissingFields(fields);
				showMessageToUser("Alternate name can only consist of numbers and letters.  "
					+ "Special characters are not allowed.  Please enter a valid alternate name.");
				return;
			}
		}
		setFieldViolations(false);
		resetFieldStyles();
		resetExtraMetaDataFieldStyles();
		presenter.getAccount().setAlternateName(alternateName);
		presenter.getAccount().setComplianceClass(complianceClassLB.getSelectedValue());
		presenter.getAccount().setPasswordLocation(passwordLocationTB.getText());
		presenter.getAccount().setSpeedType(speedTypeTB.getText());
		// emails are added as they're added in the interface
		
		// AccountExtraMetaData
		presenter.getAccountExtraMetaData().setAccountId(presenter.getAccount().getAccountId());
		presenter.getAccountExtraMetaData().setUnitOrSchool(unitOrSchoolLB.getSelectedValue());
		presenter.getAccountExtraMetaData().setDepartmentName(departmentTB.getText());
		presenter.getAccountExtraMetaData().setCliExperience(cliExperienceCB.getValue() ? "Yes" : "No");
		presenter.getAccountExtraMetaData().setLevelOfAwsExperience(awsExperienceLevelLB.getSelectedValue());
		presenter.getAccountExtraMetaData().setReferrerInfo(referrerLB.getSelectedValue());
		// primary project data
		presenter.getAccountExtraMetaData().setPrimaryProjectName(primaryProjectNameTB.getText());
		presenter.getAccountExtraMetaData().setPrimaryProjectDataType(primaryProjectDataTypeTB.getText());
		presenter.getAccountExtraMetaData().setPrimaryProjectSoftware(primaryProjectSoftwareTB.getText());
		presenter.getAccountExtraMetaData().setPrimaryProjectDomainArea(primaryProjectDomainAreaTB.getText());
		presenter.getAccountExtraMetaData().getPrimaryProjectPurpose().clear();
		for (int i=0; i<primaryProjectPurposeLB.getItemCount(); i++) {
			if (primaryProjectPurposeLB.isItemSelected(i)) {
				String value = primaryProjectPurposeLB.getValue(i);
				if (!value.equals(Constants.OTHER)) {
					presenter.getAccountExtraMetaData().getPrimaryProjectPurpose().add(value);
				}
			}
		}
		presenter.getAccountExtraMetaData().getPrimaryProjectPlatform().clear();
		for (int i=0; i<primaryProjectPlatformsLB.getItemCount(); i++) {
			if (primaryProjectPlatformsLB.isItemSelected(i)) {
				String value = primaryProjectPlatformsLB.getValue(i);
				if (!value.equals(Constants.OTHER)) {
					presenter.getAccountExtraMetaData().getPrimaryProjectPlatform().add(value);
				}
			}
		}
		// secondary project data
		presenter.getAccountExtraMetaData().setSecondaryProjectName(secondaryProjectNameTB.getText());
		presenter.getAccountExtraMetaData().setSecondaryProjectDataType(secondaryProjectDataTypeTB.getText());
		presenter.getAccountExtraMetaData().setSecondaryProjectSoftware(secondaryProjectSoftwareTB.getText());
		presenter.getAccountExtraMetaData().setSecondaryProjectDomainArea(secondaryProjectDomainAreaTB.getText());
		presenter.getAccountExtraMetaData().getSecondaryProjectPurpose().clear();
		for (int i=0; i<secondaryProjectPurposeLB.getItemCount(); i++) {
			if (secondaryProjectPurposeLB.isItemSelected(i)) {
				String value = secondaryProjectPurposeLB.getValue(i);
				if (!value.equals(Constants.OTHER)) {
					presenter.getAccountExtraMetaData().getSecondaryProjectPurpose().add(value);
				}
			}
		}
		presenter.getAccountExtraMetaData().getSecondaryProjectPlatform().clear();
		for (int i=0; i<secondaryProjectPlatformsLB.getItemCount(); i++) {
			if (secondaryProjectPlatformsLB.isItemSelected(i)) {
				String value = secondaryProjectPlatformsLB.getValue(i);
				if (!value.equals(Constants.OTHER)) {
					presenter.getAccountExtraMetaData().getSecondaryProjectPlatform().add(value);
				}
			}
		}
		
		// check CIMP status.  If we are a CIMP instance, we don't need to confirm speed type
		if (isCimpInstance) {
			presenter.saveAccount();
		}
		else {
			if (this.isSpeedTypeConfirmed()) {
				presenter.saveAccount();
			}
			else {
				if (presenter.didConfirmSpeedType()) {
					presenter.saveAccount();
				}
				else {
					this.showMessageToUser("SpeedType has not been confirmed.  Can't save the account "
							+ "until the SpeedType is confirmed.");
					GWT.log("SpeedType has not been confirmed. Can't save account until SpeedType is confirmed.");
				}
			}
		}
	}

	private static DesktopMaintainAccountUiBinder uiBinder = GWT.create(DesktopMaintainAccountUiBinder.class);

	interface DesktopMaintainAccountUiBinder extends UiBinder<Widget, DesktopMaintainAccount> {
	}

	public DesktopMaintainAccount() {
		initWidget(uiBinder.createAndBindUi(this));
		setRefreshButtonImage(refreshButton);
		setRefreshButtonImage(refreshCustomRolesButton);
		GWT.log("maintain account view init...");
		cancelButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ActionEvent.fire(presenter.getEventBus(), ActionNames.ACCOUNT_EDITING_CANCELED);
			}
		}, ClickEvent.getType());
	}

	/*
	 * Role/Roleassignment helper methods
	 */
	private void addDirectoryPersonInRoleToAccount(String roleName) {
		// get fullperson for current directory person
		// get net id from fullperson
		// create role assignment
		presenter.getAccount().setAccountId(accountIdTB.getText());
		presenter.addDirectoryPersonInRoleToAccount(roleName);
	}
	
	/*
	 * Admin helper methods
	 */
	
	@Override
	public void addRoleAssignment(final int ra_summaryIndex, String name, final String netId, String roleName, String widgetTitle) {
		final Label nameLabel = new Label(name + " (" + netId + ")/" + roleName);
		nameLabel.setTitle(widgetTitle);
		nameLabel.addStyleName("emailLabel");
		final Button removeAdminButton = new Button("Remove");
		// disable remove button if userLoggedIn is NOT an admin
		if (this.userLoggedIn.isAdminForAccount(presenter.getAccount().getAccountId()) ||
			this.userLoggedIn.isCentralAdmin()) {
			
			removeAdminButton.setEnabled(true);
		}
		else {
			removeAdminButton.setEnabled(false);
		}
		removeAdminButton.addStyleName("glowing-border");
		removeAdminButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				// RoleAssignment.Delete
				presenter.removeRoleAssignmentFromAccount(presenter.getAccount().getAccountId(), 
						presenter.getRoleAssignmentSummaries().get(ra_summaryIndex));
			}
		});
		directoryLookupSB.setText("");
		adminTable.setWidget(adminRowNum, adminColumnNum, nameLabel);
		adminTable.setWidget(adminRowNum, removeButtonColumnNum, removeAdminButton);
		adminRowNum++;
	}

	/*
	 *	associated email helper methods
	 */
	private void addEmailToAccount(String email, String type) {
		if (email != null && email.trim().length() > 0) {
			if (type != null && type.trim().length() > 0) {
				final String trimmedEmail = email.trim().toLowerCase();
				final String trimmedEmailType = type.trim().toLowerCase();
				EmailPojo emailPojo = new EmailPojo();
				emailPojo.setEmailAddress(trimmedEmail);
				emailPojo.setType(trimmedEmailType);
				if (presenter.getAccount().containsEmail(emailPojo)) {
					showStatus(addEmailButton, "That e-mail is already in the list, please enter a unique e-mail address.");
				}
				else {
					presenter.getAccount().getEmailList().add(emailPojo);
					addEmailToEmailPanel(emailPojo);
				}
			}
			else {
				showStatus(addEmailButton, "Please enter a valid e-mail address type.");
			}
		}
		else {
			showStatus(addEmailButton, "Please enter a valid e-mail address.");
		}
	}
	
	private void addEmailToEmailPanel(final EmailPojo email) {
		final int numRows = emailTable.getRowCount();
		final Label emailLabel = new Label(email.getEmailAddress() + "/" + email.getType());
		emailLabel.addStyleName("emailLabel");
		final Button removeEmailButton = new Button("Remove");
		// disable remove button if userLoggedIn is NOT an admin
		if (this.userLoggedIn.isCentralAdmin()) {
				
			removeEmailButton.setEnabled(true);
		}
		else {
			removeEmailButton.setEnabled(false);
		}
		removeEmailButton.addStyleName("glowing-border");
		removeEmailButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.getAccount().getEmailList().remove(email);
				emailTable.remove(emailLabel);
				emailTable.remove(removeEmailButton);
			}
		});
		addEmailTF.setText("");
		addEmailTypeLB.setSelectedIndex(0);
		emailTable.setWidget(numRows, 0, emailLabel);
		emailTable.setWidget(numRows, 1, removeEmailButton);
	}

	void initializeEmailPanel() {
		emailTable.removeAllRows();
		if (presenter.getAccount() != null) {
			GWT.log("Adding " + presenter.getAccount().getEmailList().size() + " e-mails to the email panel.");
			for (EmailPojo emailPojo : presenter.getAccount().getEmailList()) {
				addEmailToEmailPanel(emailPojo);
			}
		}
	}

	@Override
	public void setEditing(boolean isEditing) {
		this.editing = isEditing;
		if (isEditing) {
			// account id is immutable when/if the account is being edited
			accountIdTB.setEnabled(false);
		}
	}

	@Override
	public void setLocked(boolean locked) {
		
		
	}

	@Override
	public void setAccountIdViolation(String message) {
		
		
	}

	@Override
	public void setAccountNameViolation(String message) {
		
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void initPage() {
		// clear the page
		this.setFieldViolations(false);
		extraProjectDataDP.setOpen(false);
		speedTypeBeingTyped = "";
		speedTypeHTML.setHTML("");
		adminRowNum = 0;
		adminColumnNum = 0;
		removeButtonColumnNum = 1;
		accountIdTB.setText("");
		accountNameTB.setText("");
		alternateNameTB.setText("");
		ownerIdSB.setText("");
		passwordLocationTB.setText("");
		speedTypeTB.setText("");
		addEmailTF.setText("");
		addEmailTypeLB.setSelectedIndex(0);

		filterTB.setText("");
		filterTB.getElement().setPropertyString("placeholder", "enter search string");

		addEmailTF.setText("");
		addEmailTF.getElement().setPropertyString("placeholder", "enter e-mail");
		addEmailTypeLB.getElement().setPropertyString("placeholder", "select e-mail type");

		addPropertyButton.setText("Add");
		propertyKeyTF.setText("");
		propertyKeyTF.getElement().setPropertyString("placeholder", "enter property key");
		propertyValueTF.setText("");
		propertyValueTF.getElement().setPropertyString("placeholder", "enter property value");

		directoryLookupSB.setText("");
		directoryLookupSB.getElement().setPropertyString("placeholder", "<enter name or netid>");

		// populate fields if appropriate
		if (presenter.getAccount() != null) {
			accountIdTB.setText(presenter.getAccount().getAccountId());
			// account id can't be changed
			accountNameTB.setText(presenter.getAccount().getAccountName());
			alternateNameTB.setText(presenter.getAccount().getAlternateName());
			if (presenter.getAccount().getAccountOwnerDirectoryMetaData() != null) {
				String firstName;
				String lastName;
				if (presenter.getAccount().getAccountOwnerDirectoryMetaData().getFirstName() != null) {
					firstName = presenter.getAccount().getAccountOwnerDirectoryMetaData().getFirstName();
				}
				else {
					firstName = "Unknown";
				}
				if (presenter.getAccount().getAccountOwnerDirectoryMetaData().getLastName() != null) {
					lastName = presenter.getAccount().getAccountOwnerDirectoryMetaData().getLastName();
				}
				else {
					lastName = "Unknown";
				}

				ownerIdSB.setText(firstName + " " + lastName);
				presenter.setDirectoryMetaDataTitleOnWidget(presenter.getAccount().getAccountOwnerDirectoryMetaData().getPublicId(), ownerIdSB);
			}
			else {
				// enter placeholder text on ownerIdSB
				ownerIdSB.getElement().setPropertyString("placeholder", "<enter name or netid>");
			}
			// TODO: add a static text object to show person's name from the meta data pojo
			passwordLocationTB.setText(presenter.getAccount().getPasswordLocation());
			speedTypeTB.setText(presenter.getAccount().getSpeedType());
			// check CIMP status.  If we are a CIMP instance, we don't need to confirm speed type
			if (!isCimpInstance) {
				presenter.setSpeedChartStatusForKey(presenter.getAccount().getSpeedType(), speedTypeHTML, false);
			}
			
			// account extra meta-data
			AccountExtraMetaDataPojo aemd = presenter.getAccountExtraMetaData(); 
			if (presenter.isEdititingExtraMetaData()) {
				departmentTB.setText(aemd.getDepartmentName());
				piIdSB.setText(aemd.getPrincipalInvestigatorMetaData().getFirstName() + " " + aemd.getPrincipalInvestigatorMetaData().getLastName());
				presenter.setDirectoryMetaDataTitleOnWidget(aemd.getPrincipalInvestigatorMetaData().getPublicId(), piIdSB);
				if (aemd.getCliExperience() != null && 
					(aemd.getCliExperience().equalsIgnoreCase("yes") ||
					aemd.getCliExperience().equalsIgnoreCase("true"))) {
					
					cliExperienceCB.setValue(true);
				}
				else {
					cliExperienceCB.setValue(false);
				}
				primaryProjectNameTB.setText(aemd.getPrimaryProjectName());
				primaryProjectDataTypeTB.setText(aemd.getPrimaryProjectDataType());
				primaryProjectSoftwareTB.setText(aemd.getPrimaryProjectSoftware());
				primaryProjectDomainAreaTB.setText(aemd.getPrimaryProjectDomainArea());
				secondaryProjectNameTB.setText(aemd.getSecondaryProjectName());
				secondaryProjectDataTypeTB.setText(aemd.getSecondaryProjectDataType());
				secondaryProjectSoftwareTB.setText(aemd.getSecondaryProjectSoftware());
				secondaryProjectDomainAreaTB.setText(aemd.getSecondaryProjectDomainArea());
			}
			else {
				departmentTB.setText("");
				piIdSB.setText("");
				cliExperienceCB.setValue(false);
				primaryProjectNameTB.setText("");
				primaryProjectDataTypeTB.setText("");
				primaryProjectSoftwareTB.setText("");
				primaryProjectDomainAreaTB.setText("");
				secondaryProjectNameTB.setText("");
				secondaryProjectDataTypeTB.setText("");
				secondaryProjectSoftwareTB.setText("");
				secondaryProjectDomainAreaTB.setText("");
			}
		}
		
		registerHandlers();
		
		// populate associated emails if appropriate
		initializeEmailPanel();
		
		// populate properties panel
		initializeAccountPropertiesPanel();
		
		// populate admin net id fields if appropriate
		clearAdminTable();
	}
	
	private void registerHandlers() {
		directoryLookupSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					presenter.setDirectoryPerson(dp_suggestion.getDirectoryPerson());
					directoryLookupSB.setTitle(presenter.getDirectoryPerson().toString());
				}
			}
		});
		
		ownerIdSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					if (presenter.getAccount().getAccountOwnerDirectoryMetaData() == null) {
						presenter.getAccount().setAccountOwnerDirectoryMetaData(new DirectoryMetaDataPojo());
					}
					
					//TODO: set the ownerIdSB text to their name
					
					//set the PPID on AccountOwnerDirectoryMetaData.
					presenter.getAccount().getAccountOwnerDirectoryMetaData().
						setPublicId(dp_suggestion.getDirectoryPerson().getKey());
					
					// basically, we want to present their name but store their PPID
					
//					presenter.setDirectoryPerson(dp_suggestion.getDirectoryPerson());
//					directoryLookupSB.setTitle(presenter.getDirectoryPerson().toString());
				}
			}
		});
		
		piIdSB.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				DirectoryPersonSuggestion dp_suggestion = (DirectoryPersonSuggestion)event.getSelectedItem();
				if (dp_suggestion.getDirectoryPerson() != null) {
					if (presenter.getAccountExtraMetaData().getPrincipalInvestigatorMetaData() == null) {
						presenter.getAccountExtraMetaData().setPrincipalInvestigatorMetaData(new DirectoryMetaDataPojo());
					}
					
					//set the PPID on AccountOwnerDirectoryMetaData.
					presenter.getAccountExtraMetaData().getPrincipalInvestigatorMetaData().
						setPublicId(dp_suggestion.getDirectoryPerson().getKey());
				}
			}
		});
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	if (!editing) {
		        	accountIdTB.setFocus(true);
	        	}
	        	else {
	        		accountNameTB.setFocus(true);
	        	}
	        }
	    });
	}
	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}
	@Override
	public void setEmailTypeItems(List<String> emailTypes) {
		this.emailTypes = emailTypes;
		addEmailTypeLB.clear();
		if (emailTypes != null) {
			for (String emailType : emailTypes) {
				addEmailTypeLB.addItem(emailType, emailType);
			}
		}
	}
	@Override
	public void applyAWSAccountAdminMask() {
		GWT.log("Applying account admin mask...");
		okayButton.setEnabled(true);
		accountIdTB.setEnabled(false);
		accountNameTB.setEnabled(false);
		alternateNameTB.setEnabled(true);
		ownerIdSB.setEnabled(false);
		if (editing) {
			if (presenter.getAccount() != null) {
				if (userLoggedIn.getPublicId().equalsIgnoreCase(presenter.getAccount().getAccountOwnerDirectoryMetaData().getPublicId())) {
					ownerIdSB.setEnabled(true);
				}
			}
		}
		else {
			// it's a create
			ownerIdSB.setEnabled(true);
		}
		passwordLocationTB.setEnabled(false);
		speedTypeTB.setEnabled(true);
		addEmailTF.setEnabled(false);
		addEmailTypeLB.setEnabled(false);
		addEmailButton.setEnabled(false);
		directoryLookupSB.setEnabled(true);
		addAdminButton.setEnabled(true);
		complianceClassLB.setEnabled(false);
		billSummaryButton.setVisible(false);
		propertyKeyTF.setEnabled(false);
		propertyValueTF.setEnabled(false);
		addPropertyButton.setEnabled(false);
	}
	@Override
	public void applyAWSAccountAuditorMask() {
		GWT.log("Applying account auditor mask...");
		okayButton.setEnabled(false);
		accountIdTB.setEnabled(false);
		accountNameTB.setEnabled(false);
		alternateNameTB.setEnabled(false);
		ownerIdSB.setEnabled(false);
		passwordLocationTB.setEnabled(false);
		speedTypeTB.setEnabled(false);
		addEmailTF.setEnabled(false);
		addEmailTypeLB.setEnabled(false);
		addEmailButton.setEnabled(false);
		directoryLookupSB.setEnabled(false);
		addAdminButton.setEnabled(false);
		complianceClassLB.setEnabled(false);
		billSummaryButton.setVisible(false);
		propertyKeyTF.setEnabled(false);
		propertyValueTF.setEnabled(false);
		addPropertyButton.setEnabled(false);
	}
	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}
	@Override
	public void setAwsAccountsURL(String awsAccountsURL) {
//		String acctInfo = accountInfoHTML.getHTML();
//		accountInfoHTML.setHTML(acctInfo.replaceAll("AWS_ACCOUNTS_URL", awsAccountsURL));
	}
	@Override
	public void setAwsBillingManagementURL(String awsBillingManagementURL) {
//		String acctInfo = accountInfoHTML.getHTML();
//		accountInfoHTML.setHTML(acctInfo.replaceAll("AWS_BILLING_MANAGEMENT_URL", awsBillingManagementURL));
	}
	@Override
	public void setSpeedTypeStatus(String status) {
		speedTypeHTML.setHTML(status);
	}
	@Override
	public void setSpeedTypeColor(String color) {
		speedTypeHTML.getElement().getStyle().setColor(color);
	}
	@Override
	public Widget getSpeedTypeWidget() {
		return speedTypeTB;
	}
	@Override
	public List<Widget> getMissingRequiredFields() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		AccountPojo acct = presenter.getAccount(); 
		if (acct.getAccountId() == null || acct.getAccountId().length() == 0) {
			fields.add(accountIdTB);
		}
		if (acct.getAccountName() == null || acct.getAccountName().length() == 0) {
			fields.add(accountNameTB);
		}
		if (acct.getPasswordLocation() == null || acct.getPasswordLocation().length() == 0) {
			fields.add(passwordLocationTB);
		}
		if (acct.getEmailList() == null|| acct.getEmailList().size() == 0) {
			fields.add(addEmailTF);
		}
		if (acct.getComplianceClass() == null || acct.getComplianceClass().length() == 0) {
			fields.add(complianceClassLB);
		}
		
//		fields.addAll(getMissingRequiredExtraMetaDataFields());
		
		return fields;
	}

	@Override
	public List<Widget> getMissingRequiredExtraMetaDataFields() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		// AccountExtraMetaData
		// if we're editing an existing aemd it's fairly simple (normal processing)
		// if we're not editing an existing aemd, we'll need to determine if 
		// they've started to add aemd data to the account.  if they've started
		// adding stuff, then the required field logic will apply 
		AccountExtraMetaDataPojo aemd = presenter.getAccountExtraMetaData();
		if (presenter.isEdititingExtraMetaData() || hasStartedExtraMetaData()) {
			if (aemd.getUnitOrSchool() == null || aemd.getUnitOrSchool().length() == 0) {
				fields.add(unitOrSchoolLB);
			}
			if (aemd.getDepartmentName() == null || aemd.getDepartmentName().length() == 0) {
				fields.add(departmentTB);
			}
			if (aemd.getLevelOfAwsExperience() == null || aemd.getLevelOfAwsExperience().length() == 0) {
				fields.add(awsExperienceLevelLB);
			}
			if (aemd.getPrimaryProjectName() == null || aemd.getPrimaryProjectName().length() == 0) {
				fields.add(primaryProjectNameTB);
			}
			if (aemd.getPrimaryProjectDataType() == null || aemd.getPrimaryProjectDataType().length() == 0) {
				fields.add(primaryProjectDataTypeTB);
			}
			if (aemd.getPrimaryProjectSoftware() == null || aemd.getPrimaryProjectSoftware().length() == 0) {
				fields.add(primaryProjectSoftwareTB);
			}
			if (aemd.getPrimaryProjectDomainArea() == null || aemd.getPrimaryProjectDomainArea().length() == 0) {
				fields.add(primaryProjectDomainAreaTB);
			}
			if (aemd.getPrimaryProjectPlatform().size() == 0) {
				fields.add(primaryProjectPlatformsLB);
			}
			if (aemd.getPrimaryProjectPurpose().size() == 0) {
				fields.add(primaryProjectPurposeLB);
			}
			// for secondary, we'll need to be a little more complex
			if (hasStartedSecondaryProjectData()) {
				if (aemd.getSecondaryProjectName() == null || aemd.getSecondaryProjectName().length() == 0) {
					fields.add(secondaryProjectNameTB);
				}
				if (aemd.getSecondaryProjectDataType() == null || aemd.getSecondaryProjectDataType().length() == 0) {
					fields.add(secondaryProjectDataTypeTB);
				}
				if (aemd.getSecondaryProjectSoftware() == null || aemd.getSecondaryProjectSoftware().length() == 0) {
					fields.add(secondaryProjectSoftwareTB);
				}
				if (aemd.getSecondaryProjectDomainArea() == null || aemd.getSecondaryProjectDomainArea().length() == 0) {
					fields.add(secondaryProjectDomainAreaTB);
				}
				if (aemd.getSecondaryProjectPlatform().size() == 0) {
					fields.add(secondaryProjectPlatformsLB);
				}
				if (aemd.getSecondaryProjectPurpose().size() == 0) {
					fields.add(secondaryProjectPurposeLB);
				}
			}
		}
		return fields;
	}
	
	@Override
	public void resetFieldStyles() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		fields.add(accountIdTB);
		fields.add(accountNameTB);
		fields.add(complianceClassLB);
		fields.add(passwordLocationTB);
		fields.add(addEmailTF);
		
		this.resetFieldStyles(fields);
//		resetExtraMetaDataFieldStyles();
	}
	@Override
	public void resetExtraMetaDataFieldStyles() {
		List<Widget> fields = new java.util.ArrayList<Widget>();
		// AccountExtraMetaData
		fields.add(unitOrSchoolLB);
		fields.add(departmentTB);
		fields.add(awsExperienceLevelLB);
		fields.add(primaryProjectNameTB);
		fields.add(primaryProjectDataTypeTB);
		fields.add(primaryProjectSoftwareTB);
		fields.add(primaryProjectDomainAreaTB);
		fields.add(primaryProjectPlatformsLB);
		fields.add(primaryProjectPurposeLB);
		fields.add(secondaryProjectNameTB);
		fields.add(secondaryProjectDataTypeTB);
		fields.add(secondaryProjectSoftwareTB);
		fields.add(secondaryProjectDomainAreaTB);
		fields.add(secondaryProjectPlatformsLB);
		fields.add(secondaryProjectPurposeLB);
		
		this.resetFieldStyles(fields);
	}
	
	public boolean hasStartedExtraMetaData() {
		if (unitOrSchoolLB.getSelectedValue() != null && unitOrSchoolLB.getSelectedValue().length() > 0) {
			return true;
		}
		if (departmentTB.getText() != null && departmentTB.getText().length() > 0) {
			return true;
		}
		if (cliExperienceCB.getValue()) {
			return true;
		}
		if (awsExperienceLevelLB.getSelectedValue() != null && awsExperienceLevelLB.getSelectedValue().length() > 0) {
			return true;
		}
		if (primaryProjectNameTB.getText() != null && primaryProjectNameTB.getText().length() > 0) {
			return true;
		}
		if (primaryProjectDataTypeTB.getText() != null && primaryProjectDataTypeTB.getText().length() > 0) {
			return true;
		}
		if (primaryProjectSoftwareTB.getText() != null && primaryProjectSoftwareTB.getText().length() > 0) {
			return true;
		}
		if (primaryProjectDomainAreaTB.getText() != null && primaryProjectDomainAreaTB.getText().length() > 0) {
			return true;
		}
		for (int i=0; i<primaryProjectPlatformsLB.getItemCount(); i++) {
			if (primaryProjectPlatformsLB.isItemSelected(i)) {
				return true;
			}
		}
		for (int i=0; i<primaryProjectPurposeLB.getItemCount(); i++) {
			if (primaryProjectPurposeLB.isItemSelected(i)) {
				return true;
			}
		}

		// secondary
		return hasStartedSecondaryProjectData();
	}
	public boolean hasStartedSecondaryProjectData() {
		if (secondaryProjectNameTB.getText() != null && secondaryProjectNameTB.getText().length() > 0) {
			return true;
		}
		if (secondaryProjectDataTypeTB.getText() != null && secondaryProjectDataTypeTB.getText().length() > 0) {
			return true;
		}
		if (secondaryProjectSoftwareTB.getText() != null && secondaryProjectSoftwareTB.getText().length() > 0) {
			return true;
		}
		if (secondaryProjectDomainAreaTB.getText() != null && secondaryProjectDomainAreaTB.getText().length() > 0) {
			return true;
		}
		for (int i=0; i<secondaryProjectPlatformsLB.getItemCount(); i++) {
			if (secondaryProjectPlatformsLB.isItemSelected(i)) {
				return true;
			}
		}
		for (int i=0; i<secondaryProjectPurposeLB.getItemCount(); i++) {
			if (secondaryProjectPurposeLB.isItemSelected(i)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		return okayButton;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		return cancelButton;
	}
	@Override
	public void setSpeedTypeConfirmed(boolean confirmed) {
		this.speedTypeConfirmed = confirmed;
	}
	@Override
	public boolean isSpeedTypeConfirmed() {
		return this.speedTypeConfirmed;
	}
	@Override
	public void setRoleAssignmentSummaries(List<RoleAssignmentSummaryPojo> summaries) {
		
		
	}
	@Override
	public void setComplianceClassItems(List<String> complianceClassTypes) {
		this.complianceClassTypes = complianceClassTypes;
		complianceClassLB.clear();
		complianceClassLB.addItem("-- Select --", "");
		if (complianceClassLB != null) {
			int i=1;
			for (String type : complianceClassTypes) {
				complianceClassLB.addItem(type, type);
				if (presenter.getAccount() != null) {
					if (presenter.getAccount().getComplianceClass() != null) {
						if (presenter.getAccount().getComplianceClass().equals(type)) {
							complianceClassLB.setSelectedIndex(i);
						}
					}
				}
				i++;
			}
		}
	}
	@Override
	public void showPleaseWaitDialog(String pleaseWaitHTML) {
		adminPleaseWaitDialog = new PopupPanel(true);

		VerticalPanel vp = new VerticalPanel();
		Image img = new Image();
		img.setUrl("images/ajax-loader.gif");
		vp.add(img);
		HTML h = new HTML(pleaseWaitHTML);
		vp.add(h);
		vp.setCellHorizontalAlignment(img, HasHorizontalAlignment.ALIGN_CENTER);
		vp.setCellHorizontalAlignment(h, HasHorizontalAlignment.ALIGN_CENTER);

		adminPleaseWaitDialog.setWidget(vp);
		adminPleaseWaitDialog.center();
		adminPleaseWaitDialog.show();
	}
	@Override
	public void hidePleaseWaitDialog() {
		super.hidePleaseWaitDialog();
		if (adminPleaseWaitDialog != null) {
			adminPleaseWaitDialog.hide();
		}
	}
	@Override
	public void applyCentralAdminMask() {
		GWT.log("Applying central admin mask...");
		okayButton.setEnabled(true);
		if (!editing) {
			// account id is immutable when/if the account is being edited
			// so, only enable the account id field if it's a new account
			accountIdTB.setEnabled(true);
		}
		accountNameTB.setEnabled(true);
		alternateNameTB.setEnabled(true);
		ownerIdSB.setEnabled(true);
		passwordLocationTB.setEnabled(true);
		speedTypeTB.setEnabled(true);
		addEmailTF.setEnabled(true);
		addEmailTypeLB.setEnabled(true);
		addEmailButton.setEnabled(true);
		directoryLookupSB.setEnabled(true);
		addAdminButton.setEnabled(true);
		complianceClassLB.setEnabled(true);
		billSummaryButton.setVisible(true);
		propertyKeyTF.setEnabled(true);
		propertyValueTF.setEnabled(true);
		if (userLoggedIn.isCentralAdminManager()) {
			addPropertyButton.setEnabled(true);
		}
		else {
			addPropertyButton.setEnabled(false);
		}
	}
	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}
	@Override
	public void vpcpPromptCancel() {
		
		
	}
	@Override
	public void vpcpConfirmOkay() {
		
		
	}
	@Override
	public void vpcpConfirmCancel() {
		
		
	}
	@Override
	public void enableAdminMaintenance() {
		GWT.log("enabling admin maintenance...");
		addAdminButton.setEnabled(true);
		directoryLookupSB.setEnabled(true);
	}
	
	@Override
	public void disableAdminMaintenance() {
		GWT.log("disabling admin maintenance...");
		addAdminButton.setEnabled(false);
		directoryLookupSB.setEnabled(false);
	}
	
	@Override
	public void showWaitForNotificationsDialog(String message) {
		waitForNotificationsDialog = new PopupPanel(true);

		VerticalPanel vp = new VerticalPanel();
		Image img = new Image();
		img.setUrl("images/ajax-loader.gif");
		vp.add(img);
		HTML h = new HTML(message);
		vp.add(h);
		vp.setCellHorizontalAlignment(img, HasHorizontalAlignment.ALIGN_CENTER);
		vp.setCellHorizontalAlignment(h, HasHorizontalAlignment.ALIGN_CENTER);

		waitForNotificationsDialog.setWidget(vp);
		waitForNotificationsDialog.center();
		waitForNotificationsDialog.show();
	}
	@Override
	public void hidWaitForNotificationsDialog() {
		if (waitForNotificationsDialog != null) {
			waitForNotificationsDialog.hide();
		}
	}
	@Override
	public void setAccountNotifications(List<AccountNotificationPojo> pojos) {
		this.pojoList = pojos;
		this.initializeListTable();
		listPager.setDisplay(listTable);
	}
	@Override
	public void removeAccountNotificationFromView(AccountNotificationPojo pojo) {
		dataProvider.getList().remove(pojo);
	}
	
	private Widget initializeListTable() {
		GWT.log("initializing account notification list table...");
		listTable.setTableLayoutFixed(false);
		listTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		
		// set range to display
		listTable.setVisibleRange(0, 5);
		
		// create dataprovider
		dataProvider = new ListDataProvider<AccountNotificationPojo>();
		dataProvider.addDataDisplay(listTable);
		dataProvider.getList().clear();
		dataProvider.getList().addAll(this.pojoList);
		
		selectionModel = 
	    	new MultiSelectionModel<AccountNotificationPojo>(AccountNotificationPojo.KEY_PROVIDER);
		listTable.setSelectionModel(selectionModel);
	    
	    selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	    	@Override
	    	public void onSelectionChange(SelectionChangeEvent event) {
//	    		NotificationPojo m = selectionModel.getSelectedObject();
//	    		GWT.log("Selected service is: " + m.getNotificationId());
	    	}
	    });

	    ListHandler<AccountNotificationPojo> sortHandler = 
	    	new ListHandler<AccountNotificationPojo>(dataProvider.getList());
	    listTable.addColumnSortHandler(sortHandler);

	    if (listTable.getColumnCount() == 0) {
		    initListTableColumns(sortHandler);
	    }
		
		return listTable;
	}
	private void initListTableColumns(ListHandler<AccountNotificationPojo> sortHandler) {
		GWT.log("initializing account notification list table columns...");
		GWT.log("there are " + sortHandler.getList().size() + " user notifications in the list");

		// Notification id column
		Column<AccountNotificationPojo, String> typeColumn = 
				new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(AccountNotificationPojo object) {
				return object.getType();
			}
		};
		typeColumn.setSortable(true);
		sortHandler.setComparator(typeColumn, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				return o1.getType().compareTo(o2.getType());
			}
		});
		typeColumn.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
				ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT_NOTIFICATION, presenter.getAccount(), object);
	    	}
	    });
		typeColumn.setCellStyleNames("tableAnchor");
		listTable.addColumn(typeColumn, "Type");
		
		Column<AccountNotificationPojo, String> priorityColumn = 
				new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(AccountNotificationPojo object) {
				return object.getPriority();
			}
		};
		priorityColumn.setSortable(true);
		sortHandler.setComparator(priorityColumn, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				return o1.getPriority().compareTo(o2.getPriority());
			}
		});
	    priorityColumn.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
				ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT_NOTIFICATION, presenter.getAccount(), object);
	    	}
	    });
	    priorityColumn.setCellStyleNames("tableAnchor");
		listTable.addColumn(priorityColumn, "Priority");
		
		Column<AccountNotificationPojo, String> subjectColumn = 
				new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(AccountNotificationPojo object) {
				return object.getSubject();
			}
	    };
	    subjectColumn.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
	    		GWT.log("value updater for subject column");
				ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT_NOTIFICATION, presenter.getAccount(), object);
	    	}
	    });
		subjectColumn.setSortable(true);
		sortHandler.setComparator(subjectColumn, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				return o1.getSubject().compareTo(o2.getSubject());
			}
		});
		subjectColumn.setCellStyleNames("tableAnchor");
		listTable.addColumn(subjectColumn, "Subject");

		// Reference id column
		Column<AccountNotificationPojo, String> referenceId = 
			new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {
			
			@Override
			public String getValue(AccountNotificationPojo object) {
				return object.getReferenceid();
			}
		};
		referenceId.setSortable(true);
		sortHandler.setComparator(referenceId, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				return o1.getReferenceid().compareTo(o2.getReferenceid());
			}
		});
		referenceId.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
	    		if (object != null && object.getReferenceid() != null && object.getReferenceid().length() > 0) {
		    		presenter.showSrdForAccountNotification(object);
	    		}
	    		else {
	    			showMessageToUser("Notification doesn't have a reference id to link to.");
	    		}
	    	}
	    });
		referenceId.setCellStyleNames("tableAnchor");
		listTable.addColumn(referenceId, "Reference ID");

		Column<AccountNotificationPojo, String> createTime = 
				new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(AccountNotificationPojo object) {
				Date createTime = object.getCreateTime();
				return createTime != null ? dateFormat.format(createTime) : "Unknown";
			}
		};
		createTime.setSortable(true);
		sortHandler.setComparator(createTime, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				GWT.log("account notification create time sort handler...");
				Date c1 = o1.getCreateTime();
				Date c2 = o2.getCreateTime();
				if (c1 == null || c2 == null) {
					return 0;
				}
				return c1.compareTo(c2);
			}
		});
	    createTime.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
				ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT_NOTIFICATION, presenter.getAccount(), object);
	    	}
	    });
	    createTime.setCellStyleNames("tableAnchor");
		listTable.addColumn(createTime, "Create Time");
		
		Column<AccountNotificationPojo, String> updateTime = 
				new Column<AccountNotificationPojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(AccountNotificationPojo object) {
				Date updateTime = object.getUpdateTime();
				return updateTime != null ? dateFormat.format(updateTime) : "Unknown";
			}
		};
		updateTime.setSortable(true);
		sortHandler.setComparator(updateTime, new Comparator<AccountNotificationPojo>() {
			public int compare(AccountNotificationPojo o1, AccountNotificationPojo o2) {
				Date c1 = o1.getUpdateTime();
				Date c2 = o2.getUpdateTime();
				if (c1 == null || c2 == null) {
					return 0;
				}
				return c1.compareTo(c2);
			}
		});
	    updateTime.setFieldUpdater(new FieldUpdater<AccountNotificationPojo, String>() {
	    	@Override
	    	public void update(int index, AccountNotificationPojo object, String value) {
				ActionEvent.fire(presenter.getEventBus(), ActionNames.MAINTAIN_ACCOUNT_NOTIFICATION, presenter.getAccount(), object);
	    	}
	    });
	    updateTime.setCellStyleNames("tableAnchor");
		listTable.addColumn(updateTime, "Update Time");
	}
	
	private Widget initializeCustomRoleListTable() {
		GWT.log("initializing custom role list table...");
		customRoleListTable.setTableLayoutFixed(false);
		customRoleListTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		
		// set range to display
		customRoleListTable.setVisibleRange(0, 5);
		
		// create dataprovider
		GWT.log("[initializeCustomRoleListTable] custom role list size "
				+ "from presenter is: " + presenter.getExistingCustomRoles().size());
		customRoleDataProvider = new ListDataProvider<CustomRolePojo>();
		customRoleDataProvider.addDataDisplay(customRoleListTable);
		customRoleDataProvider.getList().clear();
		customRoleDataProvider.getList().addAll(this.existingCustomRoles);
		
		customRoleSelectionModel = 
	    	new SingleSelectionModel<CustomRolePojo>(CustomRolePojo.KEY_PROVIDER);
		customRoleListTable.setSelectionModel(customRoleSelectionModel);
		
		customRoleSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	    	@Override
	    	public void onSelectionChange(SelectionChangeEvent event) {
//	    		NotificationPojo m = selectionModel.getSelectedObject();
//	    		GWT.log("Selected service is: " + m.getNotificationId());
	    	}
	    });

	    ListHandler<CustomRolePojo> sortHandler = 
	    	new ListHandler<CustomRolePojo>(customRoleDataProvider.getList());
	    listTable.addColumnSortHandler(sortHandler);

	    if (listTable.getColumnCount() == 0) {
		    initCustomRoleListTableColumns(sortHandler);
	    }
		
		return listTable;
	}
	
	private void initCustomRoleListTableColumns(ListHandler<CustomRolePojo> sortHandler) {
		GWT.log("[initCustomRoleListTableColumns] initializing custom role list table COLUMNS...");

	    Column<CustomRolePojo, Boolean> checkColumn = new Column<CustomRolePojo, Boolean>(
		        new CheckboxCell(true, false)) {
		      @Override
		      public Boolean getValue(CustomRolePojo object) {
		        // Get the value from the selection model.
		        return customRoleSelectionModel.isSelected(object);
		      }
	    };
	    customRoleListTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
	    customRoleListTable.setColumnWidth(checkColumn, 40, Unit.PX);

		// role name column
		Column<CustomRolePojo, String> nameColumn = 
				new Column<CustomRolePojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(CustomRolePojo object) {
				return object.getRoleName();
			}
		};
		nameColumn.setSortable(true);
		sortHandler.setComparator(nameColumn, new Comparator<CustomRolePojo>() {
			public int compare(CustomRolePojo o1, CustomRolePojo o2) {
				return o1.getRoleName().compareTo(o2.getRoleName());
			}
		});
		nameColumn.setCellStyleNames("tableAnchor");
		customRoleListTable.addColumn(nameColumn, "Role Name");
		
		Column<CustomRolePojo, String> createTime = 
				new Column<CustomRolePojo, String> (new ClickableTextCell()) {

			@Override
			public String getValue(CustomRolePojo object) {
				Date createTime = object.getCreateTime();
				return createTime != null ? dateFormat.format(createTime) : "Unknown";
			}
		};
		createTime.setSortable(true);
		sortHandler.setComparator(createTime, new Comparator<CustomRolePojo>() {
			public int compare(CustomRolePojo o1, CustomRolePojo o2) {
				GWT.log("account notification create time sort handler...");
				Date c1 = o1.getCreateTime();
				Date c2 = o2.getCreateTime();
				if (c1 == null || c2 == null) {
					return 0;
				}
				return c1.compareTo(c2);
			}
		});
	    createTime.setCellStyleNames("tableAnchor");
		customRoleListTable.addColumn(createTime, "Create Time");
	}
	
	
	@Override
	public void disableButtons() {
		
		
	}
	@Override
	public void enableButtons() {
		
		
	}
	@Override
	public void showFilteredStatus() {
		filteredHTML.setVisible(true);
	}
	@Override
	public void hideFilteredStatus() {
		filteredHTML.setVisible(false);
	}
	@Override
	public void applyNetworkAdminMask() {
		
		
	}

	@Override
	public void setCimpInstance(boolean isCimpInstance) {
		this.isCimpInstance = isCimpInstance;
	}

	@Override
	public void setFinancialAccountFieldLabel(String label) {
		speedTypeLabel.setText(label);
	}

//	@Override
//	public void initializeCustomRoleTable() {
//		// initialized the custom roles table
//		initializeCustomRoleListTable();
//		customRoleListPager.setDisplay(customRoleListTable);
//
//	}

	@UiHandler("generateRoleButton")
	void generateRoleButtonClicked(ClickEvent e) {
		actionsPopup.hide();
		ActionEvent.fire(presenter.getEventBus(), ActionNames.GENERATE_ROLE_PROVISIONING, presenter.getAccount());
	}
	
	@UiHandler("actionsButton")
	void actionsButtonClicked(ClickEvent e) {
		actionsPopup.clear();
	    actionsPopup.setAutoHideEnabled(true);
	    actionsPopup.setAnimationEnabled(true);
	    actionsPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
	    Grid grid = new Grid(2, 1);
	    grid.setCellSpacing(8);
	    actionsPopup.add(grid);
	    
	    String anchorText = "Provision Custom Role";

		Anchor provisionAnchor = new Anchor(anchorText);
		provisionAnchor.addStyleName("productAnchor");
		provisionAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		provisionAnchor.setTitle("Generate a custom role");
		provisionAnchor.ensureDebugId(anchorText);
		provisionAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.GENERATE_ROLE_PROVISIONING, presenter.getAccount());
			}
		});
		grid.setWidget(0, 0, provisionAnchor);

		anchorText = "De-Provision Custom Role";
		Anchor deprovisionAnchor = new Anchor(anchorText);
		deprovisionAnchor.addStyleName("productAnchor");
		deprovisionAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		deprovisionAnchor.setTitle("De-provision the selected custom role");
		deprovisionAnchor.ensureDebugId(anchorText);
		deprovisionAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				CustomRolePojo m = customRoleSelectionModel.getSelectedObject();
				if (m != null) {
					presenter.getRoleDeprovisioningRequisition().setAccountId(m.getAccountId());
					presenter.getRoleDeprovisioningRequisition().setRequestorId(userLoggedIn.getPublicId());
					presenter.getRoleDeprovisioningRequisition().setCustomRoleName(m.getRoleName());
					presenter.deprovisionCustomRole();
				}
				else {
					showMessageToUser("Please select an item from the list");
				}
			}
		});
		grid.setWidget(1, 0, deprovisionAnchor);

		actionsPopup.showRelativeTo(actionsButton);
	}

	@Override
	public void addSpeedTypeStyle(String styleName) {
		speedTypeHTML.setStyleName(styleName);
	}

	@Override
	public void showNoResultsMessage() {
		noResultsHTML.setVisible(true);
	}

	@Override
	public void hideNoResultsMessage() {
		noResultsHTML.setVisible(false);
	}

	@Override
	public void clearAdminTable() {
		adminTable.clear();
		adminRowNum = 0;
		adminColumnNum = 0;
		removeButtonColumnNum = 1;
	}

	@Override
	public void setExistingCustomRoles(List<CustomRolePojo> customRoles) {
		GWT.log("view Setting existing custom roles.");
		this.existingCustomRoles = customRoles;
		this.initializeCustomRoleListTable();
	    customRoleListPager.setDisplay(customRoleListTable);
	}

	@Override
	public void setUnitOrSchoolItems(List<String> units) {
		this.unitsAndSchools = units;
		unitOrSchoolLB.clear();
		unitOrSchoolLB.addItem("-- Select --", "");
		if (!units.contains(Constants.OTHER)) {
			units.add(Constants.OTHER);
		}
		int i=1;
		for (String unitItem : units) {
			unitOrSchoolLB.addItem(unitItem, unitItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					String unitOrSchool = presenter.getAccountExtraMetaData().getUnitOrSchool();
					if (unitOrSchool != null && unitOrSchool.equalsIgnoreCase(unitItem)) {
						unitOrSchoolLB.setSelectedIndex(i);
					}
				}
			}
			i++;
		}
	}
	// handler for when/if they select "Other..." from the list
	@UiHandler ("unitOrSchoolLB")
	void unitOrSchoolSelected(ChangeEvent e) {
		String unitOrSchool = unitOrSchoolLB.getSelectedValue();
		if (unitOrSchool != null && unitOrSchool.equalsIgnoreCase(Constants.OTHER)) {
			String otherUnitOrSchool = Window.prompt("Enter a Unit or School", "<Unit or School>");
			if (otherUnitOrSchool != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().setUnitOrSchool(otherUnitOrSchool);
				this.unitsAndSchools.add(otherUnitOrSchool);
				this.setUnitOrSchoolItems(this.unitsAndSchools);
			}
		}
	}

	@Override
	public void setPrimaryPurposeItems(List<String> purposes) {
		this.primaryProjectPurposes = purposes;
		primaryProjectPurposeLB.clear();
		int i=0;
		for (String purposeItem : purposes) {
			primaryProjectPurposeLB.addItem(purposeItem, purposeItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					for (String purpose : presenter.getAccountExtraMetaData().getPrimaryProjectPurpose()) {
						if (purpose.equalsIgnoreCase(purposeItem)) {
							primaryProjectPurposeLB.setItemSelected(i, true);
						}
					}
				}
			}
			i++;
		}
		primaryProjectPurposeLB.addItem(Constants.OTHER, Constants.OTHER);
	}

	// handler for when/if they select "Other..." from the list
	@UiHandler ("primaryProjectPurposeLB")
	void primaryPurposeSelected(ChangeEvent e) {
		boolean isOtherSelected=false;
		loop: for (int i=0; i<primaryProjectPurposeLB.getItemCount(); i++) {
			if (primaryProjectPurposeLB.isItemSelected(i)) {
				String selected = primaryProjectPurposeLB.getValue(i);
				if (selected != null && selected.equalsIgnoreCase(Constants.OTHER)) {
					isOtherSelected=true;
					break loop;
				}
			}
		}
		if (isOtherSelected) {
			String enteredValue = Window.prompt("Enter a Purpose", "<Purpose>");
			if (enteredValue != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().getPrimaryProjectPurpose().add(enteredValue);
				this.primaryProjectPurposes.add(enteredValue);
				this.setPrimaryPurposeItems(this.primaryProjectPurposes);
			}
		}
	}

	@Override
	public void setSecondaryPurposeItems(List<String> purposes) {
		this.secondaryProjectPurposes = purposes;
		secondaryProjectPurposeLB.clear();
		int i=0;
		for (String purposeItem : purposes) {
			secondaryProjectPurposeLB.addItem(purposeItem, purposeItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					for (String purpose : presenter.getAccountExtraMetaData().getSecondaryProjectPurpose()) {
						if (purpose.equalsIgnoreCase(purposeItem)) {
							secondaryProjectPurposeLB.setItemSelected(i, true);
						}
					}
				}
			}
			i++;
		}
		secondaryProjectPurposeLB.addItem(Constants.OTHER, Constants.OTHER);
	}
	@UiHandler ("secondaryProjectPurposeLB")
	void secondaryPurposeSelected(ChangeEvent e) {
		boolean isOtherSelected=false;
		loop: for (int i=0; i<secondaryProjectPurposeLB.getItemCount(); i++) {
			if (secondaryProjectPurposeLB.isItemSelected(i)) {
				String selected = secondaryProjectPurposeLB.getValue(i);
				if (selected != null && selected.equalsIgnoreCase(Constants.OTHER)) {
					isOtherSelected=true;
					break loop;
				}
			}
		}
		if (isOtherSelected) {
			String enteredValue = Window.prompt("Enter a Purpose", "<Purpose>");
			if (enteredValue != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().getSecondaryProjectPurpose().add(enteredValue);
				this.secondaryProjectPurposes.add(enteredValue);
				this.setSecondaryPurposeItems(this.secondaryProjectPurposes);
			}
		}
	}

	@Override
	public void setPrimaryPlatformItems(List<String> platforms) {
		this.primaryProjectPlatforms = platforms;
		primaryProjectPlatformsLB.clear();
		int i=0;
		for (String platformItem : platforms) {
			primaryProjectPlatformsLB.addItem(platformItem, platformItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					for (String platform : presenter.getAccountExtraMetaData().getPrimaryProjectPlatform()) {
						if (platform.equalsIgnoreCase(platformItem)) {
							primaryProjectPlatformsLB.setItemSelected(i, true);
						}
					}
				}
			}
			i++;
		}
		primaryProjectPlatformsLB.addItem(Constants.OTHER, Constants.OTHER);
	}
	@UiHandler ("primaryProjectPlatformsLB")
	void primaryPlatformSelected(ChangeEvent e) {
		boolean isOtherSelected=false;
		loop: for (int i=0; i<primaryProjectPlatformsLB.getItemCount(); i++) {
			if (primaryProjectPlatformsLB.isItemSelected(i)) {
				String selected = primaryProjectPlatformsLB.getValue(i);
				if (selected != null && selected.equalsIgnoreCase(Constants.OTHER)) {
					isOtherSelected=true;
					break loop;
				}
			}
		}
		if (isOtherSelected) {
			String enteredValue = Window.prompt("Enter a Platform", "<Platform>");
			if (enteredValue != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().getPrimaryProjectPlatform().add(enteredValue);
				this.primaryProjectPlatforms.add(enteredValue);
				this.setPrimaryPlatformItems(this.primaryProjectPlatforms);
			}
		}
	}

	@Override
	public void setSecondaryPlatformItems(List<String> platforms) {
		this.secondaryProjectPlatforms = platforms;
		secondaryProjectPlatformsLB.clear();
		int i=0;
		for (String platformItem : platforms) {
			secondaryProjectPlatformsLB.addItem(platformItem, platformItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					for (String platform : presenter.getAccountExtraMetaData().getSecondaryProjectPlatform()) {
						if (platform.equalsIgnoreCase(platformItem)) {
							secondaryProjectPlatformsLB.setItemSelected(i, true);
						}
					}
				}
			}
			i++;
		}
		secondaryProjectPlatformsLB.addItem(Constants.OTHER, Constants.OTHER);
	}
	@UiHandler ("secondaryProjectPlatformsLB")
	void secondaryPlatformSelected(ChangeEvent e) {
		boolean isOtherSelected=false;
		loop: for (int i=0; i<secondaryProjectPlatformsLB.getItemCount(); i++) {
			if (secondaryProjectPlatformsLB.isItemSelected(i)) {
				String selected = secondaryProjectPlatformsLB.getValue(i);
				if (selected != null && selected.equalsIgnoreCase(Constants.OTHER)) {
					isOtherSelected=true;
					break loop;
				}
			}
		}
		if (isOtherSelected) {
			String enteredValue = Window.prompt("Enter a Platform", "<Platform>");
			if (enteredValue != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().getSecondaryProjectPlatform().add(enteredValue);
				this.secondaryProjectPlatforms.add(enteredValue);
				this.setSecondaryPlatformItems(this.secondaryProjectPlatforms);
			}
		}
	}

	@Override
	public void setAwsExperienceLevelItems(PropertiesPojo levels) {
		awsExperienceLevels = levels;
		awsExperienceLevelLB.clear();
		awsExperienceLevelLB.addItem("-- Select --", "");
		
		int i=1;
		Iterator<String> keys = levels.getPropertyMap().keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			String value = levels.getProperty(key);
			awsExperienceLevelLB.addItem(value, key);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					String awsLevel = presenter.getAccountExtraMetaData().getLevelOfAwsExperience();
					if (awsLevel != null && awsLevel.equalsIgnoreCase(key)) {
						awsExperienceLevelLB.setSelectedIndex(i);
					}
				}
			}
			i++;
		}
	}

	@Override
	public void setReferrerItems(List<String> items) {
		this.referrers = items;
		referrerLB.clear();
		referrerLB.addItem("-- Select --", "");
		if (!referrers.contains(Constants.OTHER)) {
			referrers.add(Constants.OTHER);
		}
		int i=1;
		for (String referrerItem : items) {
			referrerLB.addItem(referrerItem, referrerItem);
			if (presenter.getAccount() != null) {
				if (presenter.getAccountExtraMetaData() != null) {
					String referrer = presenter.getAccountExtraMetaData().getReferrerInfo();
					if (referrer != null && referrer.equalsIgnoreCase(referrerItem)) {
						referrerLB.setSelectedIndex(i);
					}
				}
			}
			i++;
		}
	}
	// handler for when/if they select "Other..." from the list
	@UiHandler ("referrerLB")
	void referrerSelected(ChangeEvent e) {
		String referrer = referrerLB.getSelectedValue();
		if (referrer != null && referrer.equalsIgnoreCase(Constants.OTHER)) {
			String otherReferrer = Window.prompt("How did you hear about AWS at Emory?", "<referrer>");
			if (otherReferrer != null) {
				// add the value they entered to the list and to the
				// account extra meta data object for this account
				presenter.getAccountExtraMetaData().setUnitOrSchool(otherReferrer);
				this.referrers.add(otherReferrer);
				this.setReferrerItems(this.referrers);
			}
		}
	}
}
