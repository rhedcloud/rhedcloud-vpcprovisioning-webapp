package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ListCentralAdminView;
import edu.emory.oit.vpcprovisioning.presenter.home.HomeView;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopListCentralAdmin extends ViewImplBase implements ListCentralAdminView {
	Presenter presenter;
	private ListDataProvider<RoleAssignmentSummaryPojo> dataProvider = new ListDataProvider<RoleAssignmentSummaryPojo>();
	private SingleSelectionModel<RoleAssignmentSummaryPojo> selectionModel;
	List<RoleAssignmentSummaryPojo> centralAdminList = new java.util.ArrayList<RoleAssignmentSummaryPojo>();
	UserAccountPojo userLoggedIn;
    PopupPanel actionsPopup = new PopupPanel(true);

	/*** FIELDS ***/
	@UiField(provided=true) SimplePager centralAdminListPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField Button actionsButton;
	@UiField(provided=true) CellTable<RoleAssignmentSummaryPojo> centralAdminListTable = new CellTable<RoleAssignmentSummaryPojo>(15, (CellTable.Resources)GWT.create(MyCellTableResources.class));
	@UiField VerticalPanel centralAdminListPanel;
	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField HTML introBodyHTML;
	@UiField PushButton refreshButton;
	@UiField HTML iicStatusHTML;

	@UiHandler("refreshButton")
	void refreshButtonClicked(ClickEvent e) {
		presenter.refreshList(userLoggedIn);
		presenter.refreshIicState();
	}

	public interface MyCellTableResources extends CellTable.Resources {

	     @Source({CellTable.Style.DEFAULT_CSS, "cellTableStyles.css" })
	     public CellTable.Style cellTableStyle();
	}
	
	private static DesktopListCentralAdminUiBinder uiBinder = GWT.create(DesktopListCentralAdminUiBinder.class);

	interface DesktopListCentralAdminUiBinder extends UiBinder<Widget, DesktopListCentralAdmin> {
	}

	public DesktopListCentralAdmin() {
		initWidget(uiBinder.createAndBindUi(this));
		setRefreshButtonImage(refreshButton);
	}

	@UiHandler("actionsButton")
	void actionsButtonClicked(ClickEvent e) {
		actionsPopup.clear();
		actionsPopup.setAutoHideEnabled(true);
		actionsPopup.setAnimationEnabled(true);
		actionsPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");
		
		Grid grid = new Grid(12, 1);
		grid.setCellSpacing(8);
		actionsPopup.add(grid);

		Anchor userNotificationAnchor = new Anchor("Create User Notification");
		userNotificationAnchor.addStyleName("productAnchor");
		userNotificationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		userNotificationAnchor.setTitle("Create a user notification");
		userNotificationAnchor.ensureDebugId(userNotificationAnchor.getText());
		userNotificationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				selectionModel.getSelectedObject();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_USER_NOTIFICATION, userLoggedIn);
			}
		});
		grid.setWidget(0, 0, userNotificationAnchor);

		Anchor accountNotificationAnchor = new Anchor("Create Account Notification");
		accountNotificationAnchor.addStyleName("productAnchor");
		accountNotificationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		accountNotificationAnchor.setTitle("Create an account notification");
		accountNotificationAnchor.ensureDebugId(accountNotificationAnchor.getText());
		accountNotificationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				selectionModel.getSelectedObject();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_ACCOUNT_NOTIFICATION, userLoggedIn);
			}
		});
		grid.setWidget(1, 0, accountNotificationAnchor);

		Anchor maintainCentralAdminRoleAssignmentsAnchor = new Anchor("Maintain Central Admin Role Assignments");
		maintainCentralAdminRoleAssignmentsAnchor.addStyleName("productAnchor");
		maintainCentralAdminRoleAssignmentsAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		maintainCentralAdminRoleAssignmentsAnchor.setTitle("Maintain the account level roles the selected central admin is assigned to.");
		maintainCentralAdminRoleAssignmentsAnchor.ensureDebugId(maintainCentralAdminRoleAssignmentsAnchor.getText());
		maintainCentralAdminRoleAssignmentsAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				RoleAssignmentSummaryPojo m = selectionModel.getSelectedObject();
				if (m == null) {
					showMessageToUser("Please select a central admin from the list");
				}
				else {
					// via presenter, get user account for selected role assignment summary
					// then, go to the maintain central admin roles page with that user
					String selectedPpid = m.getDirectoryPerson().getKey();
					String userLoggedInPpid = userLoggedIn.getPublicId();
					if (!selectedPpid.equalsIgnoreCase(userLoggedInPpid)) {
						if (!userLoggedIn.isCentralAdminManager()) {
							// user is NOT a CAM, they can view but not change
							// other central admins
							GWT.log("user logged in is NOT a CAM so they can "
								+ "VIEW other central admins.");
							presenter.maintainCentralAdminRole(userLoggedIn, m);
						}
						else {
							// they are a ca manager, let the presenter get the 
							// user account for the selected ra and do it.
							GWT.log("user logged in is a CAM so they can "
								+ "manage other central admins.");
							presenter.maintainCentralAdminRole(userLoggedIn, m);
						}
					}
					else {
						// they've selected themselves let them go
						GWT.log("user selected themselves so we don't need to "
							+ "refresh the roles for the user");
						presenter.maintainCentralAdminRole(userLoggedIn, m);
//						ActionEvent.fire(
//							presenter.getEventBus(), 
//							ActionNames.MAINTAIN_CENTRAL_ADMIN_ROLE_ASSIGNMENTS, 
//							userLoggedIn);
					}
				}
			}
		});
		grid.setWidget(2, 0, maintainCentralAdminRoleAssignmentsAnchor);
		
		Anchor manageSrdBehaviorAnchor = new Anchor("Manage SRD Behavior");
		manageSrdBehaviorAnchor.addStyleName("productAnchor");
		manageSrdBehaviorAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		manageSrdBehaviorAnchor.setTitle("Manage how the SRD behaves for one or "
			+ "more accounts.  Central admins can view but only central admin "
			+ "managers can maintain/edit");
		manageSrdBehaviorAnchor.ensureDebugId(manageSrdBehaviorAnchor.getText());
		manageSrdBehaviorAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				selectionModel.getSelectedObject();
//				showStatus(actionsButton, "Comming soon!");
				ActionEvent.fire(
						presenter.getEventBus(), 
						ActionNames.MANAGE_SRD_BEHAVIOR, 
						userLoggedIn);
			}
		});
		grid.setWidget(3, 0, manageSrdBehaviorAnchor);

		// TODO
		// - start migration
		// - show migration status
		// - start sync
		// - stop sync

		
		Anchor startMigrationAnchor = new Anchor("Start IIC Data Migration");
		startMigrationAnchor.addStyleName("productAnchor");
		startMigrationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		startMigrationAnchor.setTitle("Initiate the IAM Identity Center data migration process.");
		startMigrationAnchor.ensureDebugId(startMigrationAnchor.getText());
		startMigrationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				
				final HomeView hv = presenter.getClientFactory().getHomeView();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(final Throwable caught1) {
						presenter.refreshIicState();
					}

					@Override
					public void onSuccess(Void result) {
						Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
					        public void execute () {
								presenter.refreshIicState();
								// hide long running task message
								hv.hideIccDataMigrationNotice();
								// show migration status message
					        }
					    });
					}
				};
				boolean confirmed = Window.confirm("You are about to initiate "
						+ "the IAM Identity Center data migration process.  Are you "
						+ "sure you wish to continue?");

				if (confirmed) {
					// show long running task message
					hv.showIccDataMigrationNotice();
					// iniate the icc data migration process 
					VpcProvisioningService.Util.getInstance().startIicMigration(callback);
				}
				else {
					// show message
					showMessageToUser("Operation was cancelled.");
				}
			}
		});
		grid.setWidget(4, 0, startMigrationAnchor);

		Anchor showIicMigrationStatusAnchor = new Anchor("View IIC Data Migration Status");
		showIicMigrationStatusAnchor.addStyleName("productAnchor");
		showIicMigrationStatusAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		showIicMigrationStatusAnchor.setTitle("View the IAM Identity Center migration status");
		showIicMigrationStatusAnchor.ensureDebugId(showIicMigrationStatusAnchor.getText());
		showIicMigrationStatusAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				final HomeView hv = presenter.getClientFactory().getHomeView();
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception performing IAM Identity Center data migration."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(String result) {
						// hide long running task message
						hv.hideIccDataMigrationNotice();
						// show migration status message
						GWT.log("TEMPORARY:  RESULT: " + result);
						hv.showIccDataMigrationResults(true, false, result);
					}
				};
				// show iic migration status 
				VpcProvisioningService.Util.getInstance().getIicDataMigrationStatus(callback);
			}
		});
		grid.setWidget(5, 0, showIicMigrationStatusAnchor);

		Anchor showIicSyncStatusAnchor = new Anchor("View IIC Data Synchronization Status");
		showIicSyncStatusAnchor.addStyleName("productAnchor");
		showIicSyncStatusAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		showIicSyncStatusAnchor.setTitle("View the IAM Identity Center synchronization status");
		showIicSyncStatusAnchor.ensureDebugId(showIicSyncStatusAnchor.getText());
		showIicSyncStatusAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				final HomeView hv = presenter.getClientFactory().getHomeView();
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception performing IAM Identity Center data migration."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(String result) {
						// hide long running task message
						hv.hideIccDataMigrationNotice();
						// show migration status message
						GWT.log("TEMPORARY:  RESULT: " + result);
						hv.showIccDataMigrationResults(false, false, result);
					}
				};
				// show iic migration status 
				VpcProvisioningService.Util.getInstance().getIicSychronizationStatus(callback);
			}
		});
		grid.setWidget(6, 0, showIicSyncStatusAnchor);

		Anchor startIicSyncAnchor = new Anchor("Start IIC Synchronization");
		startIicSyncAnchor.addStyleName("productAnchor");
		startIicSyncAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		startIicSyncAnchor.setTitle("Start the background process that keeps "
			+ "IAM Identity Center in-sync with IAM custom role updates.");
		startIicSyncAnchor.ensureDebugId(startIicSyncAnchor.getText());
		startIicSyncAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception starting the IIC sync process."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(Void result) {
						Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
					        public void execute () {
								presenter.refreshIicState();
								showMessageToUser("The IAM Identity Center "
									+ "sync background process has been started.");
					        }
					    });
					}
				};
				// start iic sync 
				VpcProvisioningService.Util.getInstance().startIicSync(callback);
			}
		});
		grid.setWidget(7, 0, startIicSyncAnchor);

		Anchor stopIicSyncAnchor = new Anchor("Stop IIC Synchronization");
		stopIicSyncAnchor.addStyleName("productAnchor");
		stopIicSyncAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		stopIicSyncAnchor.setTitle("Stop the background process that keeps "
			+ "IAM Identity Center in-sync with IAM custom role updates.");
		stopIicSyncAnchor.ensureDebugId(stopIicSyncAnchor.getText());
		stopIicSyncAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception stopping the IIC sync."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(Void result) {
						Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
					        public void execute () {
								presenter.refreshIicState();
								showMessageToUser("The IAM Identity Center "
									+ "sync background process has been stopped.");
					        }
					    });
					}
				};
				// stop iic sync 
				VpcProvisioningService.Util.getInstance().stopIicSync(callback);
			}
		});
		grid.setWidget(8, 0, stopIicSyncAnchor);

		Anchor showEcsIicMigrationStatusAnchor = new Anchor("View ECS IIC Synchronization Status");
		showEcsIicMigrationStatusAnchor.addStyleName("productAnchor");
		showEcsIicMigrationStatusAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		showEcsIicMigrationStatusAnchor.setTitle("View the ECS IAM Identity Center synchronization status");
		showEcsIicMigrationStatusAnchor.ensureDebugId(showEcsIicMigrationStatusAnchor.getText());
		showEcsIicMigrationStatusAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				final HomeView hv = presenter.getClientFactory().getHomeView();
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception performing ECS IAM Identity Center data migration."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(String result) {
						// hide long running task message
						hv.hideIccDataMigrationNotice();
						// show migration status message
						hv.showIccDataMigrationResults(false, true, result);
					}
				};
				// show iic migration status 
				VpcProvisioningService.Util.getInstance().getEcsIicSynchronzationStatus(callback);
			}
		});
		grid.setWidget(9, 0, showEcsIicMigrationStatusAnchor);

		Anchor startEcsIicSyncAnchor = new Anchor("Start ECS IIC Synchronization");
		startEcsIicSyncAnchor.addStyleName("productAnchor");
		startEcsIicSyncAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		startEcsIicSyncAnchor.setTitle("Start the background process that keeps "
			+ "IAM Identity Center in-sync with ECS IAM custom role updates (manual updates on the backend).");
		startEcsIicSyncAnchor.ensureDebugId(startEcsIicSyncAnchor.getText());
		startEcsIicSyncAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception starting the ECS IIC sync process."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(Void result) {
						Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
					        public void execute () {
								presenter.refreshIicState();
								showMessageToUser("The ECS IAM Identity Center "
									+ "sync background process has been started.");
					        }
					    });
					}
				};
				// start iic sync 
				VpcProvisioningService.Util.getInstance().startEcsIicSync(callback);
			}
		});
		grid.setWidget(10, 0, startEcsIicSyncAnchor);

		Anchor stopEcsIicSyncAnchor = new Anchor("Stop ECS IIC Synchronization");
		stopEcsIicSyncAnchor.addStyleName("productAnchor");
		stopEcsIicSyncAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		stopEcsIicSyncAnchor.setTitle("Stop the background process that keeps "
			+ "IAM Identity Center in-sync with ECS IAM custom role updates (on the backend).");
		stopEcsIicSyncAnchor.ensureDebugId(stopEcsIicSyncAnchor.getText());
		stopEcsIicSyncAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						String msg = "Exception stopping the ECS IIC sync."; 
						GWT.log(msg, caught);
						showMessageToUser(msg + 
							"<p>Message from server is: " + caught.getMessage() + "</p>");
					}

					@Override
					public void onSuccess(Void result) {
						Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
					        public void execute () {
								presenter.refreshIicState();
								showMessageToUser("The ECS IAM Identity Center "
									+ "sync background process has been stopped.");
					        }
					    });
					}
				};
				// stop iic sync 
				VpcProvisioningService.Util.getInstance().stopEcsIicSync(callback);
			}
		});
		grid.setWidget(11, 0, stopEcsIicSyncAnchor);

		actionsPopup.showRelativeTo(actionsButton);
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}

	@Override
	public void applyCentralAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		
		return null;
	}

	@Override
	public void resetFieldStyles() {
		
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		
		return null;
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}

	@Override
	public void vpcpPromptCancel() {
		
		
	}

	@Override
	public void vpcpConfirmOkay() {
		
		
	}

	@Override
	public void vpcpConfirmCancel() {
		
		
	}

	@Override
	public void clearList() {
		centralAdminListTable.setVisibleRangeAndClearData(centralAdminListTable.getVisibleRange(), true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setCentralAdmins(List<RoleAssignmentSummaryPojo> centralAdmins) {
		GWT.log("view Setting centralAdmins.");
		this.centralAdminList = centralAdmins;
		this.initializeCentralAdminListTable();
	    centralAdminListPager.setDisplay(centralAdminListTable);
	}

	private Widget initializeCentralAdminListTable() {
		GWT.log("initializing Central Admin list table...");
		centralAdminListTable.setTableLayoutFixed(false);
		centralAdminListTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		
		// set range to display
		centralAdminListTable.setVisibleRange(0, 20);
		
		// create dataprovider
		dataProvider = new ListDataProvider<RoleAssignmentSummaryPojo>();
		dataProvider.addDataDisplay(centralAdminListTable);
		dataProvider.getList().clear();
		dataProvider.getList().addAll(this.centralAdminList);
		
		selectionModel = 
	    	new SingleSelectionModel<RoleAssignmentSummaryPojo>(RoleAssignmentSummaryPojo.KEY_PROVIDER);
		centralAdminListTable.setSelectionModel(selectionModel);
	    
	    selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	    	@Override
	    	public void onSelectionChange(SelectionChangeEvent event) {
	    		selectionModel.getSelectedObject();
	    	}
	    });

	    ListHandler<RoleAssignmentSummaryPojo> sortHandler = 
	    	new ListHandler<RoleAssignmentSummaryPojo>(dataProvider.getList());
	    centralAdminListTable.addColumnSortHandler(sortHandler);

	    if (centralAdminListTable.getColumnCount() == 0) {
		    initCentralAdminListTableColumns(sortHandler);
	    }
		
		return centralAdminListTable;
	}

	private void initCentralAdminListTableColumns(ListHandler<RoleAssignmentSummaryPojo> sortHandler) {
		GWT.log("initializing Central Admin list table columns...");
		
		// Checkbox column. This table will uses a checkbox column for selection.
	    // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
	    // mouse selection.
	    Column<RoleAssignmentSummaryPojo, Boolean> checkColumn = new Column<RoleAssignmentSummaryPojo, Boolean>(
	        new CheckboxCell(true, false)) {
	      @Override
	      public Boolean getValue(RoleAssignmentSummaryPojo object) {
	        // Get the value from the selection model.
	        return selectionModel.isSelected(object);
	      }
	    };
	    centralAdminListTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
	    centralAdminListTable.setColumnWidth(checkColumn, 40, Unit.PX);

	    Column<RoleAssignmentSummaryPojo, String> adminNameColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getDirectoryPerson() != null) {
					if (object.getDirectoryPerson().getFullName() == null || 
						object.getDirectoryPerson().getFullName().length() == 0) {

						return "Unknown name.  PPID is: " + object.getDirectoryPerson().getKey();
					}
					else {
						return object.getDirectoryPerson().getFullName();
					}
				}
				else {
					return "Unknown (null Directory Person)";
				}
			}
		};
		adminNameColumn.setSortable(true);
		adminNameColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(adminNameColumn, new Comparator<RoleAssignmentSummaryPojo>() {
			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
				return o1.getDirectoryPerson().getFullName().compareTo(o2.getDirectoryPerson().getFullName());
			}
		});
		centralAdminListTable.addColumn(adminNameColumn, "Admin Name");

		Column<RoleAssignmentSummaryPojo, String> reasonColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				return object.getRoleAssignment().getReason();
			}
		};
		reasonColumn.setSortable(true);
		reasonColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(reasonColumn, new Comparator<RoleAssignmentSummaryPojo>() {
			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
				return o1.getRoleAssignment().getReason().compareTo(o2.getRoleAssignment().getReason());
			}
		});
		centralAdminListTable.addColumn(reasonColumn, "Reason");


		Column<RoleAssignmentSummaryPojo, String> effectiveDateColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getRoleAssignment().getEffectiveDate() != null) {
					return dateFormat.format(object.getRoleAssignment().getEffectiveDate());
				}
				else {
					return "Unknown";
				}
			}
		};
//		effectiveDateColumn.setSortable(true);
//		sortHandler.setComparator(effectiveDateColumn, new Comparator<RoleAssignmentSummaryPojo>() {
//			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
//				if (o1.getRoleAssignment().getEffectiveDate() != null && o2.getRoleAssignment().getEffectiveDate() != null) {
//					return o1.getRoleAssignment().getEffectiveDate().compareTo(o2.getRoleAssignment().getEffectiveDate());
//				}
//				else {
//					return 0;
//				}
//			}
//		});
		centralAdminListTable.addColumn(effectiveDateColumn, "Effective Date");

		Column<RoleAssignmentSummaryPojo, String> expirationDate = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getRoleAssignment().getExpirationDate() != null) {
					return dateFormat.format(object.getRoleAssignment().getExpirationDate());
				}
				else {
					return "Unknown";
				}
			}
		};
//		effectiveDateColumn.setSortable(true);
//		sortHandler.setComparator(effectiveDateColumn, new Comparator<RoleAssignmentSummaryPojo>() {
//			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
//				if (o1.getRoleAssignment().getEffectiveDate() != null && o2.getRoleAssignment().getEffectiveDate() != null) {
//					return o1.getRoleAssignment().getEffectiveDate().compareTo(o2.getRoleAssignment().getEffectiveDate());
//				}
//				else {
//					return 0;
//				}
//			}
//		});
		centralAdminListTable.addColumn(expirationDate, "Expiration Date");
	}
	
	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void removeCentralAdminFromView(RoleAssignmentSummaryPojo centralAdmin) {
		dataProvider.getList().remove(centralAdmin);
	}

	@Override
	public void initPage() {
//		centralAdminIdTB.setText("");
//		centralAdminIdTB.getElement().setPropertyString("placeholder", "enter filter text");
	}

	@Override
	public void setMyNetIdURL(String url) {
		String intro = introBodyHTML.
				getHTML().
				replace("MY_NET_ID_URL", url);
		introBodyHTML.setHTML(intro);
	}

	@Override
	public void disableButtons() {
		
		
	}

	@Override
	public void enableButtons() {
		
		
	}

	@Override
	public void applyNetworkAdminMask() {
		
		
	}

	@Override
	public void setIicSyncState(String state) {
		actionsPopup.setTitle(state);
	}

	@Override
	public void setIicStatus(final HTML status) {
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	    		iicStatusHTML.setHTML(SafeHtmlUtils.fromSafeConstant(status.toString()));
	        }
	    });
	}

	@Override
	public void setEcsIicSyncState(String state) {
		// TODO Auto-generated method stub
		
		
		// NOTE SURE IF THIS IS NEEDED, need to look at setIicSyncState (actionsPopup.setTitle)
	}

}
