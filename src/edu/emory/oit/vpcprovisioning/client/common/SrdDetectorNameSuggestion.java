package edu.emory.oit.vpcprovisioning.client.common;

import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

public interface SrdDetectorNameSuggestion extends Suggestion {
	String getDetectorName();
}
