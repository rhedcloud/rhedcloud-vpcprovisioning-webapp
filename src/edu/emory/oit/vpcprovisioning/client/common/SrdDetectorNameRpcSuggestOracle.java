package edu.emory.oit.vpcprovisioning.client.common;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.SuggestOracle;

public class SrdDetectorNameRpcSuggestOracle extends SuggestOracle {
	List<String> detectorNames = new java.util.ArrayList<String>();

	public SrdDetectorNameRpcSuggestOracle(List<String> detectorNames) {
		this.detectorNames = detectorNames;
	}
	
	@Override
	public void requestSuggestions(final Request request, final Callback callback) {
		GWT.log("SrdDetectorNameRpcSuggestOracle.requestSuggestions...");
		List<MultiWordRpcSuggestion> descList = new java.util.ArrayList<MultiWordRpcSuggestion>();
		for (String name : detectorNames) {
			GWT.log("request is: '" + request.getQuery() + "'");
			if (name != null && request.getQuery() != null) {
				if (name.toLowerCase().
						indexOf(request.getQuery().toLowerCase()) >= 0) {
					
					descList.add(new MultiWordRpcSuggestion(name, name));
				}
			}
		}
		Response resp =
            new Response(descList);
		callback.onSuggestionsReady(request, resp);
	}

	private class MultiWordRpcSuggestion implements SrdDetectorNameSuggestion {
		String name;
		String replacementString;
		
		public MultiWordRpcSuggestion(String name, String replacementString) {
			super();
			this.name = name;
			this.replacementString = replacementString;
		}

		@Override
		public String getDetectorName() {
			return name;
		}

		@Override
		public String getDisplayString() {
			// TODO Auto-generated method stub
			return name;
		}

		@Override
		public String getReplacementString() {
			// TODO Auto-generated method stub
			return replacementString;
		}

	}
	
	public static class MWCallback implements SuggestOracle.Callback {

		public MWCallback() {
			super();
		}

		@Override
		public void onSuggestionsReady(Request request, Response response) {
		}
	}
}
