package edu.emory.oit.vpcprovisioning.client.common;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle;

import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.shared.Constants;

public class ProjectPurposeRpcSuggestOracle extends SuggestOracle {

	public ProjectPurposeRpcSuggestOracle() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void requestSuggestions(final Request request, final Callback callback) {
		AsyncCallback<List<String>> srvrCallback = new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("[MultiWordRpcSuggestOracle.requestSuggestions] Failure: " + caught);
				List<MultiWordRpcSuggestion> descList = new java.util.ArrayList<MultiWordRpcSuggestion>();
				descList.add(new MultiWordRpcSuggestion(Constants.OTHER));
				Response resp =
			            new Response(descList);
				callback.onSuggestionsReady(request, resp);
			}

			@Override
			public void onSuccess(List<String> purposes) {
				List<MultiWordRpcSuggestion> descList = new java.util.ArrayList<MultiWordRpcSuggestion>();
				if (purposes.size() == 0) {
					descList.add(new MultiWordRpcSuggestion(Constants.OTHER));
				}
				else {
					for (String purpose : purposes) {
						descList.add(new MultiWordRpcSuggestion(purpose));
					}
				}
				Response resp =
		            new Response(descList);
				callback.onSuggestionsReady(request, resp);
			}
			
		};
		VpcProvisioningService.Util.getInstance().getExistingProjectPurposes(srvrCallback);
	}

	private class MultiWordRpcSuggestion implements ProjectPurposeSuggestion {
		String purpose;
		
		public MultiWordRpcSuggestion(String platform) {
			super();
			this.purpose = platform;
		}

		@Override
		public String getProjectPurpose() {
			return purpose;
		}

		@Override
		public String getDisplayString() {
			return purpose;
		}

		@Override
		public String getReplacementString() {
			return purpose;
		}
	}
	
	public static class MWCallback implements SuggestOracle.Callback {

		public MWCallback() {
			super();
		}

		@Override
		public void onSuggestionsReady(Request request, Response response) {
		}
	}
}
