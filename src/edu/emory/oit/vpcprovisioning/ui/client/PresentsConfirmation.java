package edu.emory.oit.vpcprovisioning.ui.client;

public interface PresentsConfirmation extends PresentsWidgets {
	void vpcpConfirmOkay();
	void vpcpConfirmCancel();
}
